#include "AreaStrainLimiting.h"
#include "Utils.h"

USE_PRJ_NAMESPACE

//AreaStrainLimitingQuadratic
AreaStrainLimitingQuadratic::AreaStrainLimitingQuadratic():ElasticModel<2>(typeid(AreaStrainLimitingQuadratic).name(),-1) {}
AreaStrainLimitingQuadratic::AreaStrainLimitingQuadratic(const string& name):ElasticModel<2>(name,-1) {}
AreaStrainLimitingQuadratic::AreaStrainLimitingQuadratic(const Vec& x,sizeType a,sizeType b,sizeType c,const Vec2d& limit,scalarD coef):ElasticModel<2>(typeid(AreaStrainLimitingQuadratic).name(),-1)
{
  a*=2;
  b*=2;
  c*=2;
  VecDIMd b2a=x.template segment<2>(b)-x.template segment<2>(a);
  VecDIMd c2a=x.template segment<2>(c)-x.template segment<2>(a);
  scalar area=b2a[0]*c2a[1]-b2a[1]*c2a[0];
  if(area < 0) {
    swap(b,c);
    area=-area;
  }
  _d0=-area*limit[0];
  _d1=-area*limit[1];
  _id << a,a+1,b,b+1,c,c+1;
  _coef=coef;
}
void AreaStrainLimitingQuadratic::writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const
{
  FUNCTION_NOT_IMPLEMENTED
}
bool AreaStrainLimitingQuadratic::read(istream& is,IOData* dat)
{
  ElasticModel<2>::read(is,dat);
  readBinaryData(_d0,is);
  readBinaryData(_d1,is);
  readBinaryData(_coef,is);
  readBinaryData(_id,is);
  return is.good();
}
bool AreaStrainLimitingQuadratic::write(ostream& os,IOData* dat) const
{
  ElasticModel<2>::write(os,dat);
  writeBinaryData(_d0,os);
  writeBinaryData(_d1,os);
  writeBinaryData(_coef,os);
  writeBinaryData(_id,os);
  return os.good();
}
boost::shared_ptr<Serializable> AreaStrainLimitingQuadratic::copy() const
{
  return boost::shared_ptr<Serializable>(new AreaStrainLimitingQuadratic());
}
Coli AreaStrainLimitingQuadratic::ids() const
{
  return Vec3i(_id[0],_id[2],_id[4])/2;
}
scalarD AreaStrainLimitingQuadratic::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  Vec6d gradBlk;
  Mat6d hessBlk;
  VecDIMd b2a=x.template segment<2>(_id[2])-x.template segment<2>(_id[0]);
  VecDIMd c2a=x.template segment<2>(_id[4])-x.template segment<2>(_id[0]);
  VecDIMd b2c=b2a-c2a;
  scalarD area=b2a[0]*c2a[1]-b2a[1]*c2a[0];
  scalarD delta0=min<scalarD>(area+_d0,0),delta1=max<scalarD>(area+_d1,0);
  scalarD gradCoef=(delta0+delta1)*coef;
  if(delta0+delta1 == 0)
    return 0;
  if(fgrad) {
    Vec& gradI=fgrad->getMatrixI();
    gradBlk << b2c[1],-b2c[0],c2a[1],-c2a[0],-b2a[1],b2a[0];
    for(sizeType d=0; d<6; d++)
      gradI[_id[d]]+=gradBlk[d]*gradCoef;
  }
  if(fhess) {
    hessBlk=gradBlk*gradBlk.transpose()*coef;
    //A
    hessBlk(0,3)+=gradCoef;
    hessBlk(3,0)+=gradCoef;
    hessBlk(1,2)-=gradCoef;
    hessBlk(2,1)-=gradCoef;
    //B
    hessBlk(0,5)-=gradCoef;
    hessBlk(5,0)-=gradCoef;
    hessBlk(1,4)+=gradCoef;
    hessBlk(4,1)+=gradCoef;
    //C
    hessBlk(2,5)+=gradCoef;
    hessBlk(5,2)+=gradCoef;
    hessBlk(3,4)-=gradCoef;
    hessBlk(4,3)-=gradCoef;
    //add blk
    addBlockIndexed(*fhess,_id,hessBlk);
  }
  return (delta0*delta0+delta1*delta1)*coef/2;
}
int AreaStrainLimitingQuadratic::inputs() const
{
  return 6;
}
//AreaStrainLimitingCubic
AreaStrainLimitingCubic::AreaStrainLimitingCubic():AreaStrainLimitingQuadratic(typeid(AreaStrainLimitingCubic).name()) {}
AreaStrainLimitingCubic::AreaStrainLimitingCubic(const Vec& x,sizeType a,sizeType b,sizeType c,const Vec2d& limit,scalarD coef):AreaStrainLimitingQuadratic(typeid(AreaStrainLimitingCubic).name())
{
  a*=2;
  b*=2;
  c*=2;
  VecDIMd b2a=x.template segment<2>(b)-x.template segment<2>(a);
  VecDIMd c2a=x.template segment<2>(c)-x.template segment<2>(a);
  scalar area=b2a[0]*c2a[1]-b2a[1]*c2a[0];
  if(area < 0) {
    swap(b,c);
    area=-area;
  }
  _d0=-area*limit[0];
  _d1=-area*limit[1];
  _id << a,a+1,b,b+1,c,c+1;
  _coef=coef;
}
boost::shared_ptr<Serializable> AreaStrainLimitingCubic::copy() const
{
  return boost::shared_ptr<Serializable>(new AreaStrainLimitingCubic());
}
scalarD AreaStrainLimitingCubic::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  Vec6d gradBlk;
  Mat6d hessBlk;
  VecDIMd b2a=x.template segment<2>(_id[2])-x.template segment<2>(_id[0]);
  VecDIMd c2a=x.template segment<2>(_id[4])-x.template segment<2>(_id[0]);
  VecDIMd b2c=b2a-c2a;
  scalarD area=b2a[0]*c2a[1]-b2a[1]*c2a[0];
  scalarD delta0=min<scalarD>(area+_d0,0),delta1=max<scalarD>(area+_d1,0);
  scalarD gradCoef=(delta1*delta1-delta0*delta0)*coef;
  if(delta0+delta1 == 0)
    return 0;
  if(fgrad) {
    Vec& gradI=fgrad->getMatrixI();
    gradBlk << b2c[1],-b2c[0],c2a[1],-c2a[0],-b2a[1],b2a[0];
    for(sizeType d=0; d<6; d++)
      gradI[_id[d]]+=gradBlk[d]*gradCoef;
  }
  if(fhess) {
    hessBlk=gradBlk*gradBlk.transpose()*(2*(delta1-delta0)*coef);
    //A
    hessBlk(0,3)+=gradCoef;
    hessBlk(3,0)+=gradCoef;
    hessBlk(1,2)-=gradCoef;
    hessBlk(2,1)-=gradCoef;
    //B
    hessBlk(0,5)-=gradCoef;
    hessBlk(5,0)-=gradCoef;
    hessBlk(1,4)+=gradCoef;
    hessBlk(4,1)+=gradCoef;
    //C
    hessBlk(2,5)+=gradCoef;
    hessBlk(5,2)+=gradCoef;
    hessBlk(3,4)-=gradCoef;
    hessBlk(4,3)-=gradCoef;
    //add blk
    addBlockIndexed(*fhess,_id,hessBlk);
  }
  return (delta1*delta1*delta1-delta0*delta0*delta0)*coef/3;
}
//AreaStrainLimitingSoftmax
AreaStrainLimitingSoftmax::AreaStrainLimitingSoftmax():AreaStrainLimitingQuadratic(typeid(AreaStrainLimitingSoftmax).name()) {}
AreaStrainLimitingSoftmax::AreaStrainLimitingSoftmax(const Vec& x,sizeType a,sizeType b,sizeType c,const Vec2d& limit,scalarD coef):AreaStrainLimitingQuadratic(typeid(AreaStrainLimitingSoftmax).name())
{
  a*=2;
  b*=2;
  c*=2;
  VecDIMd b2a=x.template segment<2>(b)-x.template segment<2>(a);
  VecDIMd c2a=x.template segment<2>(c)-x.template segment<2>(a);
  scalar area=b2a[0]*c2a[1]-b2a[1]*c2a[0];
  if(area < 0) {
    swap(b,c);
    area=-area;
  }
  _softmaxReg=10/area;
  _d0=-area*limit[0]*_softmaxReg;
  _d1=-area*limit[1]*_softmaxReg;
  _id << a,a+1,b,b+1,c,c+1;
  _coef=coef/_softmaxReg/_softmaxReg;
}
bool AreaStrainLimitingSoftmax::read(istream& is,IOData* dat)
{
  AreaStrainLimitingQuadratic::read(is,dat);
  readBinaryData(_softmaxReg,is);
  return is.good();
}
bool AreaStrainLimitingSoftmax::write(ostream& os,IOData* dat) const
{
  AreaStrainLimitingQuadratic::write(os,dat);
  writeBinaryData(_softmaxReg,os);
  return os.good();
}
boost::shared_ptr<Serializable> AreaStrainLimitingSoftmax::copy() const
{
  return boost::shared_ptr<Serializable>(new AreaStrainLimitingSoftmax());
}
scalarD AreaStrainLimitingSoftmax::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  Vec6d gradBlk;
  Mat6d hessBlk;
  VecDIMd b2a=x.template segment<2>(_id[2])-x.template segment<2>(_id[0]);
  VecDIMd c2a=x.template segment<2>(_id[4])-x.template segment<2>(_id[0]);
  VecDIMd b2c=b2a-c2a;
  scalarD area=(b2a[0]*c2a[1]-b2a[1]*c2a[0])*_softmaxReg;
  scalarD delta0Exp=exp(-area-_d0),delta1Exp=exp(area+_d1);
  scalarD delta0=log(1+delta0Exp),delta1=log(1+delta1Exp),gradCoef=0;
  if(fgrad || fhess)
    gradCoef=(delta1*delta1Exp/(1+delta1Exp)-delta0*delta0Exp/(1+delta0Exp))*coef;
  if(fgrad) {
    Vec& gradI=fgrad->getMatrixI();
    gradBlk << b2c[1],-b2c[0],c2a[1],-c2a[0],-b2a[1],b2a[0];
    gradBlk*=_softmaxReg;
    for(sizeType d=0; d<6; d++)
      gradI[_id[d]]+=gradBlk[d]*gradCoef;
  }
  if(fhess) {
    hessBlk=gradBlk*gradBlk.transpose()*(((delta1+delta1Exp)*delta1Exp/((1+delta1Exp)*(1+delta1Exp))+
                                          (delta0+delta0Exp)*delta0Exp/((1+delta0Exp)*(1+delta0Exp)))*coef);
    gradCoef*=_softmaxReg;
    //A
    hessBlk(0,3)+=gradCoef;
    hessBlk(3,0)+=gradCoef;
    hessBlk(1,2)-=gradCoef;
    hessBlk(2,1)-=gradCoef;
    //B
    hessBlk(0,5)-=gradCoef;
    hessBlk(5,0)-=gradCoef;
    hessBlk(1,4)+=gradCoef;
    hessBlk(4,1)+=gradCoef;
    //C
    hessBlk(2,5)+=gradCoef;
    hessBlk(5,2)+=gradCoef;
    hessBlk(3,4)-=gradCoef;
    hessBlk(4,3)-=gradCoef;
    addBlockIndexed(*fhess,_id,hessBlk);
  }
  return (delta1*delta1+delta0*delta0)*coef/2;
}
//AreaStrainLimitingLog
AreaStrainLimitingLog::AreaStrainLimitingLog():AreaStrainLimitingQuadratic(typeid(AreaStrainLimitingLog).name()) {}
AreaStrainLimitingLog::AreaStrainLimitingLog(const Vec& x,sizeType a,sizeType b,sizeType c,const Vec2d& limit,scalarD coef):AreaStrainLimitingQuadratic(typeid(AreaStrainLimitingLog).name())
{
  a*=2;
  b*=2;
  c*=2;
  VecDIMd b2a=x.template segment<2>(b)-x.template segment<2>(a);
  VecDIMd c2a=x.template segment<2>(c)-x.template segment<2>(a);
  scalar area=b2a[0]*c2a[1]-b2a[1]*c2a[0];
  if(area < 0) {
    swap(b,c);
    area=-area;
  }
  _d0=-area*limit[0];
  _d1=-area*limit[1];
  _id << a,a+1,b,b+1,c,c+1;
  _coef=coef;
}
boost::shared_ptr<Serializable> AreaStrainLimitingLog::copy() const
{
  return boost::shared_ptr<Serializable>(new AreaStrainLimitingLog());
}
scalarD AreaStrainLimitingLog::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  Vec6d gradBlk;
  Mat6d hessBlk;
  VecDIMd b2a=x.template segment<2>(_id[2])-x.template segment<2>(_id[0]);
  VecDIMd c2a=x.template segment<2>(_id[4])-x.template segment<2>(_id[0]);
  VecDIMd b2c=b2a-c2a;
  scalarD area=b2a[0]*c2a[1]-b2a[1]*c2a[0],gradCoef=0;
  if(fgrad || fhess)
    gradCoef=-coef/(area+_d0)-coef/(area+_d1);
  if(fgrad) {
    Vec& gradI=fgrad->getMatrixI();
    gradBlk << b2c[1],-b2c[0],c2a[1],-c2a[0],-b2a[1],b2a[0];
    for(sizeType d=0; d<6; d++)
      gradI[_id[d]]+=gradBlk[d]*gradCoef;
  }
  if(fhess) {
    hessBlk=gradBlk*gradBlk.transpose()*(coef/pow(area+_d0,2)+coef/pow(area+_d1,2));
    //A
    hessBlk(0,3)+=gradCoef;
    hessBlk(3,0)+=gradCoef;
    hessBlk(1,2)-=gradCoef;
    hessBlk(2,1)-=gradCoef;
    //B
    hessBlk(0,5)-=gradCoef;
    hessBlk(5,0)-=gradCoef;
    hessBlk(1,4)+=gradCoef;
    hessBlk(4,1)+=gradCoef;
    //C
    hessBlk(2,5)+=gradCoef;
    hessBlk(5,2)+=gradCoef;
    hessBlk(3,4)-=gradCoef;
    hessBlk(4,3)-=gradCoef;
    addBlockIndexed(*fhess,_id,hessBlk);
  }
  return (-log(area+_d0)-log(-area-_d1))*coef;
}

//VolumeStrainLimitingQuadratic
VolumeStrainLimitingQuadratic::VolumeStrainLimitingQuadratic():ElasticModel<3>(typeid(VolumeStrainLimitingQuadratic).name(),-1) {}
VolumeStrainLimitingQuadratic::VolumeStrainLimitingQuadratic(const string& name):ElasticModel<3>(name,-1) {}
VolumeStrainLimitingQuadratic::VolumeStrainLimitingQuadratic(const Vec& x,sizeType a,sizeType b,sizeType c,sizeType d,const Vec2d& limit,scalarD coef):ElasticModel<3>(typeid(VolumeStrainLimitingQuadratic).name(),-1)
{
  a*=3;
  b*=3;
  c*=3;
  d*=3;
  VecDIMd b2a=x.template segment<3>(b)-x.template segment<3>(a);
  VecDIMd c2a=x.template segment<3>(c)-x.template segment<3>(a);
  VecDIMd d2a=x.template segment<3>(d)-x.template segment<3>(a);
  scalar vol=b2a.cross(c2a).dot(d2a);
  if(vol < 0) {
    swap(c,d);
    vol=-vol;
  }
  _d0=-vol*limit[0];
  _d1=-vol*limit[1];
  _id << a,a+1,a+2,b,b+1,b+2,c,c+1,c+2,d,d+1,d+2;
  _coef=coef;
}
void VolumeStrainLimitingQuadratic::writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const
{
  FUNCTION_NOT_IMPLEMENTED
}
bool VolumeStrainLimitingQuadratic::read(istream& is,IOData* dat)
{
  ElasticModel<3>::read(is,dat);
  readBinaryData(_d0,is);
  readBinaryData(_d1,is);
  readBinaryData(_coef,is);
  readBinaryData(_id,is);
  return is.good();
}
bool VolumeStrainLimitingQuadratic::write(ostream& os,IOData* dat) const
{
  ElasticModel<3>::write(os,dat);
  writeBinaryData(_d0,os);
  writeBinaryData(_d1,os);
  writeBinaryData(_coef,os);
  writeBinaryData(_id,os);
  return os.good();
}
boost::shared_ptr<Serializable> VolumeStrainLimitingQuadratic::copy() const
{
  return boost::shared_ptr<Serializable>(new VolumeStrainLimitingQuadratic());
}
Coli VolumeStrainLimitingQuadratic::ids() const
{
  return Vec4i(_id[0],_id[3],_id[6],_id[9])/3;
}
scalarD VolumeStrainLimitingQuadratic::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  Vec12d gradBlk;
  Mat12d hessBlk;
  scalarD vol=grada(x.segment<3>(_id[0]),x.segment<3>(_id[3]),x.segment<3>(_id[6]),x.segment<3>(_id[9]),fgrad ? &gradBlk : NULL,NULL,0,0);
  scalarD delta0=min<scalarD>(vol+_d0,0),delta1=max<scalarD>(vol+_d1,0);
  scalarD gradCoef=(delta0+delta1)*coef;
  if(delta0+delta1 == 0)
    return 0;
  if(fgrad) {
    Vec& gradI=fgrad->getMatrixI();
    for(sizeType d=0; d<12; d++)
      gradI[_id[d]]+=gradBlk[d]*gradCoef;
  }
  if(fhess) {
    grada(x.segment<3>(_id[0]),x.segment<3>(_id[3]),x.segment<3>(_id[6]),x.segment<3>(_id[9]),NULL,&hessBlk,gradCoef,coef);
    addBlockIndexed(*fhess,_id,hessBlk);
  }
  return (delta0*delta0+delta1*delta1)*coef/2;
}
int VolumeStrainLimitingQuadratic::inputs() const
{
  return 12;
}
scalarD VolumeStrainLimitingQuadratic::grada(const Vec3d& a,const Vec3d& b,const Vec3d& c,const Vec3d& d,Vec12d* J,Mat12d* H,scalarD hCoef,scalarD jjtCoef)
{
  //input
  //scalarD ax;
  //scalarD ay;
  //scalarD az;
  //scalarD bx;
  //scalarD by;
  //scalarD bz;
  //scalarD cx;
  //scalarD cy;
  //scalarD cz;
  //scalarD dx;
  //scalarD dy;
  //scalarD dz;
  //scalarD hCoef;
  //scalarD jjtCoef;
  scalarD E;

  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;
  scalarD tt9;
  scalarD tt10;
  scalarD tt11;
  scalarD tt12;
  scalarD tt13;
  scalarD tt14;
  scalarD tt15;
  scalarD tt16;
  scalarD tt17;
  scalarD tt18;
  scalarD tt19;
  scalarD tt20;
  scalarD tt21;
  scalarD tt22;
  scalarD tt23;
  scalarD tt24;
  scalarD tt25;
  scalarD tt26;
  scalarD tt27;
  scalarD tt28;
  scalarD tt29;
  scalarD tt30;
  scalarD tt31;
  scalarD tt32;
  scalarD tt33;
  scalarD tt34;
  scalarD tt35;
  scalarD tt36;
  scalarD tt37;
  scalarD tt38;
  scalarD tt39;
  scalarD tt40;
  scalarD tt41;
  scalarD tt42;
  scalarD tt43;
  scalarD tt44;
  scalarD tt45;
  scalarD tt46;
  scalarD tt47;
  scalarD tt48;
  scalarD tt49;
  scalarD tt50;
  scalarD tt51;
  scalarD tt52;
  scalarD tt53;
  scalarD tt54;
  scalarD tt55;
  scalarD tt56;
  scalarD tt57;
  scalarD tt58;
  scalarD tt59;
  scalarD tt60;
  scalarD tt61;
  scalarD tt62;
  scalarD tt63;
  scalarD tt64;
  scalarD tt65;
  scalarD tt66;
  scalarD tt67;
  scalarD tt68;
  scalarD tt69;
  scalarD tt70;
  scalarD tt71;
  scalarD tt72;
  scalarD tt73;
  scalarD tt74;
  scalarD tt75;
  scalarD tt76;
  scalarD tt77;
  scalarD tt78;
  scalarD tt79;
  scalarD tt80;
  scalarD tt81;
  scalarD tt82;
  scalarD tt83;
  scalarD tt84;
  scalarD tt85;
  scalarD tt86;
  scalarD tt87;
  scalarD tt88;
  scalarD tt89;
  scalarD tt90;
  scalarD tt91;
  scalarD tt92;
  scalarD tt93;
  scalarD tt94;
  scalarD tt95;
  scalarD tt96;
  scalarD tt97;
  scalarD tt98;
  scalarD tt99;
  scalarD tt100;
  scalarD tt101;
  scalarD tt102;
  scalarD tt103;
  scalarD tt104;
  scalarD tt105;
  scalarD tt106;
  scalarD tt107;
  scalarD tt108;
  scalarD tt109;
  scalarD tt110;
  scalarD tt111;

  tt1=-a[2];
  tt2=b[2]+tt1;
  tt3=-a[1];
  tt4=c[1]+tt3;
  tt5=b[1]+tt3;
  tt6=c[2]+tt1;
  tt7=tt5*tt6-tt2*tt4;
  tt8=-a[0];
  tt9=d[0]+tt8;
  tt10=c[0]+tt8;
  tt11=b[0]+tt8;
  tt12=tt2*tt10-tt11*tt6;
  tt13=d[1]+tt3;
  tt14=tt11*tt4-tt5*tt10;
  tt15=d[2]+tt1;
  E=tt14*tt15+tt12*tt13+tt7*tt9;
  if(!J && !H)
    return E;

  tt16=-b[2];
  tt17=c[2]+tt16;
  tt18=-c[1];
  tt19=tt18+b[1];
  tt20=tt19*tt15+tt17*tt13-tt5*tt6+tt2*tt4;
  tt21=-c[2];
  tt22=tt21+b[2];
  tt23=-b[0];
  tt24=c[0]+tt23;
  tt25=tt24*tt15+tt22*tt9+tt11*tt6-tt2*tt10;
  tt26=-b[1];
  tt27=c[1]+tt26;
  tt28=-c[0];
  tt29=tt28+b[0];
  tt30=tt29*tt13+tt27*tt9-tt11*tt4+tt5*tt10;
  tt31=tt21+a[2];
  tt32=tt4*tt15+tt31*tt13;
  tt33=tt28+a[0];
  tt34=tt33*tt15+tt6*tt9;
  tt35=tt18+a[1];
  tt36=tt10*tt13+tt35*tt9;
  tt37=tt26+a[1];
  tt38=tt37*tt15+tt2*tt13;
  tt39=tt16+a[2];
  tt40=tt11*tt15+tt39*tt9;
  tt41=tt23+a[0];
  tt42=tt41*tt13+tt5*tt9;
  tt43=tt25*tt20*jjtCoef;
  if(J) {
    (*J)[0]=tt20;
    (*J)[1]=tt25;
    (*J)[2]=tt30;
    (*J)[3]=tt32;
    (*J)[4]=tt34;
    (*J)[5]=tt36;
    (*J)[6]=tt38;
    (*J)[7]=tt40;
    (*J)[8]=tt42;
    (*J)[9]=tt7;
    (*J)[10]=tt12;
    (*J)[11]=tt14;
  }
  if(!H)
    return E;

  tt44=tt30*tt20*jjtCoef;
  tt45=tt20*tt32*jjtCoef;
  tt46=tt34*tt20*jjtCoef+(d[2]+tt21)*hCoef;
  tt47=-d[1];
  tt48=tt36*tt20*jjtCoef+(tt47+c[1])*hCoef;
  tt49=tt38*tt20*jjtCoef;
  tt50=-d[2];
  tt51=tt40*tt20*jjtCoef+(tt50+b[2])*hCoef;
  tt52=tt42*tt20*jjtCoef+(d[1]+tt26)*hCoef;
  tt53=tt7*tt20*jjtCoef;
  tt54=tt12*tt20*jjtCoef+tt17*hCoef;
  tt55=tt14*tt20*jjtCoef+tt19*hCoef;
  tt56=tt30*tt25*jjtCoef;
  tt57=tt25*tt32*jjtCoef+(tt50+c[2])*hCoef;
  tt58=tt34*tt25*jjtCoef;
  tt59=tt36*tt25*jjtCoef+(d[0]+tt28)*hCoef;
  tt60=tt38*tt25*jjtCoef+(d[2]+tt16)*hCoef;
  tt61=tt40*tt25*jjtCoef;
  tt62=-d[0];
  tt63=tt42*tt25*jjtCoef+(tt62+b[0])*hCoef;
  tt64=tt7*tt25*jjtCoef+tt22*hCoef;
  tt65=tt12*tt25*jjtCoef;
  tt66=tt14*tt25*jjtCoef+tt24*hCoef;
  tt67=tt30*tt32*jjtCoef+(d[1]+tt18)*hCoef;
  tt68=tt30*tt34*jjtCoef+(tt62+c[0])*hCoef;
  tt69=tt30*tt36*jjtCoef;
  tt70=tt30*tt38*jjtCoef+(tt47+b[1])*hCoef;
  tt71=tt30*tt40*jjtCoef+(d[0]+tt23)*hCoef;
  tt72=tt42*tt30*jjtCoef;
  tt73=tt7*tt30*jjtCoef+tt27*hCoef;
  tt74=tt12*tt30*jjtCoef+tt29*hCoef;
  tt75=tt14*tt30*jjtCoef;
  tt76=tt34*tt32*jjtCoef;
  tt77=tt36*tt32*jjtCoef;
  tt78=tt38*tt32*jjtCoef;
  tt79=tt40*tt32*jjtCoef+tt15*hCoef;
  tt80=tt42*tt32*jjtCoef+(tt47+a[1])*hCoef;
  tt81=tt7*tt32*jjtCoef;
  tt82=tt12*tt32*jjtCoef+tt31*hCoef;
  tt83=tt14*tt32*jjtCoef+tt4*hCoef;
  tt84=tt36*tt34*jjtCoef;
  tt85=tt38*tt34*jjtCoef+(tt50+a[2])*hCoef;
  tt86=tt40*tt34*jjtCoef;
  tt87=tt42*tt34*jjtCoef+tt9*hCoef;
  tt88=tt7*tt34*jjtCoef+tt6*hCoef;
  tt89=tt12*tt34*jjtCoef;
  tt90=tt14*tt34*jjtCoef+tt33*hCoef;
  tt91=tt36*tt38*jjtCoef+tt13*hCoef;
  tt92=tt36*tt40*jjtCoef+(tt62+a[0])*hCoef;
  tt93=tt42*tt36*jjtCoef;
  tt94=tt7*tt36*jjtCoef+tt35*hCoef;
  tt95=tt12*tt36*jjtCoef+tt10*hCoef;
  tt96=tt14*tt36*jjtCoef;
  tt97=tt40*tt38*jjtCoef;
  tt98=tt42*tt38*jjtCoef;
  tt99=tt7*tt38*jjtCoef;
  tt100=tt12*tt38*jjtCoef+tt2*hCoef;
  tt101=tt14*tt38*jjtCoef+tt37*hCoef;
  tt102=tt42*tt40*jjtCoef;
  tt103=tt7*tt40*jjtCoef+tt39*hCoef;
  tt104=tt12*tt40*jjtCoef;
  tt105=tt14*tt40*jjtCoef+tt11*hCoef;
  tt106=tt7*tt42*jjtCoef+tt5*hCoef;
  tt107=tt12*tt42*jjtCoef+tt41*hCoef;
  tt108=tt14*tt42*jjtCoef;
  tt109=tt12*tt7*jjtCoef;
  tt110=tt14*tt7*jjtCoef;
  tt111=tt14*tt12*jjtCoef;
  if(H) {
    (*H)(0,0)=pow(tt20,2)*jjtCoef;
    (*H)(0,1)=tt43;
    (*H)(0,2)=tt44;
    (*H)(0,3)=tt45;
    (*H)(0,4)=tt46;
    (*H)(0,5)=tt48;
    (*H)(0,6)=tt49;
    (*H)(0,7)=tt51;
    (*H)(0,8)=tt52;
    (*H)(0,9)=tt53;
    (*H)(0,10)=tt54;
    (*H)(0,11)=tt55;
    (*H)(1,0)=tt43;
    (*H)(1,1)=pow(tt25,2)*jjtCoef;
    (*H)(1,2)=tt56;
    (*H)(1,3)=tt57;
    (*H)(1,4)=tt58;
    (*H)(1,5)=tt59;
    (*H)(1,6)=tt60;
    (*H)(1,7)=tt61;
    (*H)(1,8)=tt63;
    (*H)(1,9)=tt64;
    (*H)(1,10)=tt65;
    (*H)(1,11)=tt66;
    (*H)(2,0)=tt44;
    (*H)(2,1)=tt56;
    (*H)(2,2)=pow(tt30,2)*jjtCoef;
    (*H)(2,3)=tt67;
    (*H)(2,4)=tt68;
    (*H)(2,5)=tt69;
    (*H)(2,6)=tt70;
    (*H)(2,7)=tt71;
    (*H)(2,8)=tt72;
    (*H)(2,9)=tt73;
    (*H)(2,10)=tt74;
    (*H)(2,11)=tt75;
    (*H)(3,0)=tt45;
    (*H)(3,1)=tt57;
    (*H)(3,2)=tt67;
    (*H)(3,3)=pow(tt32,2)*jjtCoef;
    (*H)(3,4)=tt76;
    (*H)(3,5)=tt77;
    (*H)(3,6)=tt78;
    (*H)(3,7)=tt79;
    (*H)(3,8)=tt80;
    (*H)(3,9)=tt81;
    (*H)(3,10)=tt82;
    (*H)(3,11)=tt83;
    (*H)(4,0)=tt46;
    (*H)(4,1)=tt58;
    (*H)(4,2)=tt68;
    (*H)(4,3)=tt76;
    (*H)(4,4)=pow(tt34,2)*jjtCoef;
    (*H)(4,5)=tt84;
    (*H)(4,6)=tt85;
    (*H)(4,7)=tt86;
    (*H)(4,8)=tt87;
    (*H)(4,9)=tt88;
    (*H)(4,10)=tt89;
    (*H)(4,11)=tt90;
    (*H)(5,0)=tt48;
    (*H)(5,1)=tt59;
    (*H)(5,2)=tt69;
    (*H)(5,3)=tt77;
    (*H)(5,4)=tt84;
    (*H)(5,5)=pow(tt36,2)*jjtCoef;
    (*H)(5,6)=tt91;
    (*H)(5,7)=tt92;
    (*H)(5,8)=tt93;
    (*H)(5,9)=tt94;
    (*H)(5,10)=tt95;
    (*H)(5,11)=tt96;
    (*H)(6,0)=tt49;
    (*H)(6,1)=tt60;
    (*H)(6,2)=tt70;
    (*H)(6,3)=tt78;
    (*H)(6,4)=tt85;
    (*H)(6,5)=tt91;
    (*H)(6,6)=pow(tt38,2)*jjtCoef;
    (*H)(6,7)=tt97;
    (*H)(6,8)=tt98;
    (*H)(6,9)=tt99;
    (*H)(6,10)=tt100;
    (*H)(6,11)=tt101;
    (*H)(7,0)=tt51;
    (*H)(7,1)=tt61;
    (*H)(7,2)=tt71;
    (*H)(7,3)=tt79;
    (*H)(7,4)=tt86;
    (*H)(7,5)=tt92;
    (*H)(7,6)=tt97;
    (*H)(7,7)=pow(tt40,2)*jjtCoef;
    (*H)(7,8)=tt102;
    (*H)(7,9)=tt103;
    (*H)(7,10)=tt104;
    (*H)(7,11)=tt105;
    (*H)(8,0)=tt52;
    (*H)(8,1)=tt63;
    (*H)(8,2)=tt72;
    (*H)(8,3)=tt80;
    (*H)(8,4)=tt87;
    (*H)(8,5)=tt93;
    (*H)(8,6)=tt98;
    (*H)(8,7)=tt102;
    (*H)(8,8)=pow(tt42,2)*jjtCoef;
    (*H)(8,9)=tt106;
    (*H)(8,10)=tt107;
    (*H)(8,11)=tt108;
    (*H)(9,0)=tt53;
    (*H)(9,1)=tt64;
    (*H)(9,2)=tt73;
    (*H)(9,3)=tt81;
    (*H)(9,4)=tt88;
    (*H)(9,5)=tt94;
    (*H)(9,6)=tt99;
    (*H)(9,7)=tt103;
    (*H)(9,8)=tt106;
    (*H)(9,9)=pow(tt7,2)*jjtCoef;
    (*H)(9,10)=tt109;
    (*H)(9,11)=tt110;
    (*H)(10,0)=tt54;
    (*H)(10,1)=tt65;
    (*H)(10,2)=tt74;
    (*H)(10,3)=tt82;
    (*H)(10,4)=tt89;
    (*H)(10,5)=tt95;
    (*H)(10,6)=tt100;
    (*H)(10,7)=tt104;
    (*H)(10,8)=tt107;
    (*H)(10,9)=tt109;
    (*H)(10,10)=pow(tt12,2)*jjtCoef;
    (*H)(10,11)=tt111;
    (*H)(11,0)=tt55;
    (*H)(11,1)=tt66;
    (*H)(11,2)=tt75;
    (*H)(11,3)=tt83;
    (*H)(11,4)=tt90;
    (*H)(11,5)=tt96;
    (*H)(11,6)=tt101;
    (*H)(11,7)=tt105;
    (*H)(11,8)=tt108;
    (*H)(11,9)=tt110;
    (*H)(11,10)=tt111;
    (*H)(11,11)=pow(tt14,2)*jjtCoef;
  }
  return E;
}
//VolumeStrainLimitingCubic
VolumeStrainLimitingCubic::VolumeStrainLimitingCubic():VolumeStrainLimitingQuadratic(typeid(VolumeStrainLimitingCubic).name()) {}
VolumeStrainLimitingCubic::VolumeStrainLimitingCubic(const Vec& x,sizeType a,sizeType b,sizeType c,sizeType d,const Vec2d& limit,scalarD coef):VolumeStrainLimitingQuadratic(typeid(VolumeStrainLimitingCubic).name())
{
  a*=3;
  b*=3;
  c*=3;
  d*=3;
  VecDIMd b2a=x.template segment<3>(b)-x.template segment<3>(a);
  VecDIMd c2a=x.template segment<3>(c)-x.template segment<3>(a);
  VecDIMd d2a=x.template segment<3>(d)-x.template segment<3>(a);
  scalar vol=b2a.cross(c2a).dot(d2a);
  if(vol < 0) {
    swap(c,d);
    vol=-vol;
  }
  _d0=-vol*limit[0];
  _d1=-vol*limit[1];
  _id << a,a+1,a+2,b,b+1,b+2,c,c+1,c+2,d,d+1,d+2;
  _coef=coef;
}
boost::shared_ptr<Serializable> VolumeStrainLimitingCubic::copy() const
{
  return boost::shared_ptr<Serializable>(new VolumeStrainLimitingCubic());
}
scalarD VolumeStrainLimitingCubic::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  Vec12d gradBlk;
  Mat12d hessBlk;
  scalarD vol=grada(x.segment<3>(_id[0]),x.segment<3>(_id[3]),x.segment<3>(_id[6]),x.segment<3>(_id[9]),fgrad ? &gradBlk : NULL,NULL,0,0);
  scalarD delta0=min<scalarD>(vol+_d0,0),delta1=max<scalarD>(vol+_d1,0);
  scalarD gradCoef=(delta1*delta1-delta0*delta0)*coef;
  if(delta0+delta1 == 0)
    return 0;
  if(fgrad) {
    Vec& gradI=fgrad->getMatrixI();
    for(sizeType d=0; d<12; d++)
      gradI[_id[d]]+=gradBlk[d]*gradCoef;
  }
  if(fhess) {
    grada(x.segment<3>(_id[0]),x.segment<3>(_id[3]),x.segment<3>(_id[6]),x.segment<3>(_id[9]),NULL,&hessBlk,gradCoef,2*(delta1-delta0)*coef);
    addBlockIndexed(*fhess,_id,hessBlk);
  }
  return (delta1*delta1*delta1-delta0*delta0*delta0)*coef/3;
}
//VolumeStrainLimitingSoftmax
VolumeStrainLimitingSoftmax::VolumeStrainLimitingSoftmax():VolumeStrainLimitingQuadratic(typeid(VolumeStrainLimitingSoftmax).name()) {}
VolumeStrainLimitingSoftmax::VolumeStrainLimitingSoftmax(const Vec& x,sizeType a,sizeType b,sizeType c,sizeType d,const Vec2d& limit,scalarD coef):VolumeStrainLimitingQuadratic(typeid(VolumeStrainLimitingSoftmax).name())
{
  a*=3;
  b*=3;
  c*=3;
  d*=3;
  VecDIMd b2a=x.template segment<3>(b)-x.template segment<3>(a);
  VecDIMd c2a=x.template segment<3>(c)-x.template segment<3>(a);
  VecDIMd d2a=x.template segment<3>(d)-x.template segment<3>(a);
  scalar vol=b2a.cross(c2a).dot(d2a);
  if(vol < 0) {
    swap(c,d);
    vol=-vol;
  }
  _softmaxReg=10/vol;
  _d0=-vol*limit[0]*_softmaxReg;
  _d1=-vol*limit[1]*_softmaxReg;
  _id << a,a+1,a+2,b,b+1,b+2,c,c+1,c+2,d,d+1,d+2;
  _coef=coef/_softmaxReg/_softmaxReg;
}
bool VolumeStrainLimitingSoftmax::read(istream& is,IOData* dat)
{
  VolumeStrainLimitingQuadratic::read(is,dat);
  readBinaryData(_softmaxReg,is);
  return is.good();
}
bool VolumeStrainLimitingSoftmax::write(ostream& os,IOData* dat) const
{
  VolumeStrainLimitingQuadratic::write(os,dat);
  writeBinaryData(_softmaxReg,os);
  return os.good();
}
boost::shared_ptr<Serializable> VolumeStrainLimitingSoftmax::copy() const
{
  return boost::shared_ptr<Serializable>(new VolumeStrainLimitingSoftmax());
}
scalarD VolumeStrainLimitingSoftmax::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  Vec12d gradBlk;
  Mat12d hessBlk;
  scalarD vol=VolumeStrainLimitingQuadratic::grada(x.segment<3>(_id[0]),x.segment<3>(_id[3]),x.segment<3>(_id[6]),x.segment<3>(_id[9]),fgrad ? &gradBlk : NULL,NULL,0,0)*_softmaxReg;
  scalarD delta0Exp=exp(-vol-_d0),delta1Exp=exp(vol+_d1);
  scalarD delta0=log(1+delta0Exp),delta1=log(1+delta1Exp),gradCoef=0,hessCoef=0;
  if(fgrad || fhess)
    gradCoef=(delta1*delta1Exp/(1+delta1Exp)-delta0*delta0Exp/(1+delta0Exp))*coef*_softmaxReg;
  if(fgrad) {
    Vec& gradI=fgrad->getMatrixI();
    for(sizeType d=0; d<12; d++)
      gradI[_id[d]]+=gradBlk[d]*gradCoef;
  }
  if(fhess) {
    hessCoef=((delta1+delta1Exp)*delta1Exp/((1+delta1Exp)*(1+delta1Exp))+
              (delta0+delta0Exp)*delta0Exp/((1+delta0Exp)*(1+delta0Exp)))*coef*_softmaxReg*_softmaxReg;
    grada(x.segment<3>(_id[0]),x.segment<3>(_id[3]),x.segment<3>(_id[6]),x.segment<3>(_id[9]),NULL,&hessBlk,gradCoef,hessCoef);
    addBlockIndexed(*fhess,_id,hessBlk);
  }
  return (delta1*delta1+delta0*delta0)*coef/2;
}
//VolumeStrainLimitingLog
VolumeStrainLimitingLog::VolumeStrainLimitingLog():VolumeStrainLimitingQuadratic(typeid(VolumeStrainLimitingLog).name()) {}
VolumeStrainLimitingLog::VolumeStrainLimitingLog(const Vec& x,sizeType a,sizeType b,sizeType c,sizeType d,const Vec2d& limit,scalarD coef):VolumeStrainLimitingQuadratic(typeid(VolumeStrainLimitingLog).name())
{
  a*=3;
  b*=3;
  c*=3;
  d*=3;
  VecDIMd b2a=x.template segment<3>(b)-x.template segment<3>(a);
  VecDIMd c2a=x.template segment<3>(c)-x.template segment<3>(a);
  VecDIMd d2a=x.template segment<3>(d)-x.template segment<3>(a);
  scalar vol=b2a.cross(c2a).dot(d2a);
  if(vol < 0) {
    swap(c,d);
    vol=-vol;
  }
  _d0=-vol*limit[0];
  _d1=-vol*limit[1];
  _id << a,a+1,a+2,b,b+1,b+2,c,c+1,c+2,d,d+1,d+2;
  _coef=coef;
}
boost::shared_ptr<Serializable> VolumeStrainLimitingLog::copy() const
{
  return boost::shared_ptr<Serializable>(new VolumeStrainLimitingLog());
}
scalarD VolumeStrainLimitingLog::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  Vec12d gradBlk;
  Mat12d hessBlk;
  scalarD vol=VolumeStrainLimitingQuadratic::grada(x.segment<3>(_id[0]),x.segment<3>(_id[3]),x.segment<3>(_id[6]),x.segment<3>(_id[9]),fgrad ? &gradBlk : NULL,NULL,0,0);
  scalarD gradCoef=0,hessCoef=0;
  if(fgrad || fhess)
    gradCoef=-coef/(vol+_d0)-coef/(vol+_d1);
  if(fgrad) {
    Vec& gradI=fgrad->getMatrixI();
    for(sizeType d=0; d<12; d++)
      gradI[_id[d]]+=gradBlk[d]*gradCoef;
  }
  if(fhess) {
    hessCoef=coef/pow(vol+_d0,2)+coef/pow(vol+_d1,2);
    grada(x.segment<3>(_id[0]),x.segment<3>(_id[3]),x.segment<3>(_id[6]),x.segment<3>(_id[9]),NULL,&hessBlk,gradCoef,hessCoef);
    addBlockIndexed(*fhess,_id,hessBlk);
  }
  return (-log(vol+_d0)-log(-vol-_d1))*coef;
}
