#include "LMCholmod.h"
#include <CommonFile/Timing.h>

USE_PRJ_NAMESPACE

#ifdef CHOLMOD_SUPPORT
CholmodWrapper::CholmodWrapper():_built(false),_same(false),_useEigen(false),_supernodal(true) {}
CholmodWrapper::~CholmodWrapper()
{
  clear();
}
boost::shared_ptr<LMInterface::LMDenseInterface> CholmodWrapper::copy() const
{
  return boost::shared_ptr<LMDenseInterface>(new CholmodWrapper());
}
bool CholmodWrapper::recompute(SMat& smat,scalarD shift,bool sameA)
{
  bool succ;
  TBEG("Factorize");
  if(_same && _built) {
    smat.makeCompressed();
    ASSERT_MSG(_A->nzmax == (size_t)smat.nonZeros(),"We must have same number of nonzeros in sameMode of CholmodWrapper!")
    if(!sameA)
      copyData(smat);
    succ=tryFactorize(smat,shift);
  } else {
    clear();
    cholmod_start(&_c);
    _c.supernodal=_supernodal ? CHOLMOD_SUPERNODAL :CHOLMOD_SIMPLICIAL;
    smat.makeCompressed();
    _A=cholmod_allocate_sparse((size_t)smat.rows(),(size_t)smat.cols(),(size_t)smat.nonZeros(),1,1,-1,CHOLMOD_REAL,&_c);
    ASSERT_MSG(_c.status == CHOLMOD_OK,"Create sparse failed!")
    int* P=(int*)_A->p;
    int* I=(int*)_A->i;
    ASSERT(_A->itype == CHOLMOD_INT)
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<=smat.rows(); i++)
      P[i]=smat.outerIndexPtr()[i];
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<smat.nonZeros(); i++)
      I[i]=smat.innerIndexPtr()[i];
    copyData(smat);

    _L=cholmod_analyze(_A,&_c);
    ASSERT_MSG(_c.status == CHOLMOD_OK,"Analyze failed!")
    succ=tryFactorize(smat,shift);
    _built=true;
  }
  TEND();
  return succ;
}
Matd CholmodWrapper::solve(const Matd& b)
{
  TBEG("Solve");
  if(_c.status != CHOLMOD_OK) {
    if(_useEigen)
      _ret=_eigenSol.solve(b);
    else {
      ASSERT_MSG(false,"Cannot solve with cholmod_common.status != CHOLMOD_OK.")
    }
  } else {
    ASSERT(b.rows() == (sizeType)_A->nrow)
    cholmod_dense* B=cholmod_zeros(_A->nrow,b.cols(),_A->xtype,&_c);
    Eigen::Map<Eigen::Matrix<double,-1,-1> >((double*)B->x,B->nrow,B->ncol)=b.cast<double>();

    cholmod_dense* X=cholmod_solve(CHOLMOD_A,_L,B,&_c);
    _ret=Eigen::Map<Eigen::Matrix<double,-1,-1> >((double*)X->x,B->nrow,B->ncol).cast<scalarD>();
    ASSERT_MSG(_c.status == CHOLMOD_OK,"Solve failed!")

    cholmod_free_dense(&B,&_c);
    cholmod_free_dense(&X,&_c);
  }
  TEND();
  return _ret;
}
void CholmodWrapper::setSupernodal(bool super)
{
  _supernodal=super;
  clear();
}
void CholmodWrapper::setSameStruct(bool same)
{
  _same=same;
}
void CholmodWrapper::setUseEigen(bool useEigen)
{
  _useEigen=useEigen;
}
void CholmodWrapper::debug()
{
#define N 10
  Matd A(N,N);
  A.setRandom();
  A=(A.transpose()*A).eval();

  SMat AS;
  AS.resize(N,N);
  for(sizeType r=0; r<N; r++)
    for(sizeType c=0; c<N; c++)
      AS.coeffRef(r,c)=A(r,c);

  scalarD shift=0.1f;
  Cold B=Cold::Random(N),X,X2;
  recompute(AS,shift,false);
  X=solve(B);
  X2=(A+Matd::Identity(N,N)*shift).llt().solve(B);
  INFOV("X: %f Err: %f",X.norm(),(X-X2).norm())

  shift=0.2f;
  recompute(AS,shift,false);
  X=solve(B);
  X2=(A+Matd::Identity(N,N)*shift).llt().solve(B);
  INFOV("X: %f Err: %f",X.norm(),(X-X2).norm())
#undef N
}
void CholmodWrapper::copyData(const SMat& smat)
{
  if(sizeof(SMat::Scalar) == sizeof(double))
    memcpy(_A->x,smat.valuePtr(),sizeof(double)*smat.nonZeros());
  else {
    Eigen::Matrix<double,-1,1> tmp=Eigen::Map<const Cold>(smat.valuePtr(),smat.nonZeros()).cast<double>();
    memcpy(_A->x,tmp.data(),sizeof(double)*smat.nonZeros());
  }
}
bool CholmodWrapper::tryFactorize(SMat& smat,scalarD shift)
{
  double beta[2]= {(double)shift,0};
  cholmod_factorize_p(_A,beta,NULL,0,_L,&_c);
  if(_c.status != CHOLMOD_OK) {
    WARNING("Factorize failed, fallback to eigen!")
    if(_useEigen) {
      _eigenSol.setShift((double)shift);
      _eigenSol.compute(smat);
      return true;
    } else return false;
  } else {
    return true;
  }
}
void CholmodWrapper::clear()
{
  if(_built) {
    if(_L)
      cholmod_free_factor(&_L,&_c);
    cholmod_free_sparse(&_A,&_c);
    cholmod_finish(&_c);
    _built=false;
  }
}
#endif
