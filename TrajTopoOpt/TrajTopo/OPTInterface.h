#ifndef OPT_INTERFACE_H
#define OPT_INTERFACE_H

#include <CommonFile/MathBasic.h>
#include <CommonFile/solvers/Objective.h>
#include <CommonFile/solvers/Callback.h>
#include <boost/shared_ptr.hpp>

namespace alglib
{
struct minqpstate;
struct minqpreport;
struct minbleicstate;
struct minbleicreport;
}

PRJ_BEGIN

class CommonInterface
{
public:
  CommonInterface();
  virtual void reset(sizeType sz,bool hasCI)=0;
  virtual void setCI(const Matd& CI,const Cold& CI0,const Coli& type)=0;
  virtual void setCI(const Matd& CI,const Cold& CI0,sizeType type);
  virtual void setCB(const Cold& L,const Cold& U)=0;
  virtual void setCLB(const Cold& L);
  virtual void setCUB(const Cold& U);
  virtual bool solve(Cold& x)=0;
  virtual void setTolG(scalarD tolg);
  virtual void setTolF(scalarD tolf);
  virtual void setTolX(scalarD tolx);
  virtual void setMaxIter(sizeType maxIter);
protected:
  //data
  sizeType _maxIter;
  scalarD _tolg,_tolf,_tolx;
  sizeType _sz;
  bool _hasCI;
};
class BLEICInterface : public CommonInterface, public Objective<scalarD>
{
public:
  BLEICInterface();
  virtual void reset(sizeType sz,bool hasCI);
  virtual void setCI(const Matd& CI,const Cold& CI0,const Coli& type);
  virtual void setCB(const Cold& L,const Cold& U);
  virtual void operator()(Eigen::Map<const Cold>& x,scalarD& FX,Eigen::Map<Cold>& DFDX)=0;
  virtual void gradient(const Cold& x,scalarD& FX,Cold& DFDX);
  virtual void report(const Cold& x,scalarD& FX);
  virtual bool solveCommonFile(Cold& x,Callback<scalarD,Kernel<scalarD> >& cb);
  virtual bool solve(Cold& x);
  sizeType getTermination() const;
  sizeType getIterationsCount() const;
  scalarD getFunctionValue() const;
  void debugGradient(Cold x,const Cold& mask);
  //Objective interface
  virtual int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient);
  virtual int inputs() const;
private:
  //data
  boost::shared_ptr<alglib::minbleicstate> _state;
  boost::shared_ptr<alglib::minbleicreport> _rep;
};

PRJ_END

#endif
