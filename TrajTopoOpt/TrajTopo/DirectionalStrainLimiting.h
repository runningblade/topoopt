#ifndef DIRECTIONAL_STRAIN_LIMITING_H
#define DIRECTIONAL_STRAIN_LIMITING_H

#include "ElasticModel.h"

PRJ_BEGIN

//Directional Strain Limiting
template <int DIM>
class DirectionalStrainLimitingQuadratic : public ElasticModel<DIM>
{
public:
  using typename Objective<scalarD>::Vec;
  using typename Objective<scalarD>::STrips;
  using typename ElasticModel<DIM>::VecDIMd;
  using typename ElasticModel<DIM>::MatDIMd;
  using typename ElasticModel<DIM>::SVecs;
  DirectionalStrainLimitingQuadratic();
  DirectionalStrainLimitingQuadratic(const string& name);
  DirectionalStrainLimitingQuadratic(const Vec& x,sizeType a,sizeType b,const Vec2d& limit,scalarD coef);
  virtual void writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const override;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual Coli ids() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
  virtual int inputs() const override;
protected:
  static scalarD gradl(const VecDIMd& l,VecDIMd* dl,MatDIMd* ddl);
  scalarD _d0,_d1,_coef;
  sizeType _a,_b;
};
template <int DIM>
class DirectionalStrainLimitingCubic : public DirectionalStrainLimitingQuadratic<DIM>
{
public:
  using typename DirectionalStrainLimitingQuadratic<DIM>::Vec;
  using typename DirectionalStrainLimitingQuadratic<DIM>::STrips;
  using typename DirectionalStrainLimitingQuadratic<DIM>::VecDIMd;
  using typename DirectionalStrainLimitingQuadratic<DIM>::MatDIMd;
  using typename DirectionalStrainLimitingQuadratic<DIM>::SVecs;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_d0;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_d1;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_coef;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_a;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_b;
  DirectionalStrainLimitingCubic();
  DirectionalStrainLimitingCubic(const Vec& x,sizeType a,sizeType b,const Vec2d& limit,scalarD coef);
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
};
template <int DIM>
class DirectionalStrainLimitingSoftmax : public DirectionalStrainLimitingQuadratic<DIM>
{
public:
  using typename DirectionalStrainLimitingQuadratic<DIM>::Vec;
  using typename DirectionalStrainLimitingQuadratic<DIM>::STrips;
  using typename DirectionalStrainLimitingQuadratic<DIM>::VecDIMd;
  using typename DirectionalStrainLimitingQuadratic<DIM>::MatDIMd;
  using typename DirectionalStrainLimitingQuadratic<DIM>::SVecs;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_d0;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_d1;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_coef;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_a;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_b;
  DirectionalStrainLimitingSoftmax();
  DirectionalStrainLimitingSoftmax(const Vec& x,sizeType a,sizeType b,const Vec2d& limit,scalarD coef);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
protected:
  scalarD _softmaxReg;
};
template <int DIM>
class DirectionalStrainLimitingLog : public DirectionalStrainLimitingQuadratic<DIM>
{
public:
  using typename DirectionalStrainLimitingQuadratic<DIM>::Vec;
  using typename DirectionalStrainLimitingQuadratic<DIM>::STrips;
  using typename DirectionalStrainLimitingQuadratic<DIM>::VecDIMd;
  using typename DirectionalStrainLimitingQuadratic<DIM>::MatDIMd;
  using typename DirectionalStrainLimitingQuadratic<DIM>::SVecs;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_d0;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_d1;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_coef;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_a;
  using NAMESPACE::DirectionalStrainLimitingQuadratic<DIM>::_b;
  DirectionalStrainLimitingLog();
  DirectionalStrainLimitingLog(const Vec& x,sizeType a,sizeType b,const Vec2d& limit,scalarD coef);
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
};

PRJ_END

#endif
