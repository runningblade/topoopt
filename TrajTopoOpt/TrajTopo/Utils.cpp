#include "Utils.h"
#include <CommonFile/ParallelPoissonDiskSampling.h>
#include <CommonFile/geom/BVHBuilder.h>
#include <CommonFile/solvers/AINVPreconditioner.h>
#include <CommonFile/solvers/DACGSolver.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/unordered_set.hpp>
#include <boost/lexical_cast.hpp>

extern "C"
{
  /* Subroutine */
  int nnls_(double *a,int *mda,int *m,int *n,
            double *b,double *x,double *rnorm,double *w,double *zz,
            int *index,int *mode);
}

PRJ_BEGIN

//hash
size_t Hash::operator()(const Vec2i& key) const
{
  boost::hash<sizeType> h;
  return h(key[0])+h(key[1]);
}
size_t Hash::operator()(const Vec3i& key) const
{
  boost::hash<sizeType> h;
  return h(key[0])+h(key[1])+h(key[2]);
}
size_t Hash::operator()(const Vec4i& key) const
{
  boost::hash<sizeType> h;
  return h(key[0])+h(key[1])+h(key[2])+h(key[3]);
}
size_t Hash::operator()(const Coli& key) const
{
  size_t ret=0;
  boost::hash<sizeType> h;
  for(sizeType i=0; i<key.size(); i++)
    ret+=h(key[i]);
  return ret;
}
//filesystem
bool notDigit(char c)
{
  return !std::isdigit(c);
}
bool lessDirByNumber(boost::filesystem::path A,boost::filesystem::path B)
{
  string strA=A.string();
  string::iterator itA=std::remove_if(strA.begin(),strA.end(),notDigit);
  strA.erase(itA,strA.end());

  string strB=B.string();
  string::iterator itB=std::remove_if(strB.begin(),strB.end(),notDigit);
  strB.erase(itB,strB.end());

  return boost::lexical_cast<sizeType>(strA) < boost::lexical_cast<sizeType>(strB);
}
bool exists(const boost::filesystem::path& path)
{
  return boost::filesystem::exists(path);
}
void removeDir(const boost::filesystem::path& path)
{
  if(boost::filesystem::exists(path))
    try {
      boost::filesystem::remove_all(path);
    } catch(...) {}
}
void create(const boost::filesystem::path& path)
{
  if(!boost::filesystem::exists(path))
    try {
      boost::filesystem::create_directory(path);
    } catch(...) {}
}
void recreate(const boost::filesystem::path& path)
{
  removeDir(path);
  try {
    boost::filesystem::create_directory(path);
  } catch(...) {}
}
vector<boost::filesystem::path> files(const boost::filesystem::path& path)
{
  vector<boost::filesystem::path> ret;
  for(boost::filesystem::directory_iterator beg(path),end; beg!=end; beg++)
    if(boost::filesystem::is_regular_file(*beg))
      ret.push_back(*beg);
  return ret;
}
vector<boost::filesystem::path> directories(const boost::filesystem::path& path)
{
  vector<boost::filesystem::path> ret;
  for(boost::filesystem::directory_iterator beg(path),end; beg!=end; beg++)
    if(boost::filesystem::is_directory(*beg))
      ret.push_back(*beg);
  return ret;
}
void sortFilesByNumber(vector<boost::filesystem::path>& files)
{
  sort(files.begin(),files.end(),lessDirByNumber);
}
bool isDir(const boost::filesystem::path& path)
{
  return boost::filesystem::is_directory(path);
}
size_t fileSize(const boost::filesystem::path& path)
{
  return (size_t)boost::filesystem::file_size(path);
}
string parseProps(sizeType argc,char** argv,boost::property_tree::ptree& pt)
{
  string addParam;
  for(sizeType i=0; i<argc; i++) {
    string str(argv[i]);
    size_t pos=str.find("=");
    if(pos != std::string::npos) {
      pt.put<string>(str.substr(0,pos),str.substr(pos+1));
      addParam+="_"+str;
    }
  }
  return addParam+"_";
}
//property tree
void readPtreeBinary(boost::property_tree::ptree& pt,istream& is)
{
  string str;
  readBinaryData(str,is);
  istringstream iss(str);
  boost::property_tree::read_xml(iss,pt);
}
void writePtreeBinary(const boost::property_tree::ptree& pt,ostream& os)
{
  ostringstream oss;
  boost::property_tree::write_xml(oss,pt);
  writeBinaryData(oss.str(),os);
}
void readPtreeAscii(boost::property_tree::ptree& pt,const boost::filesystem::path& path)
{
  boost::property_tree::read_xml(path.string(),pt);
}
void writePtreeAscii(const boost::property_tree::ptree& pt,const boost::filesystem::path& path)
{
#if BOOST_VERSION >= 105600
  boost::property_tree::xml_writer_settings<string> settings('\t',1);
  boost::property_tree::write_xml(path.string(),pt,std::locale(),settings);
#else
  boost::property_tree::xml_writer_settings<boost::property_tree::ptree::key_type::value_type> settings('\t',1);
  boost::property_tree::write_xml(path.string(),pt,std::locale(),settings);
#endif
}
//deformation gradient
void calcGComp2D(Mat9X12d& GComp,const Mat3d& d)
{
  //F=DH*invDM
  GComp.setZero();
  for(sizeType r=0; r<2; r++)
    for(sizeType c=0; c<2; c++)
      for(sizeType i=0; i<2; i++) {
        //DH(r,i)*invDM(i,c)=(x(i+1)(r)-x1(r))*invDM(i,c)
        GComp(c*3+r,(i+1)*3+r)+=d(i,c);
        GComp(c*3+r,r)-=d(i,c);
      }
}
void calcGComp3D(Mat9X12d& GComp,const Mat3d& d)
{
  //F=DH*invDM
  GComp.setZero();
  for(sizeType r=0; r<3; r++)
    for(sizeType c=0; c<3; c++)
      for(sizeType i=0; i<3; i++) {
        //DH(r,i)*invDM(i,c)=(x(i+1)(r)-x1(r))*invDM(i,c)
        GComp(c*3+r,(i+1)*3+r)+=d(i,c);
        GComp(c*3+r,r)-=d(i,c);
      }
}

PRJ_END
