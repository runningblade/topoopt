#include "BuildScenario.h"
#include "SpringModel.h"
#include "FEMModel.h"
#include "FEMModelLinear.h"
#include "GroundContact.h"
#include "AreaStrainLimiting.h"
#include "DirectionalStrainLimiting.h"
#include "Utils.h"

PRJ_BEGIN

#define MAXID(ID) (res[ID]-1)
#define GRD(ID) (sz[ID]/MAXID(ID))
//2D
#define POS2D(I,J) VecDIMd((I)*GRD(0)-sz[0]/2,(J)*GRD(1))
#define GI2D(I,J) ((I)*res[1]+(J))
#define ISVALID2D(I,J) ((I) >= 0 && (I) <= MAXID(0) && (J) >= 0 && (J) <= MAXID(1))
//3D
#define POS3D(I,J,K) VecDIMd((I)*GRD(0)-sz[0]/2,(J)*GRD(1),(K)*GRD(2)-sz[2]/2)
#define GI3D(I,J,K) ((I)*res[1]*res[2]+(J)*res[2]+(K))
#define ISVALID3D(I,J,K) ((I) >= 0 && (I) <= MAXID(0) && (J) >= 0 && (J) <= MAXID(1) && (K) >= 0 && (K) <= MAXID(2))
//build scenario
template <int DIM>
BuildScenario<DIM>::BuildScenario(bool quadratic,bool quadraticContact)
{
  //material
  _pt.put<scalarD>("coefStiff",0.45f);
  _pt.put<scalarD>("coefK",1E3f);
  _pt.put<scalarD>("coefM",1);
  _pt.put<bool>("linear",false);
  //limit
  _pt.put<scalarD>("coefStrainLimiting",1E8f);
  if(quadratic)
    _pt.put<sizeType>("limitType",AREA_QUADRATIC);
  else _pt.put<sizeType>("limitType",AREA_SOFTMAX);
  assignVec(Vec2d(0.95f,1.05f),"limit",_pt);
  //environment
  assignVec(Vec3d(0,-9.81f,0),"g",_pt);
  if(quadraticContact) {
    _pt.put<scalarD>("coefGroundK",1E4f);
    _pt.put<sizeType>("contactType",CONTACT_QUADRATIC);
  } else {
    _pt.put<scalarD>("coefGroundK",1E6f);
    _pt.put<sizeType>("contactType",CONTACT_CUBIC);
  }
}
template <>
void BuildScenario<2>::buildDirectionalStrainLimits(DynamicModel<2>& model,const VecDIMd& sz,const VecDIMi& res)
{
#define ADDLIMITS(I0,J0,I1,J1) if(ISVALID2D(I0,J0) && ISVALID2D(I1,J1) && GI2D(I0,J0) < GI2D(I1,J1)) { \
  if(limitType == DIRECTIONAL_QUADRATIC) \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<2> >(new DirectionalStrainLimitingQuadratic<2>(model.getXP(),GI2D(I0,J0),GI2D(I1,J1),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
  else if(limitType == DIRECTIONAL_CUBIC)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<2> >(new DirectionalStrainLimitingCubic<2>(model.getXP(),GI2D(I0,J0),GI2D(I1,J1),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
  else if(limitType == DIRECTIONAL_SOFTMAX)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<2> >(new DirectionalStrainLimitingSoftmax<2>(model.getXP(),GI2D(I0,J0),GI2D(I1,J1),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
  else if(limitType == DIRECTIONAL_LOG)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<2> >(new DirectionalStrainLimitingLog<2>(model.getXP(),GI2D(I0,J0),GI2D(I1,J1),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
}
  model.getStss().clear();
  Vec2d limit=parseVec<Vec2d>("limit",_pt);
  sizeType limitType=_pt.get<sizeType>("limitType");
  for(sizeType i=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++)
      for(sizeType ii=i-1; ii<=i+1; ii++)
        for(sizeType jj=j-1; jj<=j+1; jj++) {
          ADDLIMITS(i,j,ii,jj)
        }
#undef ADDLIMITS
}
template <>
void BuildScenario<3>::buildDirectionalStrainLimits(DynamicModel<3>& model,const VecDIMd& sz,const VecDIMi& res)
{
#define ADDLIMITS(I0,J0,K0,I1,J1,K1) if(ISVALID3D(I0,J0,K0) && ISVALID3D(I1,J1,K1) && GI3D(I0,J0,K0) < GI3D(I1,J1,K1)) { \
  if(limitType == DIRECTIONAL_QUADRATIC) \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<3> >(new DirectionalStrainLimitingQuadratic<3>(model.getXP(),GI3D(I0,J0,K0),GI3D(I1,J1,K1),limit,_pt.get<scalarD>("coefStrainLimiting"))));  \
  else if(limitType == DIRECTIONAL_CUBIC)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<3> >(new DirectionalStrainLimitingCubic<3>(model.getXP(),GI3D(I0,J0,K0),GI3D(I1,J1,K1),limit,_pt.get<scalarD>("coefStrainLimiting"))));  \
  else if(limitType == DIRECTIONAL_SOFTMAX)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<3> >(new DirectionalStrainLimitingSoftmax<3>(model.getXP(),GI3D(I0,J0,K0),GI3D(I1,J1,K1),limit,_pt.get<scalarD>("coefStrainLimiting"))));  \
  else if(limitType == DIRECTIONAL_LOG)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<3> >(new DirectionalStrainLimitingLog<3>(model.getXP(),GI3D(I0,J0,K0),GI3D(I1,J1,K1),limit,_pt.get<scalarD>("coefStrainLimiting"))));  \
}
  model.getStss().clear();
  Vec2d limit=parseVec<Vec2d>("limit",_pt);
  sizeType limitType=_pt.get<sizeType>("limitType");
  for(sizeType i=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++)
      for(sizeType k=0; k<=MAXID(2); k++)
        for(sizeType ii=i-1; ii<=i+1; ii++)
          for(sizeType jj=j-1; jj<=j+1; jj++)
            for(sizeType kk=k-1; kk<=k+1; kk++) {
              ADDLIMITS(i,j,k,ii,jj,kk)
            }
#undef ADDLIMITS
}
template <>
void BuildScenario<2>::buildAreaStrainLimits(DynamicModel<2>& model,const VecDIMd& sz,const VecDIMi& res)
{
#define ADDLIMITS(I0,J0,I1,J1,I2,J2) if(ISVALID2D(I0,J0) && ISVALID2D(I1,J1) && ISVALID2D(I2,J2)) { \
  if(limitType == AREA_QUADRATIC) \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<2> > \
    (new AreaStrainLimitingQuadratic(model.getXP(),GI2D(I0,J0),GI2D(I1,J1),GI2D(I2,J2),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
  else if(limitType == AREA_CUBIC)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<2> > \
    (new AreaStrainLimitingCubic(model.getXP(),GI2D(I0,J0),GI2D(I1,J1),GI2D(I2,J2),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
  else if(limitType == AREA_SOFTMAX)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<2> > \
    (new AreaStrainLimitingSoftmax(model.getXP(),GI2D(I0,J0),GI2D(I1,J1),GI2D(I2,J2),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
  else if(limitType == AREA_LOG)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<2> > \
    (new AreaStrainLimitingLog(model.getXP(),GI2D(I0,J0),GI2D(I1,J1),GI2D(I2,J2),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
}
  model.getStss().clear();
  Vec2d limit=parseVec<Vec2d>("limit",_pt);
  sizeType limitType=_pt.get<sizeType>("limitType");
  for(sizeType i=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++) {
      //pair I
      ADDLIMITS(i,j,     i+1,j, i,j+1)
      ADDLIMITS(i+1,j+1, i+1,j, i,j+1)
      //pair II
      ADDLIMITS(i,j+1,   i,j, i+1,j+1)
      ADDLIMITS(i+1,j,   i,j, i+1,j+1)
    }
#undef ADDLIMITS
}
template <>
void BuildScenario<3>::buildAreaStrainLimits(DynamicModel<3>& model,const VecDIMd& sz,const VecDIMi& res)
{
#define ADDLIMITS(I0,J0,K0,DX,DY,DZ) if(ISVALID3D(I0,J0,K0) && ISVALID3D(I0+DX,J0,K0) && ISVALID3D(I0,J0+DY,K0) && ISVALID3D(I0,J0,K0+DZ)) { \
  if(limitType == AREA_QUADRATIC) \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<3> > \
    (new VolumeStrainLimitingQuadratic(model.getXP(),GI3D(I0,J0,K0),GI3D(I0+DX,J0,K0),GI3D(I0,J0+DY,K0),GI3D(I0,J0,K0+DZ),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
  else if(limitType == AREA_CUBIC)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<3> > \
    (new VolumeStrainLimitingCubic(model.getXP(),GI3D(I0,J0,K0),GI3D(I0+DX,J0,K0),GI3D(I0,J0+DY,K0),GI3D(I0,J0,K0+DZ),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
  else if(limitType == AREA_SOFTMAX)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<3> > \
    (new VolumeStrainLimitingSoftmax(model.getXP(),GI3D(I0,J0,K0),GI3D(I0+DX,J0,K0),GI3D(I0,J0+DY,K0),GI3D(I0,J0,K0+DZ),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
  else if(limitType == AREA_LOG)  \
    model.getStss().push_back(boost::shared_ptr<ElasticModel<3> > \
    (new VolumeStrainLimitingLog(model.getXP(),GI3D(I0,J0,K0),GI3D(I0+DX,J0,K0),GI3D(I0,J0+DY,K0),GI3D(I0,J0,K0+DZ),limit,_pt.get<scalarD>("coefStrainLimiting")))); \
}
  model.getStss().clear();
  Vec2d limit=parseVec<Vec2d>("limit",_pt);
  sizeType limitType=_pt.get<sizeType>("limitType");
  for(sizeType i=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++)
      for(sizeType k=0; k<=MAXID(2); k++) {
        ADDLIMITS(i,j,k, 1, 1, 1)
        ADDLIMITS(i,j,k,-1, 1, 1)
        ADDLIMITS(i,j,k, 1,-1, 1)
        ADDLIMITS(i,j,k,-1,-1, 1)
        ADDLIMITS(i,j,k, 1, 1,-1)
        ADDLIMITS(i,j,k,-1, 1,-1)
        ADDLIMITS(i,j,k, 1,-1,-1)
        ADDLIMITS(i,j,k,-1,-1,-1)
      }
#undef ADDLIMITS
}
template <int DIM>
void BuildScenario<DIM>::buildGroundContact(DynamicModel<DIM>& model,const VecDIMd& sz,const VecDIMi& res)
{
  const Coli& css=model.getContactPoints();
  scalarD groundK=_pt.get<scalarD>("coefGroundK");
  sizeType contactType=_pt.get<sizeType>("contactType");
  scalarD szCell=(sz.array()/res.template cast<scalarD>().array()).matrix().maxCoeff();
  for(sizeType i=0; i<css.size(); i++) {
    VecDIMd g=model.getG().template segment<DIM>(css[i]*DIM);
    if(contactType == CONTACT_QUADRATIC)
      model.getStss().push_back(boost::shared_ptr<ElasticModel<DIM> >(new GroundContactQuadratic<DIM>(model.getXP(),css[i],g,groundK)));
    else if(contactType == CONTACT_CUBIC)
      model.getStss().push_back(boost::shared_ptr<ElasticModel<DIM> >(new GroundContactCubic<DIM>(model.getXP(),css[i],g,groundK)));
    else if(contactType == CONTACT_SOFTMAX)
      model.getStss().push_back(boost::shared_ptr<ElasticModel<DIM> >(new GroundContactSoftmax<DIM>(model.getXP(),css[i],g,groundK,szCell)));
    else if(contactType == CONTACT_FIX)
      model.getStss().push_back(boost::shared_ptr<ElasticModel<DIM> >(new GroundContactFix<DIM>(model.getXP(),css[i],g,groundK)));
  }
}
template <>
DynamicModel<2> BuildScenario<2>::buildSpringGrid(const VecDIMd& sz,const VecDIMi& res)
{
#define ADDCONS(I0,J0,I1,J1) \
if(ISVALID2D(I0,J0) && ISVALID2D(I1,J1) && GI2D(I0,J0) < GI2D(I1,J1)) \
model.getPss().push_back(boost::shared_ptr<ElasticModel<2> >  \
  (new SpringModel<2>(model.getXP(),_pt.get<scalarD>("coefK"),_pt.get<scalarD>("coefM"),GI2D(I0,J0),GI2D(I1,J1),c++)));
  //point position
  DynamicModel<2> model;
  Vec2d g=parseVec<Vec2d>("g",_pt);
  model.getContactPoints().resize(res[0]);
  model.getF()=model.getX0()=Vec::Zero(res.prod()*2);
  for(sizeType i=0,cc=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++) {
      model.getX0().segment<2>(GI2D(i,j)*2)=POS2D(i,j);
      model.getF().segment<2>(GI2D(i,j)*2)=g;
      if(j == 0)
        model.getContactPoints()[cc++]=GI2D(i,j);
    }
  model.getG()=model.getF();
  model.getXN()=model.getX()=model.getXP()=model.getX0();
  //kinetic energy
  sizeType c=0;
  model.getPss().clear();
  for(sizeType i=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++)
      for(sizeType ii=i-1; ii<=i+1; ii++)
        for(sizeType jj=j-1; jj<=j+1; jj++) {
          ADDCONS(i,j,ii,jj)
        }
  model.getCoefPss().setOnes(model.getPss().size());
  buildDirectionalStrainLimits(model,sz,res);
  buildAreaStrainLimits(model,sz,res);
  buildGroundContact(model,sz,res);
  return model;
#undef ADDCONS
}
template <>
DynamicModel<3> BuildScenario<3>::buildSpringGrid(const VecDIMd& sz,const VecDIMi& res)
{
#define ADDCONS(I0,J0,K0,I1,J1,K1) \
if(ISVALID3D(I0,J0,K0) && ISVALID3D(I1,J1,K1) && GI3D(I0,J0,K0) < GI3D(I1,J1,K1)) \
model.getPss().push_back(boost::shared_ptr<ElasticModel<3> >  \
  (new SpringModel<3>(model.getXP(),_pt.get<scalarD>("coefK"),_pt.get<scalarD>("coefM"),GI3D(I0,J0,K0),GI3D(I1,J1,K1),c++)));
  //point position
  DynamicModel<3> model;
  Vec3d g=parseVec<Vec3d>("g",_pt);
  model.getContactPoints().resize(res[0]*res[2]);
  model.getF()=model.getX0()=Vec::Zero(res.prod()*3);
  for(sizeType i=0,cc=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++)
      for(sizeType k=0; k<=MAXID(2); k++) {
        model.getX0().segment<3>(GI3D(i,j,k)*3)=POS3D(i,j,k);
        model.getF().segment<3>(GI3D(i,j,k)*3)=g;
        if(j == 0)
          model.getContactPoints()[cc++]=GI3D(i,j,k);
      }
  model.getG()=model.getF();
  model.getXN()=model.getX()=model.getXP()=model.getX0();
  //kinetic energy
  sizeType c=0;
  model.getPss().clear();
  for(sizeType i=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++)
      for(sizeType k=0; k<=MAXID(2); k++)
        for(sizeType ii=i-1; ii<=i+1; ii++)
          for(sizeType jj=j-1; jj<=j+1; jj++)
            for(sizeType kk=k-1; kk<=k+1; kk++) {
              ADDCONS(i,j,k,ii,jj,kk)
            }
  model.getCoefPss().setOnes(model.getPss().size());
  buildDirectionalStrainLimits(model,sz,res);
  buildAreaStrainLimits(model,sz,res);
  buildGroundContact(model,sz,res);
  return model;
#undef ADDCONS
}
template <>
DynamicModel<2> BuildScenario<2>::buildFEMGrid(const VecDIMd& sz,const VecDIMi& res)
{
#define ADDCONS(I,J,MODEL)  \
if(ISVALID2D(I,J) && ISVALID2D(I+1,J) && ISVALID2D(I,J+1) && ISVALID2D(I+1,J+1)) \
  model.getPss().push_back(boost::shared_ptr<ElasticModel<2> >  \
  (new MODEL<2>(model.getXP(),_pt.get<scalarD>("coefStiff"),_pt.get<scalarD>("coefK"),_pt.get<scalarD>("coefM"),  \
   GI2D(I,J),GI2D(I+1,J),GI2D(I,J+1),GI2D(I+1,J+1),c++)));
  //point position
  DynamicModel<2> model;
  Vec2d g=parseVec<Vec2d>("g",_pt);
  model.getContactPoints().resize(res[0]);
  model.getF()=model.getX0()=Vec::Zero(res.prod()*2);
  for(sizeType i=0,cc=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++) {
      model.getX0().segment<2>(GI2D(i,j)*2)=POS2D(i,j);
      model.getF().segment<2>(GI2D(i,j)*2)=g;
      if(j == 0)
        model.getContactPoints()[cc++]=GI2D(i,j);
    }
  model.getG()=model.getF();
  model.getXN()=model.getX()=model.getXP()=model.getX0();
  //kinetic energy
  sizeType c=0;
  model.getPss().clear();
  for(sizeType i=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++)
      if(_pt.get<bool>("linear")) {
        ADDCONS(i,j,FEMModelLinear)
      } else {
        ADDCONS(i,j,FEMModel)
      }
  model.getCoefPss().setOnes(model.getPss().size());
  buildDirectionalStrainLimits(model,sz,res);
  buildAreaStrainLimits(model,sz,res);
  buildGroundContact(model,sz,res);
  return model;
#undef ADDCONS
}
template <>
DynamicModel<3> BuildScenario<3>::buildFEMGrid(const VecDIMd& sz,const VecDIMi& res)
{
#define ADDCONS(I,J,K,MODEL)  \
if(ISVALID3D(I,J,K) && ISVALID3D(I+1,J,K) && ISVALID3D(I,J+1,K) && ISVALID3D(I+1,J+1,K) &&  \
   ISVALID3D(I,J,K+1) && ISVALID3D(I+1,J,K+1) && ISVALID3D(I,J+1,K+1) && ISVALID3D(I+1,J+1,K+1)) \
  model.getPss().push_back(boost::shared_ptr<ElasticModel<3> >  \
  (new MODEL<3>(model.getXP(),_pt.get<scalarD>("coefStiff"),_pt.get<scalarD>("coefK"),_pt.get<scalarD>("coefM"), \
   GI3D(I,J,K),GI3D(I+1,J,K),GI3D(I,J+1,K),GI3D(I+1,J+1,K),  \
   GI3D(I,J,K+1),GI3D(I+1,J,K+1),GI3D(I,J+1,K+1),GI3D(I+1,J+1,K+1),c++)));
  //point position
  DynamicModel<3> model;
  Vec3d g=parseVec<Vec3d>("g",_pt);
  model.getContactPoints().resize(res[0]*res[2]);
  model.getF()=model.getX0()=Vec::Zero(res.prod()*3);
  for(sizeType i=0,cc=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++)
      for(sizeType k=0; k<=MAXID(2); k++) {
        model.getX0().segment<3>(GI3D(i,j,k)*3)=POS3D(i,j,k);
        model.getF().segment<3>(GI3D(i,j,k)*3)=g;
        if(j == 0)
          model.getContactPoints()[cc++]=GI3D(i,j,k);
      }
  model.getG()=model.getF();
  model.getXN()=model.getX()=model.getXP()=model.getX0();
  //kinetic energy
  sizeType c=0;
  model.getPss().clear();
  for(sizeType i=0; i<=MAXID(0); i++)
    for(sizeType j=0; j<=MAXID(1); j++)
      for(sizeType k=0; k<=MAXID(2); k++)
        if(_pt.get<bool>("linear")) {
          ADDCONS(i,j,k,FEMModelLinear)
        } else {
          ADDCONS(i,j,k,FEMModel)
        }
  model.getCoefPss().setOnes(model.getPss().size());
  buildDirectionalStrainLimits(model,sz,res);
  buildAreaStrainLimits(model,sz,res);
  buildGroundContact(model,sz,res);
  return model;
#undef ADDCONS
}
template class BuildScenario<2>;
template class BuildScenario<3>;

PRJ_END
