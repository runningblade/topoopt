#ifndef TRAJECTORY_OBJECTIVE_H
#define TRAJECTORY_OBJECTIVE_H

#include <CommonFile/IO.h>
#include <CommonFile/solvers/Objective.h>

PRJ_BEGIN

template <int DIM>
class DynamicModel;
//TrajectoryObj
template <int DIM>
class TrajectoryObjective : public Serializable
{
public:
  typedef Objective<scalarD>::Vec Vec;
  typedef vector<Vec,Eigen::aligned_allocator<Vec> > Vss;
  TrajectoryObjective(const string& name);
  virtual scalarD operator()(const Vss& xss,Vss* dxss,const Vec& x,Vec* dfdx);
  virtual void makeLocalMinima(const Vss& xss,const Vss& dxss,const Vec& x,const Vec& dfdx);
  virtual void debugGradient(sizeType szTraj,sizeType nrX,sizeType nrVar);
  virtual void reset(const DynamicModel<DIM>& model);
};
//CompositeObjective
template <int DIM>
class CompositeObjective : public TrajectoryObjective<DIM>
{
public:
  using typename TrajectoryObjective<DIM>::Vec;
  using typename TrajectoryObjective<DIM>::Vss;
  CompositeObjective();
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operator()(const Vss& xss,Vss* dxss,const Vec& x,Vec* dfdx) override;
  virtual void makeLocalMinima(const Vss& xss,const Vss& dxss,const Vec& x,const Vec& dfdx) override;
  virtual void reset(const DynamicModel<DIM>& model) override;
  template <typename T>
  T* get(sizeType id) {
    ASSERT(id >= 0 && id < (sizeType)_objs.size())
    return boost::dynamic_pointer_cast<T>(_objs[id]).get();
  }
  template <typename T>
  const T* get(sizeType id) const {
    ASSERT(id >= 0 && id < (sizeType)_objs.size())
    return boost::dynamic_pointer_cast<T>(_objs[id]).get();
  }
  vector<boost::shared_ptr<TrajectoryObjective<DIM> > > _objs;
};
//RegularizeObj
template <int DIM>
class RegularizeObjective : public TrajectoryObjective<DIM>
{
public:
  using typename TrajectoryObjective<DIM>::Vec;
  using typename TrajectoryObjective<DIM>::Vss;
  RegularizeObjective();
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operator()(const Vss& xss,Vss* dxss,const Vec& x,Vec* dfdx) override;
  virtual void makeLocalMinima(const Vss& xss,const Vss& dxss,const Vec& x,const Vec& dfdx) override;
  scalarD _reg;
};
//HeightObj
template <int DIM>
class HeightObjective : public TrajectoryObjective<DIM>
{
public:
  using typename TrajectoryObjective<DIM>::Vec;
  using typename TrajectoryObjective<DIM>::Vss;
  HeightObjective();
  HeightObjective(const DynamicModel<DIM>& model);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operator()(const Vss& xss,Vss* dxss,const Vec& x,Vec* dfdx) override;
  virtual void reset(const DynamicModel<DIM>& model) override;
  Vec _averageHeight;
  scalarD _expCoef;
};
//RandomObj
template <int DIM>
class RandomObjective : public TrajectoryObjective<DIM>
{
public:
  using typename TrajectoryObjective<DIM>::Vec;
  using typename TrajectoryObjective<DIM>::Vss;
  RandomObjective();
  RandomObjective(const DynamicModel<DIM>& model);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operator()(const Vss& xss,Vss* dxss,const Vec& x,Vec* dfdx) override;
  virtual void reset(const DynamicModel<DIM>& model) override;
  Vec _coef;
};

PRJ_END

#endif
