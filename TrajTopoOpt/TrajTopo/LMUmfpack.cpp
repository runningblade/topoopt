#include "LMUmfpack.h"
#include <CommonFile/Timing.h>

USE_PRJ_NAMESPACE

#ifdef UMFPACK_SUPPORT
#include "umfpack.h"
UmfpackWrapper::UmfpackWrapper():_built(false),_same(false),_useEigen(false) {}
UmfpackWrapper::~UmfpackWrapper()
{
  clear();
}
boost::shared_ptr<LMInterface::LMDenseInterface> UmfpackWrapper::copy() const
{
  return boost::shared_ptr<LMDenseInterface>(new UmfpackWrapper());
}
bool UmfpackWrapper::recompute(SMat& smat,scalarD shift,bool sameA)
{
  bool succ;
  TBEG("Factorize");
  if(_same && _built) {
    smat.makeCompressed();
    ASSERT_MSG(_A.nonZeros() == (int)smat.nonZeros(),"We must have same number of nonzeros in sameMode of UmfpackWrapper!")
    copyData(smat,shift);
    succ=tryFactorize(smat);
  } else {
    clear();
    _A=smat.cast<double>();
    copyData(smat,shift);
    _status=umfpack_di_symbolic(_A.rows(),_A.cols(),_A.outerIndexPtr(),_A.innerIndexPtr(),_A.valuePtr(),&_symbolic,NULL,NULL);
    ASSERT_MSG(_status == UMFPACK_OK,"Analyze failed!")
    succ=tryFactorize(smat);
    _built=true;
  }
  TEND();
  return succ;
}
Matd UmfpackWrapper::solve(const Matd& b)
{
  TBEG("Solve");
  if(_status != UMFPACK_OK) {
    if(_useEigen)
      _ret=_eigenSol.solve(b).cast<double>();
    else {
      ASSERT_MSG(false,"Cannot solve with _status != UMFPACK_OK.")
    }
  } else {
    _ret.resize(b.rows(),b.cols());
    _b=b.cast<double>();
    for(sizeType j=0; j<b.cols(); j++) {
      int status=umfpack_di_solve(UMFPACK_A,_A.outerIndexPtr(),_A.innerIndexPtr(),_A.valuePtr(),
                                  _ret.data()+_ret.rows()*j,_b.data()+_b.rows()*j,_numeric,NULL,NULL);
      ASSERT_MSG(status == UMFPACK_OK,"Umfpack solve failed!")
    }
  }
  TEND();
  return _ret.cast<scalarD>();
}
void UmfpackWrapper::setSameStruct(bool same)
{
  _same=same;
}
void UmfpackWrapper::setUseEigen(bool useEigen)
{
  _useEigen=useEigen;
}
void UmfpackWrapper::debug()
{
#define N 10
  Matd A(N,N);
  A.setRandom();
  A=(A.transpose()*A).eval();

  SMat AS;
  AS.resize(N,N);
  for(sizeType r=0; r<N; r++)
    for(sizeType c=0; c<N; c++)
      AS.coeffRef(r,c)=A(r,c);

  scalarD shift=0.1f;
  Cold B=Cold::Random(N),X,X2;
  recompute(AS,shift,false);
  X=solve(B);
  X2=(A+Matd::Identity(N,N)*shift).llt().solve(B);
  INFOV("X: %f Err: %f",X.norm(),(X-X2).norm())

  shift=0.2f;
  recompute(AS,shift,false);
  X=solve(B);
  X2=(A+Matd::Identity(N,N)*shift).llt().solve(B);
  INFOV("X: %f Err: %f",X.norm(),(X-X2).norm())
#undef N
}
void UmfpackWrapper::copyData(const SMat& smat,scalarD shift)
{
  if(shift != 0)
    for(sizeType i=0; i<_A.rows(); i++)
      _A.coeffRef(i,i)+=shift;
  _A.makeCompressed();
}
bool UmfpackWrapper::tryFactorize(SMat& smat)
{
  //factorize
  _status=umfpack_di_numeric(_A.outerIndexPtr(),_A.innerIndexPtr(),_A.valuePtr(),_symbolic,&_numeric,NULL,NULL);
  if(_status != UMFPACK_OK) {
    WARNING("Factorize failed, fallback to eigen!")
    if(_useEigen) {
      _eigenSol.compute(smat);
      return true;
    } else return false;
  } else {
    return true;
  }
}
void UmfpackWrapper::clear()
{
  if(_built) {
    if(_symbolic) {
      umfpack_di_free_symbolic(&_symbolic);
      _symbolic=NULL;
    }
    if(_numeric) {
      umfpack_di_free_numeric(&_numeric);
      _numeric=NULL;
    }
    _built=false;
  }
}
#endif
