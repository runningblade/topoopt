#ifndef LM_UMFPACK_H
#define LM_UMFPACK_H
#ifdef UMFPACK_SUPPORT

#include "LMInterface.h"

PRJ_BEGIN

class UmfpackWrapper : public LMInterface::LMDenseInterface
{
public:
  typedef LMInterface::SMat SMat;
  UmfpackWrapper();
  virtual ~UmfpackWrapper();
  virtual boost::shared_ptr<LMInterface::LMDenseInterface> copy() const override;
  bool recompute(SMat& smat,scalarD shift,bool sameA) override;
  Matd solve(const Matd& b) override;
  void setSameStruct(bool same);
  void setUseEigen(bool useEigen);
  void debug();
private:
  void copyData(const SMat& smat,scalarD shift);
  bool tryFactorize(SMat& smat);
  void clear();
  Eigen::SparseLU<SMat> _eigenSol;
  void *_numeric;
  void *_symbolic;
  int _status;
  Eigen::Matrix<double,-1,-1> _ret,_b;
  Eigen::SparseMatrix<double,0,int> _A;
  bool _built,_same,_useEigen;
};

PRJ_END

#endif
#endif
