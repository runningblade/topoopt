#include "ElasticModel.h"

PRJ_BEGIN

//ElasticModel
template <int DIM>
ElasticModel<DIM>::ElasticModel(const string& name):Serializable(name) {}
template <int DIM>
ElasticModel<DIM>::ElasticModel(const string& name,sizeType id):Serializable(name),_pid(id) {}
template <int DIM>
ElasticModel<DIM>::~ElasticModel() {}
template <int DIM>
bool ElasticModel<DIM>::read(istream& is,IOData* dat)
{
  readBinaryData(_pid,is);
  return is.good();
}
template <int DIM>
bool ElasticModel<DIM>::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_pid,os);
  return os.good();
}
template <int DIM>
int ElasticModel<DIM>::inputsAug() const
{
  return 0;
}
template <int DIM>
void ElasticModel<DIM>::initAug(const Vec& x,Vec& aug) const {}
//operatorK
template <int DIM>
scalarD ElasticModel<DIM>::operatorK(const Vec& x,SVecs* fgrad,DMat* fhess,scalarD coef,sizeType offAug)
{
  scalarD ret=operatorK(x,fgrad,fhess?&_tmpFjacs:NULL,coef,offAug);
  if(fhess)*fhess=_tmpFjacs.toDense();
  return ret;
}
template <int DIM>
scalarD ElasticModel<DIM>::operatorK(const Vec& x,SVecs* fgrad,SMat* fhess,scalarD coef,sizeType offAug)
{
  if(fhess)  //to remember number of entries
    _tmp.clear();
  scalarD ret=operatorK(x,fgrad,fhess?&_tmp:NULL,coef,offAug);
  if(fhess) {
    fhess->resize(inputs()+inputsAug(),inputs()+inputsAug());
    fhess->setFromTriplets(_tmp.begin(),_tmp.end());
  }
  return ret;
}
template <int DIM>
scalarD ElasticModel<DIM>::operatorMKDot(const Vec& xP,const Vec& b,const Vec& other,sizeType offAug) const
{
  STrips fhess;
  SVecs fgrad(Vec::Zero(inputs()+inputsAug()));
  operatorK(xP,&fgrad,(STrips*)NULL,1,inputs());
  operatorM(fhess,1);

  SMat fhessM;
  fhessM.resize(inputs()+inputsAug(),inputs()+inputsAug());
  fhessM.setFromTriplets(fhess.begin(),fhess.end());
  return (fhessM*b+fgrad.getMatrix()).dot(other);
}
//operatorM
template <int DIM>
void ElasticModel<DIM>::operatorM(DMat& fhess,scalarD coef)
{
  operatorM(_tmpFjacs,coef);
  fhess=_tmpFjacs.toDense();
}
template <int DIM>
void ElasticModel<DIM>::operatorM(SMat& fhess,scalarD coef)
{
  _tmp.clear();
  operatorM(_tmp,coef);
  fhess.resize(inputs()+inputsAug(),inputs()+inputsAug());
  fhess.setFromTriplets(_tmp.begin(),_tmp.end());
}
template <int DIM>
void ElasticModel<DIM>::operatorM(STrips& fhess,scalarD coef) const {}
template <int DIM>
scalarD ElasticModel<DIM>::kineticBruteForce(const Vec& x) const
{
  return 0;
}
template <int DIM>
bool ElasticModel<DIM>::isQuadratic() const
{
  return false;
}
template <int DIM>
void ElasticModel<DIM>::debugGradient(scalar eps)
{
#define DELTA 1E-10f
#define NR_TEST 10
  INFO("-------------------------------------------------------------")
  sizeType I=inputs(),IA=I+inputsAug();
  for(sizeType i=0; i<NR_TEST; i++) {
    scalarD coef=RandEngine::randR01();
    SVecs fgrad1(Vec::Zero(IA));
    SVecs fgrad2(Vec::Zero(IA));
    Vec delta=Vec::Random(IA);
    Vec x=Vec::Random(IA);
    Vec b=Vec::Random(IA);
    Vec other=Vec::Random(IA);
    DMat fhess,M;
    operatorM(M,coef);
    scalarD E1=operatorK(x,&fgrad1,&fhess,coef,I);
    scalarD MKDot1=(fgrad1.getMatrix()+M*b).dot(other);
    scalarD MKDot2=operatorMKDot(x,b,other,I)*coef;
    x+=delta*DELTA;
    scalarD E2=operatorK(x,&fgrad2,(Matd*)NULL,coef,I);
    scalarD NG=(E2-E1)/DELTA,AG=fgrad1.getMatrix().dot(delta);
    Vec NH=(fgrad2.getMatrix()-fgrad1.getMatrix())/DELTA,AH=fhess*delta;
    //test
    if(std::isinf(E1) || std::isnan(E1) || std::isinf(E2) || std::isnan(E2)) {
      i--;
      continue;
    }
    INFOV("GradientK: %f Err: %f",AG,AG-NG)
    INFOV("HessianK: %f Err: %f",AH.norm(),(AH-NH).norm())
    INFOV("Kinetic: %f Err: %f",x.dot(M*x)/2,x.dot(M*x)/2-kineticBruteForce(x)*coef)
    INFOV("MKDot: %f Err: %f",MKDot1,MKDot1-MKDot2)
    ASSERT(abs(AG-NG) < eps*abs(AG) || max(abs(AG),abs(NG)) < eps)
    ASSERT((AH-NH).norm() < eps*AH.norm() || max(AH.norm(),NH.norm()) < eps)
  }
#undef NR_TEST
#undef DELTA
}
template class ElasticModel<2>;
template class ElasticModel<3>;

PRJ_END
