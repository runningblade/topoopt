#ifndef TRAJECTORY_OPTIMIZATION_H
#define TRAJECTORY_OPTIMIZATION_H

#include "DynamicModel.h"
#include <CommonFile/IO.h>
#include <CommonFile/solvers/Objective.h>
#include <boost/property_tree/ptree.hpp>

PRJ_BEGIN

template <int DIM>
class TrajectoryObjective;
//TopoOptRecord
struct TopologyOptimizationRecord : public Serializable {
  typedef typename Objective<scalarD>::Vec Vec;
  typedef typename Objective<scalarD>::SMat SMat;
  typedef vector<Vec,Eigen::aligned_allocator<Vec> > Vss;
  TopologyOptimizationRecord();
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  //data
  scalarD _FX;
  Vss _xss,_dxss;
  Vec _var;
  //temporary data
  vector<SMat,Eigen::aligned_allocator<SMat> > _hesses;
  vector<boost::shared_ptr<LMInterface::LMDenseInterface> > _hessSols;
  Vec _coefPBackup;
  Vec _fBackup;
};
//TrajOpt
template <int DIM>
class TrajectoryOptimization : public Objective<scalarD>, public Serializable
{
public:
  typedef typename DynamicModel<DIM>::VecDIMd VecDIMd;
  typedef vector<Vec,Eigen::aligned_allocator<Vec> > Vss;
  TrajectoryOptimization();
  TrajectoryOptimization(const DynamicModel<DIM>& model);
  TrajectoryOptimization(const string& name);
  TrajectoryOptimization(const string& name,const DynamicModel<DIM>& model);
  virtual bool read(const string& path);
  virtual bool write(const string& path) const;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  //setup
  void resetVar(scalarD t,scalarD dt=0.01f,scalarD dt0=10.0f);
  void resetTimeVar(scalarD t,scalarD dt=0.01f,scalarD dt0=10.0f);
  void resetTimeVarContactPhase(scalarD horizon);
  void resetObj(boost::shared_ptr<TrajectoryObjective<DIM> > obj);
  void beginChangeVariable(TopologyOptimizationRecord& r);
  void endChangeVariable(TopologyOptimizationRecord& r);
  void addVarCoefP();
  void addVarCoefPSym();
  void addVarFAll();
  void addVarFTopG();
  void addVarFTopGAll();
  Vec collect() const;
  Vec collect(const Vec& x,const SMat& varCoef) const;
  const boost::property_tree::ptree& getPt() const;
  boost::property_tree::ptree& getPt();
  void useSuiteSparse(bool use);
  //write/plot
  void writeTrajVTK(const string& path,const Vss* xss) const;
  void writeTrajVTK(const string& path) const;
  void writeCoefPVTK(const string& path) const;
  void writeTrajResultVTK(const string& path) const;
  //compute gradient/optimization
  virtual int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient) override;
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable) override;
  virtual int inputs() const override;
  virtual int values() const override;
  virtual void debugTopologyGradient(scalarD scaleCoefP,scalarD scaleForce,bool forwardOnly);
  virtual void debugTrajectoryGradient(scalarD scaleCoefP,scalarD scaleForceAndX);
  virtual void debugMinimizeTrajectoryGradient(scalarD scaleCoefP,scalarD force,scalarD tolGModel);
  virtual void debugMinimizeTrajectoryGradientError();
  virtual void debugBackwardError();
  virtual bool minimizeTrajectoryGradientLBFGS();
  virtual bool minimizeTrajectoryGradient();
  virtual bool optimizeTopology();
  virtual void optimizeJoint();
  //brute force search of external force
  virtual bool optimizeForceBruteForce();
  virtual bool optimizeForceBinarySearch(TopologyOptimizationRecord A,TopologyOptimizationRecord B);
  virtual void makeLocalMinima();
protected:
  void forward();
  void backward(Vec* dfdx=NULL);
  bool lineSearchTrajectory(TopologyOptimizationRecord& grad,scalarD& stp,scalarD gNorm,scalarD& gNorm2);
  void topologyGradient(const Vec& x,scalarD& FX,Vec& DFDX);
  void trajectoryGradient(TopologyOptimizationRecord& ret,bool computeDxss);
  void solveBlockLower(TopologyOptimizationRecord& ret) const;
  void identifySym(boost::unordered_map<sizeType,sizeType>& identify) const;
  sizeType findTopVid() const;
  //data
  boost::shared_ptr<DynamicModel<DIM> > _model;
  boost::shared_ptr<TrajectoryObjective<DIM> > _obj;
  boost::property_tree::ptree _pt;
  //map from variable to f
  SMat _varCoefPss,_varF;
  Vec _varCoefP0;
  Vss _warmStart;
  //record optimization results
  TopologyOptimizationRecord _result;
  boost::shared_ptr<LMInterface::LMDenseInterface> _interface;
};

PRJ_END

#endif
