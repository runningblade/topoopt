#include "TrajectoryOptimization.h"
#include "TrajectoryObjective.h"
#include "ElasticModel.h"
#include "DynamicModel.h"
#include "LMUmfpack.h"
#include "Utils.h"
#include <CommonFile/IO.h>
#include <CommonFile/solvers/Minimizer.h>
#include <CommonFile/geom/BVHBuilder.h>
#include <boost/lexical_cast.hpp>

PRJ_BEGIN

//callback
template <int DIM>
struct DistanceCallback {
  typedef Node<sizeType,BBox<scalarD,DIM> > NODE;
  typedef typename TrajectoryOptimization<DIM>::VecDIMd VecDIMd;
  DistanceCallback(const VecDIMd& x):_id(-1),_x(x) {}
  bool validNode(const NODE& node) const {
    return node._bb.contain(_x);
  }
  void updateDist(const NODE& node) {
    if(node._nrCell == 1 && node._bb.contain(_x))
      _id=node._cell;
  }
  sizeType _id;
  VecDIMd _x;
};
//TopoOptRecord
TopologyOptimizationRecord::TopologyOptimizationRecord():Serializable(typeid(TopologyOptimizationRecord).name()) {}
bool TopologyOptimizationRecord::read(istream& is,IOData* dat)
{
  readBinaryData(_FX,is);
  readVector(_xss,is);
  readVector(_dxss,is);
  readBinaryData(_var,is);
  return is.good();
}
bool TopologyOptimizationRecord::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_FX,os);
  writeVector(_xss,os);
  writeVector(_dxss,os);
  writeBinaryData(_var,os);
  return os.good();
}
boost::shared_ptr<Serializable> TopologyOptimizationRecord::copy() const
{
  return boost::shared_ptr<Serializable>(new TopologyOptimizationRecord());
}
//TrajOpt
template <int DIM>
TrajectoryOptimization<DIM>::TrajectoryOptimization():Serializable(typeid(TrajectoryOptimization<DIM>).name())
{
  useSuiteSparse(false);
}
template <int DIM>
TrajectoryOptimization<DIM>::TrajectoryOptimization(const DynamicModel<DIM>& model):Serializable(typeid(TrajectoryOptimization<DIM>).name())
{
  _model.reset(new DynamicModel<DIM>(model));
  useSuiteSparse(false);
}
template <int DIM>
TrajectoryOptimization<DIM>::TrajectoryOptimization(const string& name):Serializable(name)
{
  useSuiteSparse(false);
}
template <int DIM>
TrajectoryOptimization<DIM>::TrajectoryOptimization(const string& name,const DynamicModel<DIM>& model):Serializable(name)
{
  _model.reset(new DynamicModel<DIM>(model));
  useSuiteSparse(false);
}
template <int DIM>
bool TrajectoryOptimization<DIM>::read(const string& path)
{
  boost::filesystem::ifstream is(path,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  return read(is,dat.get());
}
template <int DIM>
bool TrajectoryOptimization<DIM>::write(const string& path) const
{
  boost::filesystem::ofstream os(path,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  return write(os,dat.get());
}
template <int DIM>
bool TrajectoryOptimization<DIM>::read(istream& is,IOData* dat)
{
  FixedSparseMatrix<scalarD,Kernel<scalarD> > tmp;
  registerType<DynamicModel<DIM> >(dat);
  registerType<CompositeObjective<DIM> >(dat);
  registerType<RegularizeObjective<DIM> >(dat);
  registerType<HeightObjective<DIM> >(dat);
  registerType<RandomObjective<DIM> >(dat);
  readBinaryData(_model,is,dat);
  readBinaryData(_obj,is,dat);
  readPtreeBinary(_pt,is);
  tmp.read(is);
  tmp.toEigen(_varCoefPss);
  tmp.read(is);
  tmp.toEigen(_varF);
  readBinaryData(_varCoefP0,is);
  readBinaryData(_result,is,dat);
  return is.good();
}
template <int DIM>
bool TrajectoryOptimization<DIM>::write(ostream& os,IOData* dat) const
{
  FixedSparseMatrix<scalarD,Kernel<scalarD> > tmp;
  registerType<DynamicModel<DIM> >(dat);
  registerType<CompositeObjective<DIM> >(dat);
  registerType<RegularizeObjective<DIM> >(dat);
  registerType<HeightObjective<DIM> >(dat);
  registerType<RandomObjective<DIM> >(dat);
  writeBinaryData(_model,os,dat);
  writeBinaryData(_obj,os,dat);
  writePtreeBinary(_pt,os);
  tmp.fromEigen(_varCoefPss);
  tmp.write(os);
  tmp.fromEigen(_varF);
  tmp.write(os);
  writeBinaryData(_varCoefP0,os);
  writeBinaryData(_result,os,dat);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> TrajectoryOptimization<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new TrajectoryOptimization<DIM>());
}
//setup
template <int DIM>
void TrajectoryOptimization<DIM>::resetVar(scalarD t,scalarD dt,scalarD dt0)
{
  if(_varCoefPss.size() == _model->getCoefPss().size() && _varF.rows() == _model->getF().size())
    beginChangeVariable(_result);
  //variable configuration
  _varCoefP0.setOnes(_model->getCoefPss().size());
  _varCoefPss.resize(_model->getCoefPss().size(),0);
  _varF.resize(_model->getF().size(),0);
  if(_varCoefPss.size() == _model->getCoefPss().size() && _varF.rows() == _model->getF().size())
    endChangeVariable(_result);
  //time related variables
  resetTimeVar(t,dt,dt0);
}
template <int DIM>
void TrajectoryOptimization<DIM>::resetTimeVar(scalarD t,scalarD dt,scalarD dt0)
{
  //timestep configuration
  _pt.put<scalarD>("t",t);
  _pt.put<scalarD>("dt",dt);
  _pt.put<scalarD>("dt0",dt0);
}
template <int DIM>
void TrajectoryOptimization<DIM>::resetTimeVarContactPhase(scalarD horizon)
{
  bool useCB=_pt.get<bool>("resetTime.useCB",false);
  Vec &x=_result._var;
  //initialize
  _model->getCoefPss()=_varCoefPss*x.segment(0,_varCoefPss.cols())+_varCoefP0;
  _model->getF()=_varF*x.segment(_varCoefPss.cols(),_varF.cols());
  //advance
  scalarD endTime=0,time=0;
  scalarD altitudeCurr,altitudeMax=-ScalarUtil<scalarD>::scalar_max;
  for(sizeType i=0;; i++) {
    //advance
    if(i == 0) {
      _model->getXN()=_model->getX()=_model->getX0();
      _model->advance(_pt.get<scalarD>("dt0"),false,NULL);
      _model->getXN()=_model->getX()=_model->getXP();
    } else _model->advance(_pt.get<scalarD>("dt"),false,NULL);
    //determine altitude
    altitudeCurr=ScalarUtil<scalarD>::scalar_max;
    for(sizeType i=0; i<_model->getX0().size()/DIM; i++) {
      VecDIMd gravity=_model->getG().template segment<DIM>(i*DIM);
      VecDIMd pos=_model->getXP().template segment<DIM>(i*DIM);
      altitudeCurr=min(altitudeCurr,-pos.dot(gravity));
    }
    if(useCB) {
      INFOV("Altitude at time=%f: %f!",time,altitudeCurr)
    }
    //determine contact phase
    if(altitudeCurr > altitudeMax) {
      altitudeMax=altitudeCurr;
      if(useCB) {
        INFOV("Found new altitude=%f!",altitudeMax)
      }
      if(_model->groundContact(_model->getXP(),NULL,NULL) <= 0)
        endTime=0;  //flag for new end time
    } else if(endTime == 0 && _model->groundContact(_model->getXP(),NULL,NULL) > 0) {
      endTime=time; //set new end time
      if(useCB) {
        INFOV("Set new endTime=%f!",endTime)
      }
    }
    time+=_pt.get<scalarD>("dt");
    if(time >= horizon)
      break;
    //update
    if(i == 0)
      _model->resetF();
    _model->getXN()=_model->getX();
    _model->getX()=_model->getXP();
  }
  //endTime == 0 means we are not on ground at time=_t, then just use _t
  _pt.put<scalarD>("t",endTime == 0 ? _pt.get<scalarD>("t") : endTime);
  forward();
}
template <int DIM>
void TrajectoryOptimization<DIM>::resetObj(boost::shared_ptr<TrajectoryObjective<DIM> > obj)
{
  _obj=obj;
}
template <int DIM>
void TrajectoryOptimization<DIM>::beginChangeVariable(TopologyOptimizationRecord& r)
{
  r._coefPBackup=_varCoefPss*r._var.segment(0,_varCoefPss.cols())+_varCoefP0;
  r._fBackup=_varF*r._var.segment(_varCoefPss.cols(),_varF.cols())+_model->getG();
}
template <int DIM>
void TrajectoryOptimization<DIM>::endChangeVariable(TopologyOptimizationRecord& r)
{
  DynamicModel<DIM>& model=*(_model);
  model.getCoefPss()=r._coefPBackup;
  model.getF()=r._fBackup;
  r._var=collect();
}
template <int DIM>
void TrajectoryOptimization<DIM>::addVarCoefP()
{
  beginChangeVariable(_result);
  set<sizeType> pset;
  const Coli& p=_model->getContactPoints();
  for(sizeType i=0; i<p.size(); i++)
    pset.insert(p[i]);
  sizeType nrP=_model->getCoefPss().size(),off=0;
  //assemble
  STrips trips;
  _varCoefP0.setZero();
  for(sizeType i=0; i<nrP; i++) {
    //determine is contact
    bool contact=false;
    Coli ids=_model->getPss()[i]->ids();
    for(sizeType j=0; j<ids.size(); j++)
      if(pset.find(ids[j]) != pset.end())
        contact=true;
    //assemble
    if(!contact)
      trips.push_back(STrip(i,off++,1));
    else _varCoefP0[i]=1;
  }
  //build
  _varCoefPss.resize(nrP,off);
  _varCoefPss.setFromTriplets(trips.begin(),trips.end());
  endChangeVariable(_result);
}
template <int DIM>
void TrajectoryOptimization<DIM>::addVarCoefPSym()
{
  beginChangeVariable(_result);
  set<sizeType> pset;
  const Coli& p=_model->getContactPoints();
  for(sizeType i=0; i<p.size(); i++)
    pset.insert(p[i]);
  sizeType nrP=_model->getCoefPss().size(),off=0;
  boost::unordered_map<sizeType,sizeType> identify,offMap;
  identifySym(identify);
  //assemble
  STrips trips;
  _varCoefP0.setZero();
  for(sizeType i=0; i<nrP; i++) {
    //determine is contact
    bool contact=false;
    Coli ids=_model->getPss()[i]->ids();
    for(sizeType j=0; j<ids.size(); j++)
      if(pset.find(ids[j]) != pset.end())
        contact=true;
    //assemble
    if(!contact) {
      if(identify[i] == i) {
        offMap[i]=off;
        trips.push_back(STrip(i,off++,1));
      }
    } else _varCoefP0[i]=1;
  }
  for(sizeType i=0; i<nrP; i++)
    if(_varCoefP0[i] == 0) {  //not contact
      if(identify[i] != i) {
        ASSERT(offMap.find(identify[i]) != offMap.end())
        trips.push_back(STrip(i,offMap[identify[i]],1));
      }
    }
  //build
  _varCoefPss.resize(nrP,off);
  _varCoefPss.setFromTriplets(trips.begin(),trips.end());
  endChangeVariable(_result);
}
template <int DIM>
void TrajectoryOptimization<DIM>::addVarFAll()
{
  beginChangeVariable(_result);
  sizeType nrF=_model->getF().size();
  _varF.resize(nrF,nrF);
  for(sizeType i=0; i<nrF; i++)
    _varF.coeffRef(i,i)=1;
  endChangeVariable(_result);
}
template <int DIM>
void TrajectoryOptimization<DIM>::addVarFTopG()
{
  beginChangeVariable(_result);
  sizeType id=findTopVid();
  VecDIMd G=_model->getG().template segment<DIM>(id*DIM).normalized();
  sizeType nrF=_model->getF().size();
  _varF.resize(nrF,1);
  for(sizeType i=0; i<DIM; i++)
    _varF.coeffRef(id*DIM+i,0)=G[i];
  endChangeVariable(_result);
}
template <int DIM>
void TrajectoryOptimization<DIM>::addVarFTopGAll()
{
  beginChangeVariable(_result);
  sizeType id=findTopVid();
  sizeType nrF=_model->getF().size();
  _varF.resize(nrF,DIM);
  for(sizeType i=0; i<DIM; i++)
    _varF.coeffRef(id*DIM+i,i)=1;
  endChangeVariable(_result);
}
template <int DIM>
typename TrajectoryOptimization<DIM>::Vec TrajectoryOptimization<DIM>::collect() const
{
  Vec dp=_model->getCoefPss()-_varCoefP0;
  Vec df=_model->getF()-_model->getG();
  return concat<Vec>(collect(dp,_varCoefPss),collect(df,_varF));
}
template <int DIM>
typename TrajectoryOptimization<DIM>::Vec TrajectoryOptimization<DIM>::collect(const Vec& x,const SMat& varCoef) const
{
  Vec b=varCoef.transpose()*x;
  Eigen::SimplicialLLT<SMat> sol(varCoef.transpose()*varCoef);
  return sol.solve(b);
}
template <int DIM>
const boost::property_tree::ptree& TrajectoryOptimization<DIM>::getPt() const
{
  return _pt;
}
template <int DIM>
boost::property_tree::ptree& TrajectoryOptimization<DIM>::getPt()
{
  return _pt;
}
template <int DIM>
void TrajectoryOptimization<DIM>::useSuiteSparse(bool use)
{
#ifdef UMFPACK_SUPPORT
  if(use)
    _interface.reset(new UmfpackWrapper());
  else _interface=LMInterface::getInterface(false);
#else
  _interface=LMInterface::getInterface(false);
#endif
}
//write/plot
template <int DIM>
void TrajectoryOptimization<DIM>::writeTrajVTK(const string& path,const Vss* xss) const
{
  recreate(path);
  for(sizeType i=0; i<(sizeType)xss->size(); i++) {
    const Vec& x=xss->at(i);
    _model->writeVTK(&x,path+"/frm"+boost::lexical_cast<string>(i)+".vtk");
  }
}
template <int DIM>
void TrajectoryOptimization<DIM>::writeTrajVTK(const string& path) const
{
  writeTrajVTK(path,&_result._xss);
}
template <int DIM>
void TrajectoryOptimization<DIM>::writeCoefPVTK(const string& path) const
{
  Vec pid=Vec::Zero(_varCoefPss.cols());
  for(sizeType i=0; i<pid.size(); i++)
    pid[i]=(scalarD)i;
  Vec pidAll=_varCoefPss*pid-_varCoefP0;
  _model->writeVTK(path,&pidAll,"ElementIndex");
}
template <int DIM>
void TrajectoryOptimization<DIM>::writeTrajResultVTK(const string& path) const
{
  writeTrajVTK(path,&(_result._xss));
}
//compute gradient/optimize
template <int DIM>
int TrajectoryOptimization<DIM>::operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient)
{
  Vec fvec;
  SMat fjac;
  Objective<scalarD>::operator()(x,fvec,&fjac,true);
  DFDX=fjac.transpose()*fvec;
  FX=fvec.squaredNorm()/2;
  return 0;
}
template <int DIM>
int TrajectoryOptimization<DIM>::operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable)
{
  Vec tmp;
  SMat grad;
  sizeType nrXAll=_result._xss[0].size(),IXN=0,IX=0;
  sizeType nT=(sizeType)_result._xss.size();
  fvec.setZero(nrXAll*nT);
  //assign
  for(sizeType i=0; i<nT; i++)
    _result._xss[i]=x.segment(nrXAll*i,nrXAll);
  //main loop
  _model->resetF();
  _model->getCoefPss()=_varCoefPss*_result._var.segment(0,_varCoefPss.cols())+_varCoefP0;
  _model->getF()+=_varF*_result._var.segment(_varCoefPss.cols(),_varF.cols());
  for(sizeType i=0; i<nT; i++) {
    if(i == 0) {
      _model->getDt()=_pt.get<scalarD>("dt0");
      _model->getXN()=_model->getX()=_model->getX0();
    } else {
      IXN=max<sizeType>(i-2,0);
      IX=max<sizeType>(i-1,0);
      _model->getDt()=_pt.get<scalarD>("dt");
      _model->getXN()=_result._xss[IXN];
      _model->getX()=_result._xss[IX];
    }
    tmp.setZero(nrXAll);
    _model->Objective<scalarD>::operator()(_result._xss[i],&tmp,fjac ? &grad : NULL);
    fvec.segment(nrXAll*i,nrXAll)=tmp;
    if(fjac)
      addSparseBlock(*fjac,nrXAll*i,nrXAll*i,grad,1);
    if(i == 0)
      _model->resetF();
    else if(fjac) {
      addSparseBlock(*fjac,nrXAll*i,nrXAll*IX,_model->getM(),-2/pow(_model->getDt(),2));
      addSparseBlock(*fjac,nrXAll*i,nrXAll*IXN,_model->getM(),1/pow(_model->getDt(),2));
    }
  }
  return 0;
}
template <int DIM>
int TrajectoryOptimization<DIM>::inputs() const
{
  return (int)(_result._xss.size()*_result._xss[0].size());
}
template <int DIM>
int TrajectoryOptimization<DIM>::values() const
{
  return inputs();
}
template <int DIM>
void TrajectoryOptimization<DIM>::debugTopologyGradient(scalarD scaleCoefP,scalarD scaleForce,bool forwardOnly)
{
#define DELTA 1E-10f
#define NR_TEST 10
  if(scaleCoefP > 0 && scaleForce > 0) {
    //setup variable configuration
    addVarCoefP();
    addVarFTopG();
    //collect variable, apply perturb
    _result._var=collect();
    _result._var.segment(0,_varCoefPss.cols())+=Vec::Random(_varCoefPss.cols())*scaleCoefP;
    _result._var.segment(_varCoefPss.cols(),_varF.cols())+=Vec::Random(_varF.cols()).cwiseAbs()*scaleForce;
  }
  _warmStart.clear();
  if(forwardOnly) {
    //calc forward propagation
    forward();
    writeTrajVTK("./anim");
  } else {
    //calc gradient
    scalarD FX,FX2;
    Vec delta,DFDX,DFDX2;
    INFO("-------------------------------------------------------------")
    for(sizeType i=0; i<NR_TEST; i++) {
      delta=Vec::Random(_result._var.size());
      DFDX=Vec::Zero(_result._var.size()),DFDX2=DFDX;
      topologyGradient(_result._var,FX,DFDX);
      topologyGradient(_result._var+delta*DELTA,FX2,DFDX2);
      INFOV("ObjectiveGradient: %f Err: %f",DFDX.dot(delta),DFDX.dot(delta)-(FX2-FX)/DELTA)
    }
  }
#undef NR_TEST
#undef DELTA
}
template <int DIM>
void TrajectoryOptimization<DIM>::debugTrajectoryGradient(scalarD scaleCoefP,scalarD scaleForceAndX)
{
#define DELTA 1E-12f
#define NR_TEST 10
  if(scaleCoefP > 0 && scaleForceAndX > 0) {
    //setup variable configuration
    addVarCoefP();
    addVarFAll();
    //collect variable, apply perturb
    _result._var=collect();
    _result._var.segment(0,_varCoefPss.cols())+=Vec::Random(_varCoefPss.cols())*scaleCoefP;
    _result._var.segment(_varCoefPss.cols(),_varF.cols())+=Vec::Random(_varF.cols())*scaleForceAndX;
  }
  //calc gradient
  if(_result._xss.empty())
    forward();
  SMat jac;
  Vec fvec,fvec2,dxss,xvec=Vec::Zero(inputs());
  sizeType nrXAll=_result._xss[0].size();
  TopologyOptimizationRecord ret;
  INFO("-------------------------------------------------------------TrajectoryGradient")
  for(sizeType i=0; i<NR_TEST; i++) {
    for(sizeType x=0; x<(sizeType)_result._xss.size(); x++) {
      if(scaleForceAndX > 0)
        _result._xss[x]+=Vec::Random(_result._xss[x].size())*scaleForceAndX;
      xvec.segment(nrXAll*x,nrXAll)=_result._xss[x];
    }
    Objective<scalarD>::operator()(xvec,fvec,&jac,true);
    trajectoryGradient(ret,true);
    dxss=xvec,fvec2=xvec;
    for(sizeType x=0; x<(sizeType)_result._xss.size(); x++) {
      dxss.segment(nrXAll*x,nrXAll)=ret._dxss[x];
      fvec2.segment(nrXAll*x,nrXAll)=ret._xss[x];
    }
    INFOV("Compared with LM FVec: %f Err: %f ErrInvGFVec: %f",
          fvec.norm(),(fvec2-fvec).norm(),(fvec-jac*dxss).norm())
    //modify
    Vec d=Vec::Random(xvec.size());
    Objective<scalarD>::operator()(xvec+d*DELTA,fvec2,(SMat*)NULL,true);
    INFOV("Gradient: %f Err: %f",(jac*d).norm(),(jac*d-(fvec2-fvec)/DELTA).norm())
  }
#undef NR_TEST
#undef DELTA
}
template <int DIM>
void TrajectoryOptimization<DIM>::debugMinimizeTrajectoryGradient(scalarD scaleCoefP,scalarD force,scalarD tolGModel)
{
  //setup variable configuration
  _model->getTolG()=tolGModel;
  addVarCoefPSym();
  addVarFTopG();
  //collect variable, apply perturb
  _result._var=collect();
  _result._var.segment(0,_varCoefPss.cols())+=Vec::Random(_varCoefPss.cols())*scaleCoefP;
  _result._var[_varCoefPss.cols()]=0;
  forward();
  _result._var[_varCoefPss.cols()]=force;
  minimizeTrajectoryGradient();
  writeTrajVTK("./anim");
}
template <int DIM>
void TrajectoryOptimization<DIM>::debugMinimizeTrajectoryGradientError()
{
  read("./minimizeTrajectoryGradient_error.dat");
  _pt.put<bool>("trajGrad.useCB",true);
  minimizeTrajectoryGradient();
  writeTrajVTK("./anim");
}
template <int DIM>
void TrajectoryOptimization<DIM>::debugBackwardError()
{
  read("./backward_error.dat");
  while(true)
    backward();
}
template <int DIM>
bool TrajectoryOptimization<DIM>::minimizeTrajectoryGradientLBFGS()
{
  Vec xvec;
  scalarD FX;
  LBFGSMinimizer<scalarD> sol;
  bool useCB=_pt.get<bool>("trajGrad.useCB",true);
  sol.delta()=_pt.get<scalarD>("trajGrad.tolXLBFGS",1E-8f);
  sol.epsilon()=_pt.get<scalarD>("trajGrad.tolGLBFGS",1E-8f);
  xvec.setZero(inputs());
  sizeType nrX=_result._xss[0].size();
  for(sizeType x=0; x<(sizeType)_result._xss.size(); x++)
    xvec.segment(nrX*x,nrX)=_result._xss[x];
  Callback<scalarD,Kernel<scalarD> > cb;
  NoCallback<scalarD,Kernel<scalarD> > ncb;
  sizeType ret=sol.minimize(xvec,FX,*this,useCB ? cb : ncb);
  for(sizeType x=0; x<(sizeType)_result._xss.size(); x++)
    _result._xss[x]=xvec.segment(nrX*x,nrX);
  return ret >= 0;
}
template <int DIM>
bool TrajectoryOptimization<DIM>::minimizeTrajectoryGradient()
{
  //return minimizeTrajectoryGradientLBFGS();
  bool useCB=_pt.get<bool>("trajGrad.useCB",false);
  bool canRestart=_pt.get<bool>("trajGrad.canRestart",false);
  sizeType maxIter=_pt.get<sizeType>("trajGrad.maxIter",100);
  scalarD tolG=_pt.get<scalarD>("trajGrad.tolG",1E-4f);
  TopologyOptimizationRecord grad,bkg=_result;
  bool restarted=canRestart ? false : true;
  scalarD stp,xNorm,gMax,gNorm,gNorm2=0;
  //main loop
  sizeType i=0;
  for(; i<maxIter; i++) {
    trajectoryGradient(grad,true);
    xNorm=0;
    gMax=0;
    gNorm=0;
    for(sizeType x=0; x<(sizeType)grad._xss.size(); x++) {
      xNorm+=grad._dxss[x].squaredNorm();
      gMax=max(gMax,grad._xss[x].cwiseAbs().maxCoeff());
      gNorm+=grad._xss[x].squaredNorm();
    }
    if(useCB) {
      INFOV("MinimizeTrajectoryGradient at gNorm: %f, gMax: %f!",gNorm,gMax)
    }
    if(gMax <= tolG) {
      if(useCB) {
        INFOV("Gradient small, exit on tolG, gMax: %f!",gMax)
      }
      return true;
    }
    //line search: Newton step
    bool searchNewton=lineSearchTrajectory(grad,stp=1,gNorm,gNorm2);
    if(!searchNewton) {
      if(!restarted) {
        WARNING("MinimizeTrajectoryGradient: Newton failed, restarting!")
        forward();
        restarted=true;
      } else {
        _result=bkg;
        write("./minimizeTrajectoryGradient_error.dat");
        return false;
      }
    }
  }
  return i < maxIter;
}
template <int DIM>
bool TrajectoryOptimization<DIM>::optimizeTopology()
{
  bool useCB=_pt.get<bool>("topo.useCB",true);
  sizeType maxIter=_pt.get<sizeType>("topo.maxIter",100);
  scalarD tolE=_pt.get<scalarD>("topo.tolE",1E-6f);
  scalarD tolP=_pt.get<scalarD>("topo.tolP",1E-4f);
  scalarD dec=_pt.get<scalarD>("topo.stepDec",0.8f);
  scalarD inc=_pt.get<scalarD>("topo.stepInc",1.25f);
  scalarD stp=_pt.get<scalarD>("topo.stepSize",1.0f);
  scalarD incThres=_pt.get<scalarD>("topo.stepIncThres",0.75f);
  scalarD decThres=_pt.get<scalarD>("topo.stepIncThres",0.25f);
  Vec2d limit(_pt.get<scalarD>("topo.LB",0.01f),_pt.get<scalarD>("topo.UB",1.0f));
  //initialize
  Vec DFDX;
  scalarD actualDecrease=0;
  scalarD estimatedDecrease=0;
  scalarD stp0,FX2,gMax;
  TopologyOptimizationRecord tmp;
  forward();
  if(!minimizeTrajectoryGradient()) {
    if(useCB) {
      WARNING("minimizeTrajectoryGradient failed!")
    }
    return false;
  }
  //main loop
  for(sizeType i=0; i<maxIter; i++) {
    DFDX.setZero(_result._var.size());
    _result._dxss.assign(_result._xss.size(),Vec::Zero(_result._xss[0].size()));
    _result._FX=(*_obj)(_result._xss,&(_result._dxss),_result._var,&DFDX);
    backward(&DFDX);
    //avoid update other variables than topology
    DFDX.segment(_varCoefPss.cols(),_varF.cols()).setZero();
    gMax=DFDX.cwiseAbs().maxCoeff();
    if(useCB) {
      INFOV("optimizeTopology at gMax: %f!",gMax)
    }
    //line search
    stp0=stp;
    tmp=_result;
    while(true) {
      _result=tmp;
      _result._var-=DFDX*stp;
      _result._var.segment(0,_varCoefPss.cols())=_result._var.segment(0,_varCoefPss.cols()).eval().cwiseMax(limit[0]);
      _result._var.segment(0,_varCoefPss.cols())=_result._var.segment(0,_varCoefPss.cols()).eval().cwiseMin(limit[1]);
      if(!minimizeTrajectoryGradient()) {
        stp*=dec;
        if(useCB) {
          WARNINGV("Rejected topology, minimizeTrajectoryGradient failed, stp: %f!",stp)
        }
        continue;
      }
      FX2=(*_obj)(_result._xss,NULL,_result._var,NULL);
      estimatedDecrease=DFDX.dot(tmp._var-_result._var);
      actualDecrease=_result._FX-FX2;
      if(actualDecrease < 0) {
        stp*=dec;
        if(useCB) {
          WARNINGV("Rejected topology, actualDec: %f, estimatedDec: %f, stp: %f!",actualDecrease,estimatedDecrease,stp)
        }
        if(gMax*stp < tolP)
          break;
      } else {
        if(actualDecrease > estimatedDecrease*incThres && stp0 == stp)
          stp=stp*inc;
        else if(actualDecrease < estimatedDecrease*decThres)
          stp*=dec;
        if(useCB) {
          INFOV("Accepted topology, FX=%f, actualDec: %f, estimatedDec: %f, stp: %f!",FX2,actualDecrease,estimatedDecrease,stp)
        }
        break;
      }
    }
    if(gMax*stp < tolP) {
      if(useCB) {
        INFOV("Step size too small, exit on tolP, stp: %f!",stp)
      }
      break;
    }
    if(actualDecrease < tolE) {
      if(useCB) {
        INFOV("Function decrease too small, exit on tolE, stp: %f!",stp)
      }
      break;
    }
  }
  _pt.put<scalarD>("topo.stepSize",stp);
  return true;
}
template <int DIM>
void TrajectoryOptimization<DIM>::optimizeJoint()
{
  bool debugTraj=_pt.get<bool>("joint.debugTraj",true);
  scalarD horizon=_pt.get<scalarD>("joint.horizon",5);
  sizeType maxIter=_pt.get<sizeType>("joint.maxIter",10);
  string prefix=_pt.get<string>("joint.prefix");
  create(prefix);

  //initialize
  if(!exists(prefix+"/init.dat")) {
    if(!optimizeForceBruteForce()) {
      INFO("Cannot find initial trajectory!")
      exit(EXIT_FAILURE);
    }
    makeLocalMinima();
    write(prefix+"/init.dat");
    INFO("Found initial trajectory!")
  } else {
    read(prefix+"/init.dat");
    INFO("Read initial trajectory!")
  }
  if(debugTraj)
    writeTrajVTK(prefix+"/animInit");

  //main loop
  _pt.put<scalarD>("topo.stepSize",1);
  scalarD lastT=_pt.get<scalarD>("t"),currT;
  for(sizeType i=0; i<maxIter; i++) {
    resetTimeVarContactPhase(horizon);
    currT=_pt.get<scalarD>("t");
    INFOV("Iter %ld: reset horizon: %f!",i,currT)
    if(lastT == currT) {
      INFO("Exit on same horizon!")
      break;
    }
    lastT=currT;
    scalarD FX=_result._FX;
    if(!exists(prefix+"/iter"+boost::lexical_cast<string>(i)+".dat")) {
      optimizeTopology();
      write(prefix+"/iter"+boost::lexical_cast<string>(i)+".dat");
      INFOV("Iter %ld: reduced from %f to %f!",i,FX,_result._FX)
    } else {
      read(prefix+"/iter"+boost::lexical_cast<string>(i)+".dat");
      INFOV("Read iter %ld: reduced from %f to %f!",i,FX,_result._FX)
    }
    if(debugTraj)
      writeTrajVTK(prefix+"/animIter"+boost::lexical_cast<string>(i));
    if(_result._FX >= FX) {
      INFO("Exit on same function value!")
      break;
    }
  }
}
//brute force search of external force
template <int DIM>
bool TrajectoryOptimization<DIM>::optimizeForceBruteForce()
{
  Vec3d range(_pt.get<scalarD>("force.minF",0),_pt.get<scalarD>("force.maxF",1E10f),_pt.get<scalarD>("force.deltaF",1E2f));
  scalarD dec=_pt.get<scalarD>("force.stepDec",_pt.get<scalarD>("topo.stepDec",0.8f));
  scalarD inc=_pt.get<scalarD>("force.stepInc",_pt.get<scalarD>("topo.stepInc",1.25f));
  scalarD target=_pt.get<scalarD>("force.target");
  bool useCB=_pt.get<bool>("force.useCB",true);
  scalarD deltaF=range[2];
  //initialize
  TopologyOptimizationRecord lastR;
  ASSERT_MSGV(_varF.cols() == 1,"BruteForce search only supports 1 variable (%ld found)!",_varF.cols())
  scalarD off=_varCoefPss.cols();
  //brute-force search force
  forward();
  for(scalarD f=range[0]; f<range[1]; f+=deltaF) {
    _result._var[off]=f;
    lastR=_result;
    if(!minimizeTrajectoryGradient()) {
      f-=deltaF;
      deltaF*=dec;
      _result=lastR;
      if(useCB) {
        WARNINGV("Rejected force, minimizeTrajectoryGradient failed, deltaF: %f!",deltaF)
      }
      continue;
    } else {
      deltaF=min(deltaF*inc,range[2]);
    }
    _result._FX=_obj->operator()(_result._xss,NULL,_result._var,NULL);
    if(useCB) {
      INFOV("optimizeForceBruteForce at force magnitude: %f, objective: %f, target: %f, deltaF: %f!",_result._var[off],_result._FX,target,deltaF)
    }
    //binary search force
    if(f > range[0] && (_result._FX-target)*(lastR._FX-target) <= 0) {
      if(useCB) {
        INFO("Enter binary-search loop!")
      }
      return optimizeForceBinarySearch(_result,lastR);
    } else lastR=_result;
  }
  return false;
}
template <int DIM>
bool TrajectoryOptimization<DIM>::optimizeForceBinarySearch(TopologyOptimizationRecord A,TopologyOptimizationRecord B)
{
  bool useCB=_pt.get<bool>("force.useCB",true);
  scalarD eps=_pt.get<scalarD>("force.epsF",1.0f);
  scalarD epsTarget=_pt.get<scalarD>("force.epsT",1E-6f);
  scalarD target=_pt.get<scalarD>("force.target");
  //initialize
  ASSERT_MSGV((A._FX-target)*(B._FX-target) <= 0,"Invalid initial condition for binary search (A=%f,B=%f,target=%f)!",A._FX,B._FX,target)
  scalarD off=_varCoefPss.cols();
  while(true) {
#define USE_SECANT
#ifdef USE_SECANT
    scalarD alpha=(A._FX-target)/(A._FX-B._FX);
    _result._var=interp1D(A._var,B._var,alpha);
#else
    _result._var=(A._var+B._var)/2;
#endif
    //forward();
    if(!minimizeTrajectoryGradient()) {
      if(useCB) {
        WARNING("minimizeTrajectoryGradient failed!")
      }
      return false;
    }
    _result._FX=_obj->operator()(_result._xss,NULL,_result._var,NULL);
    if(useCB) {
      INFOV("optimizeForceBinarySearch at force magnitude: %f objective: %f, target: %f!",_result._var[off],_result._FX,target)
    }
    if(abs(_result._FX-target) < epsTarget) {
      if(useCB) {
        INFOV("Error small, exit on epsTarget: %f, err: %f!",epsTarget,abs(_result._FX-target))
      }
      return true;
    }
    if((_result._FX-target)*(A._FX-target) > 0)
      A=_result;
    else if((_result._FX-target)*(B._FX-target) > 0)
      B=_result;
    if((A._var-B._var).norm() < eps) {
      if(useCB) {
        INFOV("Error small, exit on eps: %f, err: %f!",eps,(A._var-B._var).norm())
      }
      return true;
    }
  }
  return true;
}
template <int DIM>
void TrajectoryOptimization<DIM>::makeLocalMinima()
{
  Vec DFDX;
  scalarD FX;
  topologyGradient(_result._var,FX,DFDX);
  _obj->makeLocalMinima(_result._xss,_result._dxss,_result._var,DFDX);
  _result._FX=_obj->operator()(_result._xss,NULL,_result._var,NULL);
}
//debug
template <int DIM>
void TrajectoryOptimization<DIM>::forward()
{
  _model->resetF();
  _model->getCoefPss()=_varCoefPss*_result._var.segment(0,_varCoefPss.cols())+_varCoefP0;
  _model->getF()+=_varF*_result._var.segment(_varCoefPss.cols(),_varF.cols());
  //forward propagation
  bool useCB=_pt.get<bool>("useCB",false);
  _result._xss.clear();
  scalarD time=0;
  for(sizeType i=0; time<_pt.get<scalarD>("t"); i++) {
    if(i == 0)
      _model->getXN()=_model->getX()=_model->getX0();
    else {
      _model->getXN()=_result._xss[max<sizeType>(i-2,0)];
      _model->getX()=_result._xss[max<sizeType>(i-1,0)];
    }
    //advance
    Vec* initGuess=i<(sizeType)_warmStart.size() ? &_warmStart[i] : NULL;
    scalarD dt=i == 0 ? _pt.get<scalarD>("dt0") : _pt.get<scalarD>("dt");
    _model->advance(dt,useCB,initGuess);
    _result._xss.push_back(_model->getXP());
    //update configuration
    if(i == 0)
      _model->resetF();
    time+=_pt.get<scalarD>("dt");
  }
  _warmStart=_result._xss;
}
template <int DIM>
void TrajectoryOptimization<DIM>::backward(Vec* dfdx)
{
  sizeType nT=(sizeType)_result._xss.size();
  while((sizeType)_result._hessSols.size() < nT)
    _result._hessSols.push_back(_interface->copy());
  //main loop: stage 1
  Vec f;
  sizeType nrFailed=0;
  _model->getCoefPss()=_varCoefPss*_result._var.segment(0,_varCoefPss.cols())+_varCoefP0;
  _model->buildMass();
  OMP_PARALLEL_FOR_I(OMP_PRI(f) OMP_ADD(nrFailed))
  for(sizeType i=0; i<nT; i++) {
    bool ret=true;
    if(i == 0) {
      f=_varF*_result._var.segment(_varCoefPss.cols(),_varF.cols());
      ret=_model->adjointTransferStage1ThreadSafe(_pt.get<scalarD>("dt0"),
          _result._xss[i],
          _model->getX0(),
          _model->getX0(),f,*(_result._hessSols[i]));
    } else {
      f=Vec::Zero(_model->getG().size());
      ret=_model->adjointTransferStage1ThreadSafe(_pt.get<scalarD>("dt"),
          _result._xss[i],
          _result._xss[max<sizeType>(i-1,0)],
          _result._xss[max<sizeType>(i-2,0)],f,*(_result._hessSols[i]));
    }
    if(!ret)
      nrFailed++;
  }
  if(nrFailed > 0) {
    WARNING("Backward: factorization failed!")
    write("./backward_error.dat");
    exit(EXIT_FAILURE);
  }
  //main loop: stage 2
  if(!dfdx)
    return;
  Vec DEDX,DEDXN,DEDCoefP,DEDF;
  for(sizeType i=nT-1; i>=0; i--) {
    if(i == 0) {
      _model->adjointTransferStage2ThreadSafe(_pt.get<scalarD>("dt0"),
                                              _result._xss[i],
                                              _model->getX0(),
                                              _model->getX0(),
                                              _result._dxss[0],NULL,NULL,&DEDCoefP,&DEDF,*(_result._hessSols[0]));
      dfdx->segment(_varCoefPss.cols(),_varF.cols())+=_varF.transpose()*DEDF;
      dfdx->segment(0,_varCoefPss.cols())+=_varCoefPss.transpose()*DEDCoefP;
    } else {
      _model->adjointTransferStage2ThreadSafe(_pt.get<scalarD>("dt"),
                                              _result._xss[i],
                                              _result._xss[max<sizeType>(i-1,0)],
                                              _result._xss[max<sizeType>(i-2,0)],
                                              _result._dxss[i],&DEDX,&DEDXN,&DEDCoefP,NULL,*(_result._hessSols[i]));
      _result._dxss[max<sizeType>(i-2,0)].segment(0,DEDXN.size())+=DEDXN;
      _result._dxss[max<sizeType>(i-1,0)].segment(0,DEDX.size())+=DEDX;
      dfdx->segment(0,_varCoefPss.cols())+=_varCoefPss.transpose()*DEDCoefP;
    }
  }
}
template <int DIM>
bool TrajectoryOptimization<DIM>::lineSearchTrajectory(TopologyOptimizationRecord& grad,scalarD& stp,scalarD gNorm,scalarD& gNorm2)
{
  bool useCB=_pt.get<bool>("trajGrad.useCBLineSearch",false);
  scalarD dec=_pt.get<scalarD>("trajGrad.stepDec",0.5f);
  scalarD tolX=_pt.get<scalarD>("trajGrad.tolX",1E-9f);
  TopologyOptimizationRecord grad2,tmp=_result;
  //main loop
  scalarD xMax=0;
  for(sizeType x=0; x<(sizeType)grad._dxss.size(); x++)
    xMax=max(xMax,grad._dxss[x].maxCoeff());
  while(xMax*stp > tolX) {
    for(sizeType x=0; x<(sizeType)grad._xss.size(); x++)
      _result._xss[x]-=grad._dxss[x]*stp;
    trajectoryGradient(grad2,false);
    gNorm2=0;
    for(sizeType x=0; x<(sizeType)grad2._xss.size(); x++)
      gNorm2+=grad2._xss[x].squaredNorm();
    if(gNorm2 < gNorm) {
      if(useCB) {
        INFOV("Reduced to gNorm2: %f, stp: %f!",gNorm2,stp)
      }
      return true;
    } else {
      stp*=dec;
      _result=tmp;
      if(useCB) {
        WARNINGV("Cannot reduce, gNorm2: %f, stp: %f!",gNorm2,stp)
      }
    }
  }
  return false;
}
template <int DIM>
void TrajectoryOptimization<DIM>::topologyGradient(const Vec& x,scalarD& FX,Vec& DFDX)
{
  //forward
  _result._var=x;
  forward();
  //obj
  DFDX.setZero(x.size());
  _result._dxss.assign(_result._xss.size(),Vec::Zero(_result._xss[0].size()));
  FX=(*_obj)(_result._xss,&(_result._dxss),x,&DFDX);
  //backward
  backward(&DFDX);
}
template <int DIM>
void TrajectoryOptimization<DIM>::trajectoryGradient(TopologyOptimizationRecord& ret,bool computeDxss)
{
  scalarD dtCoef=1/pow(_pt.get<scalarD>("dt"),2);
  sizeType nT=(sizeType)_result._xss.size();
  sizeType nrX=_model->getM().rows();
  ret._xss.resize(nT);
  if(computeDxss) {
    ret._hesses.resize(_result._xss.size());
    ret._dxss.resize(nT);
    while((sizeType)ret._hessSols.size() < nT)
      ret._hessSols.push_back(_interface->copy());
  }
  //main loop
  _model->getCoefPss()=_varCoefPss*_result._var.segment(0,_varCoefPss.cols())+_varCoefP0;
  _model->buildMass();
  omp_set_nested(0);
  STrips trips;
  Vec f;
  OMP_PARALLEL_FOR_I(OMP_PRI(trips,f))
  for(sizeType i=0; i<nT; i++) {
    if(computeDxss)
      trips.clear(1);
    if(i == 0) {
      f=_varF*_result._var.segment(_varCoefPss.cols(),_varF.cols());
      _model->operatorThreadSafe(_pt.get<scalarD>("dt0"),
                                 _result._xss[i],
                                 _model->getX0(),
                                 _model->getX0(),f,&(ret._xss[i]),
                                 computeDxss ? &trips : NULL);
    } else {
      f=Vec::Zero(_model->getG().size());
      _model->operatorThreadSafe(_pt.get<scalarD>("dt"),
                                 _result._xss[i],
                                 _result._xss[max<sizeType>(i-1,0)],
                                 _result._xss[max<sizeType>(i-2,0)],f,&(ret._xss[i]),
                                 computeDxss ? &trips : NULL);
    }
    if(computeDxss) {
      ret._hesses[i].resize(_model->inputs(),_model->inputs());
      ret._hesses[i].setFromTriplets(trips.begin(),trips.end());
      ret._hessSols[i]->recompute(ret._hesses[i],0,false);
    }
  }
  if(computeDxss)
    for(sizeType i=0; i<nT; i++) {
      Vec& dx=ret._dxss[i]=ret._xss[i];
      if(i > 0)
        dx.segment(0,nrX)+=_model->getM()*(ret._dxss[max<sizeType>(i-1,0)]*2-ret._dxss[max<sizeType>(i-2,0)]).segment(0,nrX)*dtCoef;
      dx=ret._hessSols[i]->solve(dx);
    }
}
template <int DIM>
void TrajectoryOptimization<DIM>::identifySym(boost::unordered_map<sizeType,sizeType>& identify) const
{
#define BB BBox<scalarD,DIM>
#define NODE Node<sizeType,BB>
  //map pss
  boost::unordered_map<Coli,sizeType,Hash> pMap;
  const vector<boost::shared_ptr<ElasticModel<DIM> > >& pss=_model->getPss();
  for(sizeType i=0; i<(sizeType)pss.size(); i++) {
    Coli ids=pss[i]->ids();
    sort(ids.data(),ids.data()+ids.size());
    pMap[ids]=i;
  }
  //map xss
  NODE node;
  vector<NODE> bvh;
  scalarD eps=1E-5f;
  const Vec& x0=_model->getX0();
  for(sizeType i=0; i<x0.size()/DIM; i++) {
    node._bb._minC=node._bb._maxC=VecDIMd::Zero();
    node._bb._minC=x0.template segment<DIM>(i*DIM)-VecDIMd::Constant(eps);
    node._bb._maxC=x0.template segment<DIM>(i*DIM)+VecDIMd::Constant(eps);
    node._cell=i;
    node._nrCell=1;
    node._l=node._r=-1;
    bvh.push_back(node);
  }
  BVHBuilder<NODE,DIM>().buildBVH(bvh);
  boost::unordered_map<sizeType,sizeType> vMap;
  for(sizeType i=x0.size()/DIM; i<(sizeType)bvh.size(); i++)
    bvh[i]._cell=-1;
  for(sizeType i=0; i<x0.size()/DIM; i++) {
    DistanceCallback<DIM> cb(x0.template segment<DIM>(i*DIM).cwiseAbs());
    BVHQuery<sizeType,BB>(bvh,DIM,-1).pointQuery(cb);
    ASSERT_MSG(cb._id >= 0,"Cannot identify vertex!")
    vMap[i]=cb._id;
  }
  //identify
  for(sizeType i=0; i<(sizeType)pss.size(); i++) {
    Coli ids=pss[i]->ids();
    Coli ids0=pss[i]->ids();
    for(sizeType j=0; j<ids.size(); j++) {
      ASSERT_MSG(vMap.find(ids[j]) != vMap.end(),"Cannot identify vertex!")
      ids[j]=vMap[ids[j]];
    }
    sort(ids.data(),ids.data()+ids.size());
    sort(ids0.data(),ids0.data()+ids0.size());
    ASSERT_MSG(pMap.find(ids) != pMap.end(),"Cannot identify element!")
    identify[pMap[ids0]]=pMap[ids];
  }
}
template <int DIM>
sizeType TrajectoryOptimization<DIM>::findTopVid() const
{
  Vec3d x=Vec3d::Zero();
  Vec3d g=Vec3d::Zero();
  scalarD len=0;
  sizeType id=-1;
  const Vec& x0=_model->getX0();
  for(sizeType i=0; i<x0.size()/DIM; i++) {
    x.template segment<DIM>(0)=x0.template segment<DIM>(i*DIM);
    g.template segment<DIM>(0)=_model->getG().template segment<DIM>(i*DIM);
    if(x.cross(g).norm() < 1E-5f && -x.dot(g) > len) {
      len=-x.dot(g);
      id=i;
    }
  }
  ASSERT_MSG(id >= 0,"Cannot find TopVid!")
  return id;
}
template class TrajectoryOptimization<2>;
template class TrajectoryOptimization<3>;

PRJ_END
