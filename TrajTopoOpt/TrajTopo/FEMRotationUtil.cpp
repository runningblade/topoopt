#include "FEMRotationUtil.h"
#include <iostream>

//cubatureGPU.txt
extern "C" {
  extern unsigned char cubatureGPU_hpp[];
  extern unsigned int cubatureGPU_hpp_len;
}

PRJ_BEGIN

//basic
FORCE_INLINE Mat3d crossD(sizeType d)
{
  static Mat3d crossDMat[3]= {
    cross<scalarD>(Vec3d::Unit(0)),
    cross<scalarD>(Vec3d::Unit(1)),
    cross<scalarD>(Vec3d::Unit(2)),
  };
  return crossDMat[d];
}
//rotation along X,Y
Mat3d expWYZ(scalarD y,scalarD z,Vec3d* DRDY,Vec3d* DRDZ,Vec3d* DRDYDZ)
{
  scalarD sy,cy,sz,cz;
  sincos(y,&sy,&cy);
  sincos(z,&sz,&cz);

  Mat3d ret;
  ret <<
      cy*cz,-sz,sy*cz,
      cy*sz, cz,sy*sz,
      -sy,    0,   cy;
  if(DRDY)
    *DRDY << -sz,cz,0;
  if(DRDZ)
    *DRDZ <<   0, 0,1;
  if(DRDYDZ)
    *DRDYDZ << -cz,-sz,0;
  return ret;
}
//rotation using rodriguez formulation
static const scalarD DELTA_BASIC_VARIABLES=1E-6f;
FORCE_INLINE bool basicVariables(const Vec3d& w,scalarD& theta,Vec3d& wBar,scalarD& A,scalarD& B,scalarD& C,scalarD& D,scalarD& E,scalarD& F,scalarD& G)
{
  //A=sin(theta)
  //B=cos(theta)
  //C=sin(theta)/theta
  //D=(1-cos(theta))/theta
  //E=(1-cos(theta))/theta/theta
  //F=(theta-sin(theta))/theta/theta
  //G=(sin(theta)-theta*cos(theta))/theta/theta
  theta=w.norm();
  if(theta > DELTA_BASIC_VARIABLES) {
    scalarD invTheta=1/theta;
    scalarD invTheta2=invTheta*invTheta;
    wBar=w*invTheta;
    sincos(theta,&A,&B);
    C=A*invTheta;
    D=(1-cos(theta))*invTheta;
    E=D*invTheta;
    F=(theta-A)*invTheta2;
    G=(A-theta*B)*invTheta2;
    return false;
  } else {
    wBar=Vec3d(0,0,1);
    A=0;
    B=1;
    C=1;
    D=0;
    E=0.5f;
    F=0;
    G=0;
    return true;
  }
}
FORCE_INLINE int findNonZeroIndex(const Vec3d& val)
{
  sizeType id;
  val.cwiseAbs().maxCoeff(&id);
  return id;
}
FORCE_INLINE int crossAxis(int a,int b)
{
  static const int data[3][3]= {
    findNonZeroIndex(cross<scalarD>(Vec3d::Unit(0))*Vec3d::Unit(0)),
    findNonZeroIndex(cross<scalarD>(Vec3d::Unit(0))*Vec3d::Unit(1)),
    findNonZeroIndex(cross<scalarD>(Vec3d::Unit(0))*Vec3d::Unit(2)),

    findNonZeroIndex(cross<scalarD>(Vec3d::Unit(1))*Vec3d::Unit(0)),
    findNonZeroIndex(cross<scalarD>(Vec3d::Unit(1))*Vec3d::Unit(1)),
    findNonZeroIndex(cross<scalarD>(Vec3d::Unit(1))*Vec3d::Unit(2)),

    findNonZeroIndex(cross<scalarD>(Vec3d::Unit(2))*Vec3d::Unit(0)),
    findNonZeroIndex(cross<scalarD>(Vec3d::Unit(2))*Vec3d::Unit(1)),
    findNonZeroIndex(cross<scalarD>(Vec3d::Unit(2))*Vec3d::Unit(2)),
  };
  return data[a][b];
}
FORCE_INLINE int kronecker(int a,int b)
{
  return a==b?1:0;
}
FORCE_INLINE scalarD crossAxisSgn(int a,int b)
{
  static const scalarD data[3][3]= {
    (cross<scalarD>(Vec3d::Unit(0))*Vec3d::Unit(0)).sum(),
    (cross<scalarD>(Vec3d::Unit(0))*Vec3d::Unit(1)).sum(),
    (cross<scalarD>(Vec3d::Unit(0))*Vec3d::Unit(2)).sum(),

    (cross<scalarD>(Vec3d::Unit(1))*Vec3d::Unit(0)).sum(),
    (cross<scalarD>(Vec3d::Unit(1))*Vec3d::Unit(1)).sum(),
    (cross<scalarD>(Vec3d::Unit(1))*Vec3d::Unit(2)).sum(),

    (cross<scalarD>(Vec3d::Unit(2))*Vec3d::Unit(0)).sum(),
    (cross<scalarD>(Vec3d::Unit(2))*Vec3d::Unit(1)).sum(),
    (cross<scalarD>(Vec3d::Unit(2))*Vec3d::Unit(2)).sum(),
  };
  return data[a][b];
}
Mat3d expWBF(const Vec3d& w)
{
  Mat3d R=Mat3d::Identity();
  Mat3d C=cross<scalarD>(w),coef=Mat3d::Identity();
  for(int i=1;; i++) {
    coef*=C/(scalarD)i;
    R+=coef;
    if(coef.cwiseAbs().maxCoeff() < 1E-7f)
      break;
  }
  return R;
}
Mat3d expWGradInte(const Vec3d& w)
{
  //used in: Modal Warping: Real-Time Simulation of Large Rotational Deformation and Manipulation
  Vec3d wBar;
  scalarD theta,A,B,C,D,E,F,G;
  basicVariables(w,theta,wBar,A,B,C,D,E,F,G);
  Mat3d CROSS=cross<scalarD>(wBar);
  return Mat3d::Identity()+D*CROSS+(1-C)*CROSS*CROSS;
}
Mat3d expWGradInteOpt(const Vec3d& w)
{
  Mat3d RInt;
  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;
  scalarD tt9;
  scalarD tt10;
  scalarD tt11;
  scalarD tt12;
  scalarD tt13;
  scalarD tt14;

  tt1=pow(w[1],2);
  tt2=pow(w[0],2);
  tt3=pow(w[2],2);
  tt4=std::max<scalarD>(tt3+tt1+tt2,1E-12);
  tt5=1/tt4;
  tt6=-tt1*tt5;
  tt7=-tt3*tt5;
  tt8=sqrt(tt4);
  tt9=1-sin(tt8)/tt8;
  tt10=1-cos(tt8);
  tt11=w[0]*w[1]*tt5*tt9;
  tt12=w[0]*w[2]*tt5*tt9;
  tt13=-tt2*tt5;
  tt14=w[1]*w[2]*tt5*tt9;
  RInt(0,0)=(tt7+tt6)*tt9+1;
  RInt(0,1)=tt11-w[2]*tt5*tt10;
  RInt(0,2)=tt12+w[1]*tt5*tt10;
  RInt(1,0)=tt11+w[2]*tt5*tt10;
  RInt(1,1)=(tt7+tt13)*tt9+1;
  RInt(1,2)=tt14-w[0]*tt5*tt10;
  RInt(2,0)=tt12-w[1]*tt5*tt10;
  RInt(2,1)=tt14+w[0]*tt5*tt10;
  RInt(2,2)=(tt6+tt13)*tt9+1;
  return RInt;
}
Vec3d invExpW(const Mat3d& R)
{
  scalarD cosTheta=max<scalarD>((R.trace()-1)/2,DELTA_BASIC_VARIABLES-1);
  cosTheta=min<scalarD>(cosTheta,1-DELTA_BASIC_VARIABLES);
  scalarD theta=acos(cosTheta),sinTheta=sin(theta);
  if(abs(sinTheta) < DELTA_BASIC_VARIABLES)
    sinTheta=sgn(sinTheta)*DELTA_BASIC_VARIABLES;
  scalarD coef=theta/(2*sinTheta);
  return Vec3d(R(2,1)-R(1,2),R(0,2)-R(2,0),R(1,0)-R(0,1))*coef;
}
template <typename TDiff,typename TDDiff,typename TDiffRotT>
Mat3d expWGradV(const Vec3d& w,TDiff diffW[3],TDDiff ddiffW[3][3],const Vec3d* wDotT,TDiffRotT diffWDotT[3],Vec3d diffVOut[3],Vec3d vOut[3][3])
{
  Vec3d wBar,diffV[3],v;
  scalarD theta,A,B,C,D,E,F,G;
  bool singular=basicVariables(w,theta,wBar,A,B,C,D,E,F,G);
  Mat3d CROSS,R;
  if(singular) {
    CROSS=cross<scalarD>(w);
    R=Mat3d::Identity()+CROSS/2;
    if(diffW || diffVOut)
      for(int d=0; d<3; d++) {
        //gradient
        if(diffW)
          diffW[d]=crossD(d)+(crossD(d)*CROSS+CROSS*crossD(d))/2;
        if(diffVOut)
          diffVOut[d]=Vec3d::Unit(d);
        //hessian
        if(ddiffW)
          for(int d2=0; d2<3; d2++)
            ddiffW[d][d2]=(crossD(d)*crossD(d2)+crossD(d2)*crossD(d))/2;
        if(vOut)
          for(int d2=0; d2<3; d2++)
            vOut[d][d2].setZero();
      }
    //gradient dotT
    if(wDotT && diffWDotT && (diffW || diffVOut))
      for(int d=0; d<3; d++)
        diffWDotT[d]=(crossD(d)*cross<scalarD>(*wDotT)+cross<scalarD>(*wDotT)*crossD(d))/2;
    return Mat3d::Identity()+CROSS*R;
  } else {
    CROSS=cross<scalarD>(wBar);
    R=Mat3d::Identity()*B+CROSS*A+wBar*wBar.transpose()*(1-B);
    //gradient
    if(diffW || diffVOut)
      for(int d=0; d<3; d++) {
        diffV[d]=(1-C)*wBar*wBar[d]+D*CROSS.col(d);
        diffV[d][d]+=C;
        if(diffW)
          diffW[d]=cross<scalarD>(diffV[d])*R;
        if(diffVOut)
          diffVOut[d]=diffV[d];
      }
    //gradient dotT
    if(wDotT && diffWDotT && (diffW || diffVOut)) {
      scalarD wBarDotWDotT=wBar.dot(*wDotT);
      Mat3d diffWDotWDotT=diffW[0]*(*wDotT)[0]+diffW[1]*(*wDotT)[1]+diffW[2]*(*wDotT)[2];
      for(int d=0; d<3; d++) {
        v=((G-2*F)*wBar[d]*wBarDotWDotT+(*wDotT)[d]*F)*wBar;
        v+=(C-2*E)*CROSS.col(d)*wBarDotWDotT;
        v+=E*cross<scalarD>(*wDotT).col(d);
        v[d]-=wBarDotWDotT*G;
        v+=(wBar[d]*F)**wDotT;
        diffWDotT[d]=cross<scalarD>(v)*R+cross<scalarD>(diffV[d])*diffWDotWDotT;
      }
    }
    //hessian
    if((diffW || diffVOut) && (ddiffW || vOut))
      for(int d=0; d<3; d++) {
        for(int d2=d; d2<3; d2++) {
          v=((G-2*F)*wBar[d]*wBar[d2]+kronecker(d,d2)*F)*wBar;
          v+=(C-2*E)*CROSS.col(d)*wBar[d2];
          //v+=E*cross(Vec3d::Unit(d2))*Vec3d::Unit(d);
          //std::cout << d << " " << d2 << " " << crossAxis(d2,d) << " " << crossAxisSgn(d2,d) << endl;
          v[d]-=wBar[d2]*G;
          v[d2]+=wBar[d]*F;
          v[crossAxis(d2,d)]+=E*crossAxisSgn(d2,d);
          if(ddiffW)
            ddiffW[d][d2]=ddiffW[d2][d]=cross<scalarD>(v)*R+cross<scalarD>(diffV[d])*diffW[d2];
          if(vOut)
            vOut[d][d2]=vOut[d2][d]=v;
        }
      }
    return R;
  }
}
void debugExpWGrad(bool debugBasic,bool debugDiff,bool debugDiffDotT,bool debugInte,int maxIt)
{
#define DELTA 1E-7f
  for(int it=0; it<maxIt; it++) {
    //debug basic variable
    if(debugBasic) {
      scalarD theta;
      scalarD A1,B1,C1,D1,E1,F1,G1;
      scalarD A2,B2,C2,D2,E2,F2,G2;
      Vec3d w2=Vec3d::Constant(1E-7f);
      Vec3d w1=Vec3d::Constant(1E-6f),wBar;
      basicVariables(w1,theta,wBar,A1,B1,C1,D1,E1,F1,G1);
      basicVariables(w2,theta,wBar,A2,B2,C2,D2,E2,F2,G2);
      INFOV("Err: A%f B%f C%f D%f E%f F%f G%f",
            A1-A2,B1-B2,C1-C2,D1-D2,E1-E2,F1-F2,G1-G2)
    }

    //debug rodrigues formula and derivative
    if(debugDiff) {
      Vec3d w=Vec3d::Random();
      if(RandEngine::randR01() < 0.05f)
        w.setZero();
      Mat3d A=expWBF(w),diffWZ[3],diffWZ2[3],ddiffWZ[3][3];
      Mat3d B=expWGradV<Mat3d,Mat3d,Mat3d>(w,diffWZ,ddiffWZ,NULL,NULL);
      INFOV("Norm: %f Err: %f",A.norm(),(A-B).norm())
      for(int d=0; d<3; d++) {
        Mat3d C2=expWGradV<Mat3d,Mat3d,Mat3d>(w+Vec3d::Unit(d)*DELTA,diffWZ2,NULL,NULL,NULL);
        INFOV("Diff%d: %f Err: %f",d,diffWZ[d].norm(),(diffWZ[d]-(C2-B)/DELTA).norm())
        INFOV("DDiff%d: %f %f %f ErrZ: %f %f %f",d,
              ddiffWZ[0][d].norm(),ddiffWZ[1][d].norm(),ddiffWZ[2][d].norm(),
              (ddiffWZ[0][d]-(diffWZ2[0]-diffWZ[0])/DELTA).norm(),
              (ddiffWZ[1][d]-(diffWZ2[1]-diffWZ[1])/DELTA).norm(),
              (ddiffWZ[2][d]-(diffWZ2[2]-diffWZ[2])/DELTA).norm())
      }
    }

    //debug rodrigues formula and derivative
    if(debugDiffDotT) {
      Vec3d w=Vec3d::Random(),wDotT=Vec3d::Random();
      if(RandEngine::randR01() < 0.05f)
        w.setZero();
      Mat3d diffW[3],diffW2[3],diffWDotT[3];
      expWGradV<Mat3d,Mat3d,Mat3d>(w,diffW,NULL,&wDotT,diffWDotT);
      expWGradV<Mat3d,Mat3d,Mat3d>(w+wDotT*DELTA,diffW2,NULL,NULL,NULL);
      INFOV("DiffDotT: %f %f %f Err: %f %f %f",
            diffWDotT[0].norm(),diffWDotT[1].norm(),diffWDotT[2].norm(),
            (diffWDotT[0]-(diffW2[0]-diffW[0])/DELTA).norm(),
            (diffWDotT[1]-(diffW2[1]-diffW[1])/DELTA).norm(),
            (diffWDotT[2]-(diffW2[2]-diffW[2])/DELTA).norm())
    }

    //debug exponential integral
    if(debugInte) {
      Vec3d test=Vec3d::Random();
      INFOV("ExpWInte: %f Err: %f",expWGradInte(test).norm(),(expWGradInte(test)-expWGradInteOpt(test)).norm())
    }
  }
#undef DELTA
}
void debugInvExpW(int maxIt)
{
  for(sizeType i=0; i<maxIt; i++) {
    Vec3d w=Vec3d::Random();
    Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(w,NULL,NULL,NULL,NULL);
    Vec3d wRef=invExpW(R);
    Mat3d RRef=expWGradV<Mat3d,Mat3d,Mat3d>(wRef,NULL,NULL,NULL,NULL);
    INFOV("w: %f Err: %f",R.norm(),(R-RRef).norm())
  }
}
//rotation-strain transformation
FORCE_INLINE Mat3d unitMat3d(int r,int c)
{
  Mat3d ret=Mat3d::Zero();
  ret(r,c)=1;
  return ret;
}
template <typename TDiff,typename TDDiff,typename TDiffRotT>
Mat3d calcFGrad(const Mat3d& RS,TDiff diffRS[9],TDDiff ddiffRS[9][9],const Mat3d* RSDotT,TDiffRotT diffRSDotT[9])
{
#define FETCH(R,C) (R)+(C)*3
  static const Mat3d mask[6]= {
    unitMat3d(1,2)+unitMat3d(2,1),
    unitMat3d(0,2)+unitMat3d(2,0),
    unitMat3d(0,1)+unitMat3d(1,0),
    unitMat3d(0,0),
    unitMat3d(1,1),
    unitMat3d(2,2),
  };

  Vec3d w=invCross<scalarD>((RS-RS.transpose())/2),wDotT;
  if(RSDotT)
    wDotT=invCross<scalarD>((*RSDotT-RSDotT->transpose())/2);

  Mat3d diffR[3],ddiffR[3][3],diffRDotT[3],tmp,tmp2;
  Mat3d R=expWGradV(w,(diffRS||ddiffRS||diffRSDotT)?diffR:NULL,
                    (ddiffRS||diffRSDotT)?ddiffR:NULL,
                    diffRSDotT?&wDotT:NULL,
                    diffRSDotT?diffRDotT:NULL);
  Mat3d S=(RS+RS.transpose())/2+Mat3d::Identity();
  if(diffRS) {
    //offdiag
    tmp=R*mask[0];
    tmp2=diffR[0]*S;
    diffRS[FETCH(1,2)]=(tmp-tmp2)/2;
    diffRS[FETCH(2,1)]=(tmp+tmp2)/2;
    tmp=R*mask[1];
    tmp2=diffR[1]*S;
    diffRS[FETCH(0,2)]=(tmp+tmp2)/2;
    diffRS[FETCH(2,0)]=(tmp-tmp2)/2;
    tmp=R*mask[2];
    tmp2=diffR[2]*S;
    diffRS[FETCH(0,1)]=(tmp-tmp2)/2;
    diffRS[FETCH(1,0)]=(tmp+tmp2)/2;
    //diag
    diffRS[FETCH(0,0)]=R*mask[3];
    diffRS[FETCH(1,1)]=R*mask[4];
    diffRS[FETCH(2,2)]=R*mask[5];
  }
  if(ddiffRS) {
    //for(int i=0;i<9;i++)
    //  for(int j=0;j<9;j++)
    //    ddiffRS[i][j].setZero();

    //offdiag
#define ADD_OFFDIAG(R,MASKID,SGN)  \
ddiffRS[R][FETCH(2,1)]=( diffR[0]*mask[MASKID]+SGN (diffR[MASKID]*mask[0]+ddiffR[MASKID][0]*S))/4;  \
ddiffRS[R][FETCH(1,2)]=(-diffR[0]*mask[MASKID]+SGN (diffR[MASKID]*mask[0]-ddiffR[MASKID][0]*S))/4;  \
ddiffRS[R][FETCH(2,0)]=(-diffR[1]*mask[MASKID]+SGN (diffR[MASKID]*mask[1]-ddiffR[MASKID][1]*S))/4;  \
ddiffRS[R][FETCH(0,2)]=( diffR[1]*mask[MASKID]+SGN (diffR[MASKID]*mask[1]+ddiffR[MASKID][1]*S))/4;  \
ddiffRS[R][FETCH(1,0)]=( diffR[2]*mask[MASKID]+SGN (diffR[MASKID]*mask[2]+ddiffR[MASKID][2]*S))/4;  \
ddiffRS[R][FETCH(0,1)]=(-diffR[2]*mask[MASKID]+SGN (diffR[MASKID]*mask[2]-ddiffR[MASKID][2]*S))/4;  \
ddiffRS[R][FETCH(0,0)]=SGN diffR[MASKID]*mask[3]/2;  \
ddiffRS[R][FETCH(1,1)]=SGN diffR[MASKID]*mask[4]/2;  \
ddiffRS[R][FETCH(2,2)]=SGN diffR[MASKID]*mask[5]/2;
    ADD_OFFDIAG(FETCH(1,2),0,-)
    ADD_OFFDIAG(FETCH(2,1),0, )
    ADD_OFFDIAG(FETCH(0,2),1, )
    ADD_OFFDIAG(FETCH(2,0),1,-)
    ADD_OFFDIAG(FETCH(0,1),2,-)
    ADD_OFFDIAG(FETCH(1,0),2, )
#undef ADD_OFFDIAG

    //diag
#define ADD_DIAG(R,MASKID)  \
ddiffRS[R][FETCH(2,1)]= diffR[0]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(1,2)]=-diffR[0]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(2,0)]=-diffR[1]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(0,2)]= diffR[1]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(1,0)]= diffR[2]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(0,1)]=-diffR[2]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(0,0)].setZero();  \
ddiffRS[R][FETCH(1,1)].setZero();  \
ddiffRS[R][FETCH(2,2)].setZero();
    ADD_DIAG(FETCH(0,0),3)
    ADD_DIAG(FETCH(1,1),4)
    ADD_DIAG(FETCH(2,2),5)
#undef ADD_DIAG
  }
  if(diffRSDotT) {
    Mat3d RdotT=diffR[0]*wDotT[0]+diffR[1]*wDotT[1]+diffR[2]*wDotT[2];
    Mat3d SdotT=(*RSDotT+RSDotT->transpose())/2;
    //offdiag
    tmp=RdotT*mask[0];
    tmp2=diffRDotT[0]*S+diffR[0]*SdotT;
    diffRSDotT[FETCH(1,2)]=(tmp-tmp2)/2;
    diffRSDotT[FETCH(2,1)]=(tmp+tmp2)/2;
    tmp=RdotT*mask[1];
    tmp2=diffRDotT[1]*S+diffR[1]*SdotT;
    diffRSDotT[FETCH(0,2)]=(tmp+tmp2)/2;
    diffRSDotT[FETCH(2,0)]=(tmp-tmp2)/2;
    tmp=RdotT*mask[2];
    tmp2=diffRDotT[2]*S+diffR[2]*SdotT;
    diffRSDotT[FETCH(0,1)]=(tmp-tmp2)/2;
    diffRSDotT[FETCH(1,0)]=(tmp+tmp2)/2;
    //diag
    diffRSDotT[FETCH(0,0)]=RdotT*mask[3];
    diffRSDotT[FETCH(1,1)]=RdotT*mask[4];
    diffRSDotT[FETCH(2,2)]=RdotT*mask[5];
  }
  return R*S-Mat3d::Identity();
#undef FETCH
}
Mat3d calcFGradBF(const Mat3d& RS)
{
  Mat3d R=expWBF(invCross<scalarD>((RS-RS.transpose())/2));
  Mat3d S=(RS+RS.transpose())/2+Mat3d::Identity();
  return R*S-Mat3d::Identity();
}
Mat3d calcFGradDU(const Mat3d& U)
{
  Mat3d diffRS[9],ret=Mat3d::Zero();
  calcFGrad<Mat3d,Mat3d,Mat3d>(Mat3d::Zero(),diffRS,NULL,NULL,NULL);
  for(int r=0; r<9; r++)
    ret+=diffRS[r]*U.data()[r];
  return ret;
}
Mat3d calcFGradDDU(const Mat3d& U)
{
  return calcFGradDUDV(U,U);
}
Mat3d calcFGradDUDV(const Mat3d& U,const Mat3d& V)
{
  Mat3d diffRSDotT[9],ret=Mat3d::Zero();
  calcFGrad<Mat3d,Mat3d,Mat3d>(Mat3d::Zero(),NULL,NULL,&U,diffRSDotT);
  for(int r=0; r<9; r++)
    ret+=diffRSDotT[r]*V.data()[r];
  return ret;
}
template <typename FTYPE>
Mat3d calcFGrad(const FTYPE& F,Mat9d* diffRS)
{
  Mat3d RS;
  for(int i=0; i<9; i++)
    RS.array()(i)=F.array()(i);
  if(diffRS == NULL) {
    return calcFGrad<Mat3d,Mat3d,Mat3d>(RS,NULL,NULL,NULL,NULL);
  } else {
    Eigen::Map<Mat3d> diffRSCols[9]= {
      Eigen::Map<Mat3d>(diffRS->data()+9*0),
      Eigen::Map<Mat3d>(diffRS->data()+9*1),
      Eigen::Map<Mat3d>(diffRS->data()+9*2),

      Eigen::Map<Mat3d>(diffRS->data()+9*3),
      Eigen::Map<Mat3d>(diffRS->data()+9*4),
      Eigen::Map<Mat3d>(diffRS->data()+9*5),

      Eigen::Map<Mat3d>(diffRS->data()+9*6),
      Eigen::Map<Mat3d>(diffRS->data()+9*7),
      Eigen::Map<Mat3d>(diffRS->data()+9*8),
    };
    return calcFGrad<Eigen::Map<Mat3d>,Mat3d,Mat3d>(RS,diffRSCols,NULL,NULL,NULL);
  }
}
void debugCalcFGrad(bool debugDiff,bool debugDDiff,bool debugDiffDotT,bool debugDUDV,int maxIt)
{
#define DELTA 1E-5f
  for(int it=0; it<maxIt; it++) {
    //debug RS derivative
    if(debugDiff) {
      Mat3d RS=Mat3d::Random(),deltaRS=Mat3d::Random();
      Mat3d diffRS[9];
      Mat3d A=calcFGradBF(RS);
      Mat3d B=calcFGrad<Mat3d,Mat3d,Mat3d>(RS,diffRS,NULL,NULL,NULL);
      Mat3d B2=calcFGrad<Mat3d,Mat3d,Mat3d>(RS+deltaRS*DELTA,NULL,NULL,NULL,NULL);
      Mat3d diffBRef=Mat3d::Zero();
      for(int i=0; i<9; i++)
        diffBRef+=diffRS[i]*deltaRS(i%3,i/3);
      INFOV("Euclidean: %f Err: %f",A.norm(),(A-B).norm())
      INFOV("DiffEuclidean: %f Err: %f",diffBRef.norm(),(diffBRef-(B2-B)/DELTA).norm())
    }

    //debug RS double derivative
    if(debugDDiff) {
      Mat3d RS=Mat3d::Random(),DRS=Mat3d::Random();
      Mat3d diffRS[9],diffRS2[9],ddiffRS[9][9];
      calcFGrad<Mat3d,Mat3d,Mat3d>(RS,diffRS,ddiffRS,NULL,NULL);
      calcFGrad<Mat3d,Mat3d,Mat3d>(RS+DRS*DELTA,diffRS2,NULL,NULL,NULL);
      for(int i=0; i<9; i++) {
        Mat3d diffRSDir=Mat3d::Zero();
        for(int j=0; j<9; j++)
          diffRSDir+=ddiffRS[i][j]*DRS(j%3,j/3);
        INFOV("DDiffEuclidean%d: %f Err: %f",i,diffRSDir.norm(),(diffRSDir-(diffRS2[i]-diffRS[i])/DELTA).norm())
      }
    }

    //debug RS derivative dotT
    if(debugDiffDotT) {
      Mat3d RS=Mat3d::Random(),RSDotT=Mat3d::Random();
      Mat3d diffRS[9],diffRS2[9],diffRSDotT[9];
      calcFGrad<Mat3d,Mat3d,Mat3d>(RS,diffRS,NULL,&RSDotT,diffRSDotT);
      calcFGrad<Mat3d,Mat3d,Mat3d>(RS+RSDotT*DELTA,diffRS2,NULL,NULL,NULL);
      for(int i=0; i<9; i++) {
        INFOV("DiffEuclideanDotT%d: %f Err: %f",i,diffRSDotT[i].norm(),(diffRSDotT[i]-(diffRS2[i]-diffRS[i])/DELTA).norm())
      }
    }

    //debug RS double derivative at origin
    if(debugDUDV) {
      //WARNING: delta must be larger than delta used in basicVariable(*) to ensure correct derivative calculation
      ASSERT_MSG(DELTA > DELTA_BASIC_VARIABLES,"Too small delta, incorrect derivative behavior!")
      Mat9d gradRSF,gradRSF1,gradRSF2;
      calcFGrad<Mat3d>(Mat3d::Zero(),&gradRSF);
      Mat3d U=Mat3d::Random();
      Mat3d V=Mat3d::Random();
      Eigen::Map<Cold> mapU(U.data(),9);
      Eigen::Map<Cold> mapV(V.data(),9);
      //debug dexpU
      Mat3d DU=calcFGradDU(U);
      INFOV("DFGradDU: %f Err: %f",(gradRSF*mapU).norm(),(Eigen::Map<Cold>(DU.data(),9)-gradRSF*mapU).norm());
      //debug ddexpU
      calcFGrad<Mat3d>(U*DELTA,&gradRSF1);
      Mat3d DDU=calcFGradDDU(U);
      INFOV("DDFGradDDU: %f Err: %f",DDU.norm(),
            (Eigen::Map<Cold>(DDU.data(),9)-(gradRSF1-gradRSF)*mapU/DELTA).norm());
      //debug ddexpUV
      calcFGrad<Mat3d>(V*DELTA,&gradRSF2);
      Mat3d DUDV=calcFGradDUDV(U,V);
      INFOV("DDFGradDUDV: %f Err: %f %f",DUDV.norm(),
            (Eigen::Map<Cold>(DUDV.data(),9)-(gradRSF1-gradRSF)*mapV/DELTA).norm(),
            (Eigen::Map<Cold>(DUDV.data(),9)-(gradRSF2-gradRSF)*mapU/DELTA).norm());
    }
  }
#undef DELTA
}
//se/SE(3) operations
Mat4d exponentialSE3(const Vec6d& se3)
{
  Mat4d SE3=Mat4d::Identity();
  //rot
  SE3.block<3,3>(0,0)=expWGradV<Mat3d,Mat3d,Mat3d>(se3.segment<3>(0),NULL,NULL,NULL,NULL,NULL,NULL);
  //trans
  scalarD t=se3.segment<3>(0).norm(),s,c;
  sincos(t,&s,&c);
  Mat3d cw=cross<scalarD>(se3.segment<3>(0)),V;
  if(abs(t) < DELTA_BASIC_VARIABLES)
    V=Mat3d::Identity()+0.5f*cw+1/6.0f*cw*cw;
  else V=Mat3d::Identity()+(1-c)/(t*t)*cw+(t-s)/(t*t*t)*cw*cw;
  SE3.block<3,1>(0,3)=V*se3.segment<3>(3);
  return SE3;
}
Vec6d logarithmSE3(const Mat4d& SE3)
{
  Vec6d se3;
  //rot
  se3.segment<3>(0)=invExpW(SE3.block<3,3>(0,0));
  //trans
  scalarD t=se3.segment<3>(0).norm(),s,c;
  sincos(t,&s,&c);
  Mat3d cw=cross<scalarD>(se3.segment<3>(0)),V;
  if(abs(t) < DELTA_BASIC_VARIABLES)
    V=Mat3d::Identity()+0.5f*cw+1/6.0f*cw*cw;
  else V=Mat3d::Identity()+(1-c)/(t*t)*cw+(t-s)/(t*t*t)*cw*cw;
  se3.segment<3>(3)=V.inverse()*SE3.block<3,1>(0,3);
  return se3;
}
void debugExpLogSE3(int maxIt)
{
#define DELTA 1E-5f
  for(int it=0; it<maxIt; it++) {
    Vec6d se3=Vec6d::Random();
    Mat4d SE3=exponentialSE3(se3);
    Vec6d se3Test=logarithmSE3(SE3);
    Mat4d SE3Test=exponentialSE3(se3Test);
    INFOV("Log: %f Err: %f",se3Test.norm(),(se3Test-se3).norm())
    INFOV("Exp: %f Err: %f",SE3Test.norm(),(SE3Test-SE3).norm())
  }
#undef DELTA
  exit(-1);
}
//derivative of SVD decomposition
void adjustF3D(Mat3& F,Mat3& U,Mat3& V,Vec3& S)
{
  Eigen::SelfAdjointEigenSolver<Mat3> eig(F.transpose()*F);
  //F Hat
  S=eig.eigenvalues().cwiseAbs().cwiseSqrt();
  //adjust V
  V=eig.eigenvectors();
  if(V.determinant() < 0.0f)
    V.col(0)*=-1.0f;
  //adjust U
#define F_EPS 1E-6f
  sizeType good[3];
  sizeType nrGood=0;
  for(sizeType v=0; v<3; v++)
    if(S[v] > F_EPS) {
      U.col(v)=F*V.col(v)/S[v];
      good[nrGood++]=v;
    }
  //the tet is a point
  if(nrGood == 0)
    U.setIdentity();
  //set columns of U to be orthogonal
  else for(sizeType v=0; v<3; v++)
      if(S[v] <= F_EPS) {
        if(nrGood == 1) {
          Vec3 d;
          scalar nd;
          while(true) {
            d=U.col(good[0]).cross(Vec3::Random());
            nd=d.norm();
            if(nd > 1E-3f) {
              U.col(v)=d/nd;
              break;
            }
          }
        } else {
          ASSERT(nrGood == 2)
          U.col(v)=U.col(good[0]).cross(U.col(good[1]));
        }
        good[nrGood++]=v;
      }
#undef F_EPS
  //negate negative U columns
  if(U.determinant() < 0.0f) {
    sizeType id;
    S.minCoeff(&id);
    S[id]*=-1.0f;
    U.col(id)*=-1.0f;
  }
  //rebuild F
  Mat3 Sigma=Mat3::Zero();
  Sigma.diagonal()=S;
  F=U*Sigma*V.transpose();
}
void adjustF2D(Mat3& F,Mat3& U,Mat3& V,Vec3& S)
{
  Eigen::SelfAdjointEigenSolver<Mat2> eig((F.transpose()*F).block<2,2>(0,0));
  //F Hat
  S.setZero();
  S.block<2,1>(0,0)=eig.eigenvalues().cwiseAbs().cwiseSqrt();
  //adjust V
  V.setZero();
  V.block<2,2>(0,0)=eig.eigenvectors();
  if(V.block<2,2>(0,0).determinant() < 0.0f)
    V.col(0)*=-1.0f;
  //adjust U
#define F_EPS 1E-6f
  U.setZero();
  sizeType good[2];
  sizeType nrGood=0;
  for(sizeType v=0; v<2; v++)
    if(S[v] > F_EPS) {
      U.col(v)=F*V.col(v)/S[v];
      good[nrGood++]=v;
    }
  //the tet is a point
  if(nrGood == 0)
    U.block<2,2>(0,0).setIdentity();
  //set columns of U to be orthogonal
  else for(sizeType v=0; v<2; v++)
      if(S[v] <= F_EPS) {
        U.col(v)=Vec3(-U(1,good[0]),U(0,good[0]),0.0f);
        good[nrGood++]=v;
      }
#undef F_EPS
  //negate negative U columns
  if(U.block<2,2>(0,0).determinant() < 0.0f) {
    sizeType id;
    S.block<2,1>(0,0).minCoeff(&id);
    S[id]*=-1.0f;
    U.col(id)*=-1.0f;
  }
  //rebuild F
  Mat3 Sigma=Mat3::Zero();
  Sigma.diagonal()=S;
  F=U*Sigma*V.transpose();
}
void derivSVD(Mat3& derivU,Vec3& derivD,Mat3& derivV,const Mat3& LHS,const Mat3& U,const Vec3& D,const Mat3& V)
{
#define SOLVE_SAFE(a,b,c) \
tmp << D(c),D(b),D(b),D(c); \
if(std::abs(tmp(0,0)-tmp(0,1)) < 1E-6f)	\
  tmpInv=(tmp*tmp+reg).inverse()*tmp; \
else  \
  tmpInv=tmp.inverse(); \
a=tmpInv*Vec2(LHS(b,c),-LHS(c,b));

  derivD=Vec3(LHS(0,0),LHS(1,1),LHS(2,2));

  //21,31,32
  Mat2 reg=Mat2::Identity()*1E-6f,tmp,tmpInv;
  Vec2 omg10,omg20,omg21;
  SOLVE_SAFE(omg10,1,0)
  SOLVE_SAFE(omg20,2,0)
  SOLVE_SAFE(omg21,2,1)
#undef SOLVE_SAFE

  Mat3 omgU;
  omgU<<      0,-omg10(0),-omg20(0),
       omg10(0),        0,-omg21(0),
       omg20(0), omg21(0),        0;
  derivU=U*omgU;

  Mat3 omgV;
  omgV<<      0,-omg10(1),-omg20(1),
       omg10(1),        0,-omg21(1),
       omg20(1), omg21(1),        0;
  derivV=-V*omgV;
}
void derivSVDDir(Mat3& derivU,Vec3& derivD,Mat3& derivV,const Mat3& dir,const Mat3& U,const Vec3& D,const Mat3& V)
{
  Mat3 LHS=Mat3::Zero();
  for(sizeType r=0; r<3; r++)
    for(sizeType c=0; c<3; c++)
      LHS+=U.row(r).transpose()*V.row(c)*dir(r,c);
  derivSVD(derivU,derivD,derivV,LHS,U,D,V);
}
void derivRDF(Mat9d& DRDF,const Mat3& F,const Mat3& U,const Vec3& D,const Mat3& V)
{
  Vec3 derivD;
  Mat3 derivU,derivV;
  if(D.minCoeff() < 1E-3f)
    DRDF.setZero();
  for(sizeType r=0; r<3; r++)
    for(sizeType c=0; c<3; c++) {
      derivSVD(derivU,derivD,derivV,U.row(r).transpose()*V.row(c),U,D,V);
      Eigen::Map<Mat3d>(DRDF.col(r+c*3).data())=(derivU*V.transpose()+U*derivV.transpose()).cast<scalarD>();
    }
}
void debugDerivSVD(bool debugSVD,bool debugRDF,int maxIt)
{
#define DELTA 1E-7f
  for(int it=0; it<maxIt; it++) {
    //debug derivative of SVD decomposition
    if(debugSVD) {
      Vec3 dD;
      Mat3 F=Mat3::Random(),dF=Mat3::Random(),dU,dV;
      Eigen::JacobiSVD<Mat3> svd(F,Eigen::ComputeFullU|Eigen::ComputeFullV);
      derivSVDDir(dU,dD,dV,dF,svd.matrixU(),svd.singularValues(),svd.matrixV());
      Eigen::JacobiSVD<Mat3> svd2(F+dF*DELTA,Eigen::ComputeFullU|Eigen::ComputeFullV);
      INFOV("DerivSVD: %f %f %f Err: %f %f %f",dU.norm(),dD.norm(),dV.norm(),
            (dU-(svd2.matrixU()-svd.matrixU())/DELTA).norm(),
            (dD-(svd2.singularValues()-svd.singularValues())/DELTA).norm(),
            (dV-(svd2.matrixV()-svd.matrixV())/DELTA).norm())
    }

    //debug derivative of RS (polar) decomposition
    if(debugRDF) {
      Mat9d DRDF;
      Mat3 F=Mat3::Random(),dF=Mat3::Random();
      Eigen::JacobiSVD<Mat3> svd(F,Eigen::ComputeFullU|Eigen::ComputeFullV);
      derivRDF(DRDF,F,svd.matrixU(),svd.singularValues(),svd.matrixV());
      Eigen::JacobiSVD<Mat3> svd2(F+dF*DELTA,Eigen::ComputeFullU|Eigen::ComputeFullV);
      Mat3 R=svd.matrixU()*svd.matrixV().transpose();
      Mat3 R2=svd2.matrixU()*svd2.matrixV().transpose(),DRDFN=(R2-R)/DELTA;
      ScalarUtil<scalar>::ScalarCol DRDFA=DRDF.cast<scalar>()*Eigen::Map<ScalarUtil<scalar>::ScalarCol>(dF.data(),9);
      INFOV("DerivRS: %f Err: %f",DRDFA.norm(),(DRDFA-Eigen::Map<ScalarUtil<scalar>::ScalarCol>(DRDFN.data(),9)).norm())
    }
  }
#undef DELTA
}

//force instantiation
template Mat3d calcFGrad<Mat3d,Mat3d,Mat3d>(const Mat3d& RS,Mat3d diffRS[9],Mat3d ddiffRS[9][9],const Mat3d* RSDotT,Mat3d diffRSDotT[9]);
template Mat3d calcFGrad<Eigen::Map<Mat3d> >(const Eigen::Map<Mat3d>& F,Mat9d* diffRS);
template Mat3d calcFGrad<Mat3d>(const Mat3d& F,Mat9d* diffRS);
template Mat3d calcFGrad<Cold>(const Cold& F,Mat9d* diffRS);

PRJ_END
