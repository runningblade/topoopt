#include "FEMModel.h"
#include "FEMRotationUtil.h"
#include "Utils.h"

PRJ_BEGIN

//FEMModel
template <int DIM>
FEMModel<DIM>::FEMModel():ElasticModel<DIM>(typeid(FEMModel<DIM>).name()) {}
template <int DIM>
FEMModel<DIM>::FEMModel(const string& name):ElasticModel<DIM>(name) {}
template <int DIM>
FEMModel<DIM>::FEMModel(const string& name,sizeType id):ElasticModel<DIM>(name,id) {}
template <int DIM>
FEMModel<DIM>::FEMModel(const Vec& x,scalarD poisson,scalarD young,scalarD coefM,sizeType x0,sizeType x1,sizeType x2,sizeType x3,sizeType id):ElasticModel<DIM>(typeid(FEMModel<DIM>).name(),id)
{
  _id << x0,x1,x2,x3;
  _id*=DIM;
  _coefM=coefM;
  _l[0]=(x.template segment<DIM>(_id[1])-x.template segment<DIM>(_id[0])).norm();
  _l[1]=(x.template segment<DIM>(_id[2])-x.template segment<DIM>(_id[0])).norm();
  for(sizeType i=0; i<NRP; i++)
    _X0.template segment<DIM>(i*DIM)=x.template segment<DIM>(_id[i]);
  calcH(poisson,young);
}
template <int DIM>
FEMModel<DIM>::FEMModel(const Vec& x,scalarD poisson,scalarD young,scalarD coefM,sizeType x0,sizeType x1,sizeType x2,sizeType x3,sizeType x4,sizeType x5,sizeType x6,sizeType x7,sizeType id):ElasticModel<DIM>(typeid(FEMModel<DIM>).name(),id)
{
  _id << x0,x1,x2,x3,x4,x5,x6,x7;
  _id*=DIM;
  _coefM=coefM;
  _l[0]=(x.template segment<DIM>(_id[1])-x.template segment<DIM>(_id[0])).norm();
  _l[1]=(x.template segment<DIM>(_id[2])-x.template segment<DIM>(_id[0])).norm();
  _l[2]=(x.template segment<DIM>(_id[4])-x.template segment<DIM>(_id[0])).norm();
  for(sizeType i=0; i<NRP; i++)
    _X0.template segment<DIM>(i*DIM)=x.template segment<DIM>(_id[i]);
  calcH(poisson,young);
}
template <int DIM>
void FEMModel<DIM>::writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const
{
  ID iss[2]= {_id/DIM,_id/DIM};
  scalarD coefMK[2],coefM[2];
  coefMK[0]=coef;
  coefM[0]=_coefM*coef;
  os.appendCells(iss,iss+1,DIM == 2 ? VTKWriter<scalarD>::PIXEL : VTKWriter<scalarD>::VOXEL);
  os.appendCustomData("coef",coefMK,coefMK+1);
  os.appendCustomData("coefM",coefM,coefM+1);
  if(pData) {
    sizeType pid=ElasticModel<DIM>::_pid;
    os.appendCustomData(pDataName,pData->data()+pid,pData->data()+pid+1);
  }
}
template <int DIM>
bool FEMModel<DIM>::read(istream& is,IOData* dat)
{
  Matd H;
  Coli id;
  Vec X0;

  ElasticModel<DIM>::read(is,dat);
  readBinaryData(H,is);
  readBinaryData(_coefM,is);
  readBinaryData(_l,is);
  readBinaryData(id,is);
  readBinaryData(X0,is);

  _H=H;
  _id=id;
  _X0=X0;
  return is.good();
}
template <int DIM>
bool FEMModel<DIM>::write(ostream& os,IOData* dat) const
{
  ElasticModel<DIM>::write(os,dat);
  writeBinaryData(Matd(_H),os);
  writeBinaryData(_coefM,os);
  writeBinaryData(_l,os);
  writeBinaryData(Coli(_id),os);
  writeBinaryData(Vec(_X0),os);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> FEMModel<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new FEMModel);
}
template <int DIM>
int FEMModel<DIM>::inputsAug() const
{
  return DIM == 2 ? 1 : 3;
}
template <>
void FEMModel<2>::initAug(const Vec& x,Vec& aug) const
{
  Vec3d w=invExpW(calcR(x));
  aug[ElasticModel<2>::_pid]=w[2];
}
template <>
void FEMModel<3>::initAug(const Vec& x,Vec& aug) const
{
  Vec3d w=invExpW(calcR(x));
  aug.segment<3>(ElasticModel<3>::_pid*3)=w;
}
template <int DIM>
Coli FEMModel<DIM>::ids() const
{
  return _id/DIM;
}
template <>
scalarD FEMModel<2>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  //E=(R*X-_X0)^T*_H*(R*X-_X0)/2
  sizeType wid=offAug+ElasticModel<2>::_pid;
  Mat3d DRDW[3],DRDWW[3][3];
  Mat2d R=expWGradV<Mat3d,Mat3d,Mat3d>(Vec3d(0,0,x[wid]),(fgrad || fhess) ? DRDW : NULL,fhess ? DRDWW : NULL,NULL,NULL).block<2,2>(0,0);
  Mat2d DRDZ=DRDW[2].block<2,2>(0,0);
  Mat2d DRDZZ=DRDWW[2][2].block<2,2>(0,0);
  //Stage1
  VecDIMd blk;
  MatDIMd RTHR;
  VecDIMAlld DX,DWX,DWWX,HX,HWX;
  for(sizeType i=0; i<NRP; i++) {
    DX.template segment<2>(i*2)=R*x.template segment<2>(_id[i]);
    if(fgrad || fhess)
      DWX.template segment<2>(i*2)=DRDZ*x.template segment<2>(_id[i]);
    if(fhess) {
      DWWX.template segment<2>(i*2)=DRDZZ*x.template segment<2>(_id[i]);
      for(sizeType j=0; j<NRP; j++) {
        RTHR=R.transpose()*(_H.template block<2,2>(i*2,j*2)*R)*coef;
        addBlock(*fhess,_id[i],_id[j],RTHR);
      }
    }
  }
  //Stage2
  HX=_H*(DX-_X0)*coef;
  if(fgrad) {
    for(sizeType i=0; i<NRP; i++)
      fgrad->getMatrixI().template segment<2>(_id[i])+=R.transpose()*HX.template segment<2>(i*2);
    fgrad->getMatrixI()[wid]+=DWX.dot(HX);
  }
  //Stage3
  if(fhess) {
    HWX=_H*DWX*coef;
    scalarD DWW=DWX.dot(HWX);
    DWW+=HX.dot(DWWX);
    fhess->push_back(STrip(wid,wid,DWW));
    for(sizeType i=0; i<NRP; i++) {
      blk=R.transpose()*HWX.template segment<2>(i*2);
      blk+=DRDZ.transpose()*HX.template segment<2>(i*2);
      addBlock(*fhess,wid,_id[i],blk.transpose());
      addBlock(*fhess,_id[i],wid,blk);
    }
  }
  return (DX-_X0).dot(HX)/2;
}
template <>
scalarD FEMModel<3>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  //E=(R*X-_X0)^T*_H*(R*X-_X0)/2
  sizeType wid=offAug+ElasticModel<3>::_pid*3;
  Mat3d DRDZ[3],DRDZZ[3][3],R=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment<3>(wid),(fgrad || fhess) ? DRDZ : NULL,fhess ? DRDZZ : NULL,NULL,NULL);
  //Stage1
  VecDIMd blk;
  MatDIMd RTHR;
  VecDIMAlld DX,DWX[3],DWWX[3][3],HX,HWX[3];
  for(sizeType i=0; i<NRP; i++) {
    DX.template segment<3>(i*3)=R*x.template segment<3>(_id[i]);
    if(fgrad || fhess)
      for(sizeType w1=0; w1<3; w1++)
        DWX[w1].template segment<3>(i*3)=DRDZ[w1]*x.template segment<3>(_id[i]);
    if(fhess) {
      for(sizeType w1=0; w1<3; w1++)
        for(sizeType w2=0; w2<3; w2++)
          DWWX[w1][w2].template segment<3>(i*3)=DRDZZ[w1][w2]*x.template segment<3>(_id[i]);
      for(sizeType j=0; j<NRP; j++) {
        RTHR=R.transpose()*(_H.template block<3,3>(i*3,j*3)*R)*coef;
        addBlock(*fhess,_id[i],_id[j],RTHR);
      }
    }
  }
  //Stage2
  HX=_H*(DX-_X0)*coef;
  if(fgrad) {
    for(sizeType i=0; i<NRP; i++)
      fgrad->getMatrixI().template segment<3>(_id[i])+=R.transpose()*HX.template segment<3>(i*3);
    for(sizeType w1=0; w1<3; w1++)
      fgrad->getMatrixI()[wid+w1]+=DWX[w1].dot(HX);
  }
  //Stage3
  if(fhess) {
    scalarD DWW;
    for(sizeType w1=0; w1<3; w1++) {
      HWX[w1]=_H*DWX[w1]*coef;
      for(sizeType w2=0; w2<3; w2++) {
        DWW=DWX[w2].dot(HWX[w1]);
        DWW+=HX.dot(DWWX[w1][w2]);
        fhess->push_back(STrip(wid+w1,wid+w2,DWW));
      }
    }
    for(sizeType w1=0; w1<3; w1++)
      for(sizeType i=0; i<NRP; i++) {
        blk=R.transpose()*HWX[w1].template segment<3>(i*3);
        blk+=DRDZ[w1].transpose()*HX.template segment<3>(i*3);
        addBlock(*fhess,wid+w1,_id[i],blk.transpose());
        addBlock(*fhess,_id[i],wid+w1,blk);
      }
  }
  return (DX-_X0).dot(HX)/2;
}
template <>
scalarD FEMModel<2>::operatorMKDot(const Vec& xP,const Vec& b,const Vec& other,sizeType offAug) const
{
  //E=(R*X-_X0)^T*_H*(R*X-_X0)/2
  sizeType wid=offAug+ElasticModel<2>::_pid;
  Mat3d DRDW[3];
  Mat2d R=expWGradV<Mat3d,Mat3d,Mat3d>(Vec3d(0,0,xP[wid]),DRDW,NULL,NULL,NULL).block<2,2>(0,0);
  Mat2d DRDZ=DRDW[2].block<2,2>(0,0);
  //Stage1
  VecDIMAlld DX,DWX,HX,O,B;
  for(sizeType i=0; i<NRP; i++) {
    DX.template segment<2>(i*2)=R*xP.template segment<2>(_id[i]);
    DWX.template segment<2>(i*2)=DRDZ*xP.template segment<2>(_id[i]);
  }
  //Stage2
  scalarD ret=0;
  HX=_H*(DX-_X0);
  for(sizeType i=0; i<NRP; i++) {
    B.template segment<2>(i*2)=b.segment<2>(_id[i]);
    O.template segment<2>(i*2)=other.segment<2>(_id[i]);
    ret+=other.segment<2>(_id[i]).dot(R.transpose()*HX.template segment<2>(i*2));
  }
  ret+=other[wid]*DWX.dot(HX);
  return ret+O.dot(_MDIM*B*(_coefM*_l.prod()));
}
template <>
scalarD FEMModel<3>::operatorMKDot(const Vec& xP,const Vec& b,const Vec& other,sizeType offAug) const
{
  //E=(R*X-_X0)^T*_H*(R*X-_X0)/2
  sizeType wid=offAug+ElasticModel<3>::_pid*3;
  Mat3d DRDZ[3],R=expWGradV<Mat3d,Mat3d,Mat3d>(xP.segment<3>(wid),DRDZ,NULL,NULL,NULL);
  DRDZ[0]=DRDZ[0]*other[wid+0]+DRDZ[1]*other[wid+1]+DRDZ[2]*other[wid+2];
  //Stage1
  VecDIMAlld DX,DWX,HX,O,B;
  for(sizeType i=0; i<NRP; i++) {
    DX.template segment<3>(i*3)=R*xP.template segment<3>(_id[i]);
    DWX.template segment<3>(i*3)=DRDZ[0]*xP.template segment<3>(_id[i]);
  }
  //Stage2
  HX=_H*(DX-_X0);
  scalarD ret=DWX.dot(HX);
  for(sizeType i=0; i<NRP; i++) {
    B.template segment<3>(i*3)=b.segment<3>(_id[i]);
    O.template segment<3>(i*3)=other.segment<3>(_id[i]);
    ret+=other.segment<3>(_id[i]).dot(R.transpose()*HX.template segment<3>(i*3));
  }
  return ret+O.dot(_MDIM*B*(_coefM*_l.prod()));
}
template <int DIM>
void FEMModel<DIM>::operatorM(STrips& fhess,scalarD coef) const
{
  coef*=_coefM*_l.prod();
  for(sizeType i=0; i<NRP; i++)
    for(sizeType j=0; j<NRP; j++)
      addIK(fhess,_id[i],_id[j],_M(i,j)*coef,DIM);
}
template <int DIM>
scalarD FEMModel<DIM>::kineticBruteForce(const Vec& x) const
{
#define SEG 100
  scalarD ret=0,alpha;
  VecDIMd cacheI[4],cacheJ[2];
  for(sizeType i=0; i<SEG; i++) {
    alpha=((scalarD)i+0.5f)/(scalarD)SEG;
    for(sizeType d=0; d<(1<<(DIM-1)); d++)
      cacheI[d]=interp1D<VecDIMd,scalarD>(x.segment<DIM>(_id[d*2]),x.segment<DIM>(_id[d*2+1]),alpha);

    for(sizeType j=0; j<SEG; j++) {
      alpha=((scalarD)j+0.5f)/(scalarD)SEG;
      for(sizeType d=0; d<(1<<(DIM-2)); d++)
        cacheJ[d]=interp1D<VecDIMd,scalarD>(cacheI[d*2],cacheI[d*2+1],alpha);
      if(DIM == 2)
        ret+=cacheJ[0].squaredNorm();
      else
        for(sizeType k=0; k<SEG; k++) {
          alpha=((scalarD)k+0.5f)/(scalarD)SEG;
          VecDIMd vel=interp1D<VecDIMd,scalarD>(cacheJ[0],cacheJ[1],alpha);
          ret+=vel.squaredNorm();
        }
    }
  }
  return ret*(_coefM*_l.prod()/pow((scalarD)SEG,DIM))/2;
#undef SEG
}
template <int DIM>
int FEMModel<DIM>::inputs() const
{
  return pow(2,DIM)*DIM;
}
//potential
template <>
void FEMModel<2>::calcH(scalarD poisson,scalarD young)
{
  //input
  scalarD lambda=poisson*young/((1+poisson)*(1-2*poisson));
  scalarD mu=young/(2*(1+poisson));
  scalarD lx=_l[0];
  scalarD ly=_l[1];

  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;
  scalarD tt9;
  scalarD tt10;
  scalarD tt11;
  scalarD tt12;
  scalarD tt13;
  scalarD tt14;
  scalarD tt15;
  scalarD tt16;
  scalarD tt17;
  scalarD tt18;
  scalarD tt19;
  scalarD tt20;
  scalarD tt21;

  tt1=1/lx;
  tt2=1/ly;
  tt3=pow(lx,2);
  tt4=2*tt3;
  tt5=pow(ly,2);
  tt6=2*tt5;
  tt7=(tt6+tt4)*mu;
  tt8=tt5*lambda;
  tt9=tt8+tt7;
  tt10=tt1*tt2*tt9/3.0;
  tt11=lambda/4.0;
  tt12=-tt1*tt2*(tt8+(tt6-tt3)*mu)/3.0;
  tt13=tt1*tt2*(tt8+(tt6-4*tt3)*mu)/6.0;
  tt14=-lambda/4.0;
  tt15=-tt1*tt2*tt9/6.0;
  tt16=tt3*lambda;
  tt17=tt16+tt7;
  tt18=tt1*tt2*tt17/3.0;
  tt19=tt1*tt2*(tt16+(tt4-4*tt5)*mu)/6.0;
  tt20=-tt1*tt2*(tt16+(tt4-tt5)*mu)/3.0;
  tt21=-tt1*tt2*tt17/6.0;
  _H(0,0)=tt10;
  _H(0,1)=tt11;
  _H(0,2)=tt12;
  _H(0,3)=tt11;
  _H(0,4)=tt13;
  _H(0,5)=tt14;
  _H(0,6)=tt15;
  _H(0,7)=tt14;
  _H(1,0)=tt11;
  _H(1,1)=tt18;
  _H(1,2)=tt14;
  _H(1,3)=tt19;
  _H(1,4)=tt11;
  _H(1,5)=tt20;
  _H(1,6)=tt14;
  _H(1,7)=tt21;
  _H(2,0)=tt12;
  _H(2,1)=tt14;
  _H(2,2)=tt10;
  _H(2,3)=tt14;
  _H(2,4)=tt15;
  _H(2,5)=tt11;
  _H(2,6)=tt13;
  _H(2,7)=tt11;
  _H(3,0)=tt11;
  _H(3,1)=tt19;
  _H(3,2)=tt14;
  _H(3,3)=tt18;
  _H(3,4)=tt11;
  _H(3,5)=tt21;
  _H(3,6)=tt14;
  _H(3,7)=tt20;
  _H(4,0)=tt13;
  _H(4,1)=tt11;
  _H(4,2)=tt15;
  _H(4,3)=tt11;
  _H(4,4)=tt10;
  _H(4,5)=tt14;
  _H(4,6)=tt12;
  _H(4,7)=tt14;
  _H(5,0)=tt14;
  _H(5,1)=tt20;
  _H(5,2)=tt11;
  _H(5,3)=tt21;
  _H(5,4)=tt14;
  _H(5,5)=tt18;
  _H(5,6)=tt11;
  _H(5,7)=tt19;
  _H(6,0)=tt15;
  _H(6,1)=tt14;
  _H(6,2)=tt13;
  _H(6,3)=tt14;
  _H(6,4)=tt12;
  _H(6,5)=tt11;
  _H(6,6)=tt10;
  _H(6,7)=tt11;
  _H(7,0)=tt14;
  _H(7,1)=tt21;
  _H(7,2)=tt11;
  _H(7,3)=tt20;
  _H(7,4)=tt14;
  _H(7,5)=tt19;
  _H(7,6)=tt11;
  _H(7,7)=tt18;
}
template <>
void FEMModel<3>::calcH(scalarD poisson,scalarD young)
{
  //input
  scalarD lambda=poisson*young/((1+poisson)*(1-2*poisson));
  scalarD mu=young/(2*(1+poisson));
  scalarD lx=_l[0];
  scalarD ly=_l[1];
  scalarD lz=_l[2];

  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;
  scalarD tt9;
  scalarD tt10;
  scalarD tt11;
  scalarD tt12;
  scalarD tt13;
  scalarD tt14;
  scalarD tt15;
  scalarD tt16;
  scalarD tt17;
  scalarD tt18;
  scalarD tt19;
  scalarD tt20;
  scalarD tt21;
  scalarD tt22;
  scalarD tt23;
  scalarD tt24;
  scalarD tt25;
  scalarD tt26;
  scalarD tt27;
  scalarD tt28;
  scalarD tt29;
  scalarD tt30;
  scalarD tt31;
  scalarD tt32;
  scalarD tt33;
  scalarD tt34;
  scalarD tt35;
  scalarD tt36;
  scalarD tt37;
  scalarD tt38;
  scalarD tt39;
  scalarD tt40;
  scalarD tt41;
  scalarD tt42;
  scalarD tt43;
  scalarD tt44;
  scalarD tt45;
  scalarD tt46;
  scalarD tt47;
  scalarD tt48;
  scalarD tt49;
  scalarD tt50;
  scalarD tt51;
  scalarD tt52;
  scalarD tt53;
  scalarD tt54;
  scalarD tt55;
  scalarD tt56;
  scalarD tt57;
  scalarD tt58;
  scalarD tt59;
  scalarD tt60;
  scalarD tt61;
  scalarD tt62;
  scalarD tt63;
  scalarD tt64;
  scalarD tt65;
  scalarD tt66;
  scalarD tt67;
  scalarD tt68;
  scalarD tt69;

  tt1=1/lx;
  tt2=1/ly;
  tt3=1/lz;
  tt4=pow(lx,2);
  tt5=pow(ly,2);
  tt6=2*tt4*tt5;
  tt7=2*tt4;
  tt8=2*tt5;
  tt9=pow(lz,2);
  tt10=(tt8+tt7)*tt9;
  tt11=(tt10+tt6)*mu;
  tt12=tt5*tt9*lambda;
  tt13=tt12+tt11;
  tt14=tt1*tt2*tt3*tt13/9.0;
  tt15=lz*lambda/12.0;
  tt16=ly*lambda/12.0;
  tt17=-tt4*tt5;
  tt18=-tt4;
  tt19=(tt8+tt18)*tt9;
  tt20=-tt1*tt2*tt3*(tt12+(tt19+tt17)*mu)/9.0;
  tt21=-4*tt4;
  tt22=(tt8+tt21)*tt9;
  tt23=(tt22+tt6)*mu;
  tt24=tt1*tt2*tt3*(tt12+tt23)/18.0;
  tt25=-lz*lambda/12.0;
  tt26=ly*lambda/24.0;
  tt27=(tt10+tt17)*mu;
  tt28=-tt1*tt2*tt3*(tt12+tt27)/18.0;
  tt29=-4*tt4*tt5;
  tt30=(tt10+tt29)*mu;
  tt31=tt1*tt2*tt3*(tt12+tt30)/18.0;
  tt32=lz*lambda/24.0;
  tt33=-ly*lambda/12.0;
  tt34=(tt19+tt6)*mu;
  tt35=-tt1*tt2*tt3*(tt12+tt34)/18.0;
  tt36=tt1*tt2*tt3*(tt12+(tt22+tt29)*mu)/36.0;
  tt37=-lz*lambda/24.0;
  tt38=-ly*lambda/24.0;
  tt39=-tt1*tt2*tt3*tt13/36.0;
  tt40=tt4*tt9*lambda;
  tt41=tt40+tt11;
  tt42=tt1*tt2*tt3*tt41/9.0;
  tt43=lx*lambda/12.0;
  tt44=-4*tt5;
  tt45=(tt44+tt7)*tt9;
  tt46=(tt45+tt6)*mu;
  tt47=tt1*tt2*tt3*(tt40+tt46)/18.0;
  tt48=lx*lambda/24.0;
  tt49=-tt5;
  tt50=(tt49+tt7)*tt9;
  tt51=-tt1*tt2*tt3*(tt40+(tt50+tt17)*mu)/9.0;
  tt52=-tt1*tt2*tt3*(tt40+tt27)/18.0;
  tt53=tt1*tt2*tt3*(tt40+tt30)/18.0;
  tt54=-lx*lambda/12.0;
  tt55=tt1*tt2*tt3*(tt40+(tt45+tt29)*mu)/36.0;
  tt56=-lx*lambda/24.0;
  tt57=(tt50+tt6)*mu;
  tt58=-tt1*tt2*tt3*(tt40+tt57)/18.0;
  tt59=-tt1*tt2*tt3*tt41/36.0;
  tt60=tt4*tt5*lambda;
  tt61=tt60+tt11;
  tt62=tt1*tt2*tt3*tt61/9.0;
  tt63=tt1*tt2*tt3*(tt60+tt46)/18.0;
  tt64=tt1*tt2*tt3*(tt60+tt23)/18.0;
  tt65=tt1*tt2*tt3*(tt60+((tt44+tt21)*tt9+tt6)*mu)/36.0;
  tt66=-tt1*tt2*tt3*(tt60+((tt49+tt18)*tt9+tt6)*mu)/9.0;
  tt67=-tt1*tt2*tt3*(tt60+tt34)/18.0;
  tt68=-tt1*tt2*tt3*(tt60+tt57)/18.0;
  tt69=-tt1*tt2*tt3*tt61/36.0;
  _H(0,0)=tt14;
  _H(0,1)=tt15;
  _H(0,2)=tt16;
  _H(0,3)=tt20;
  _H(0,4)=tt15;
  _H(0,5)=tt16;
  _H(0,6)=tt24;
  _H(0,7)=tt25;
  _H(0,8)=tt26;
  _H(0,9)=tt28;
  _H(0,10)=tt25;
  _H(0,11)=tt26;
  _H(0,12)=tt31;
  _H(0,13)=tt32;
  _H(0,14)=tt33;
  _H(0,15)=tt35;
  _H(0,16)=tt32;
  _H(0,17)=tt33;
  _H(0,18)=tt36;
  _H(0,19)=tt37;
  _H(0,20)=tt38;
  _H(0,21)=tt39;
  _H(0,22)=tt37;
  _H(0,23)=tt38;
  _H(1,0)=tt15;
  _H(1,1)=tt42;
  _H(1,2)=tt43;
  _H(1,3)=tt25;
  _H(1,4)=tt47;
  _H(1,5)=tt48;
  _H(1,6)=tt15;
  _H(1,7)=tt51;
  _H(1,8)=tt43;
  _H(1,9)=tt25;
  _H(1,10)=tt52;
  _H(1,11)=tt48;
  _H(1,12)=tt32;
  _H(1,13)=tt53;
  _H(1,14)=tt54;
  _H(1,15)=tt37;
  _H(1,16)=tt55;
  _H(1,17)=tt56;
  _H(1,18)=tt32;
  _H(1,19)=tt58;
  _H(1,20)=tt54;
  _H(1,21)=tt37;
  _H(1,22)=tt59;
  _H(1,23)=tt56;
  _H(2,0)=tt16;
  _H(2,1)=tt43;
  _H(2,2)=tt62;
  _H(2,3)=tt33;
  _H(2,4)=tt48;
  _H(2,5)=tt63;
  _H(2,6)=tt26;
  _H(2,7)=tt54;
  _H(2,8)=tt64;
  _H(2,9)=tt38;
  _H(2,10)=tt56;
  _H(2,11)=tt65;
  _H(2,12)=tt16;
  _H(2,13)=tt43;
  _H(2,14)=tt66;
  _H(2,15)=tt33;
  _H(2,16)=tt48;
  _H(2,17)=tt67;
  _H(2,18)=tt26;
  _H(2,19)=tt54;
  _H(2,20)=tt68;
  _H(2,21)=tt38;
  _H(2,22)=tt56;
  _H(2,23)=tt69;
  _H(3,0)=tt20;
  _H(3,1)=tt25;
  _H(3,2)=tt33;
  _H(3,3)=tt14;
  _H(3,4)=tt25;
  _H(3,5)=tt33;
  _H(3,6)=tt28;
  _H(3,7)=tt15;
  _H(3,8)=tt38;
  _H(3,9)=tt24;
  _H(3,10)=tt15;
  _H(3,11)=tt38;
  _H(3,12)=tt35;
  _H(3,13)=tt37;
  _H(3,14)=tt16;
  _H(3,15)=tt31;
  _H(3,16)=tt37;
  _H(3,17)=tt16;
  _H(3,18)=tt39;
  _H(3,19)=tt32;
  _H(3,20)=tt26;
  _H(3,21)=tt36;
  _H(3,22)=tt32;
  _H(3,23)=tt26;
  _H(4,0)=tt15;
  _H(4,1)=tt47;
  _H(4,2)=tt48;
  _H(4,3)=tt25;
  _H(4,4)=tt42;
  _H(4,5)=tt43;
  _H(4,6)=tt15;
  _H(4,7)=tt52;
  _H(4,8)=tt48;
  _H(4,9)=tt25;
  _H(4,10)=tt51;
  _H(4,11)=tt43;
  _H(4,12)=tt32;
  _H(4,13)=tt55;
  _H(4,14)=tt56;
  _H(4,15)=tt37;
  _H(4,16)=tt53;
  _H(4,17)=tt54;
  _H(4,18)=tt32;
  _H(4,19)=tt59;
  _H(4,20)=tt56;
  _H(4,21)=tt37;
  _H(4,22)=tt58;
  _H(4,23)=tt54;
  _H(5,0)=tt16;
  _H(5,1)=tt48;
  _H(5,2)=tt63;
  _H(5,3)=tt33;
  _H(5,4)=tt43;
  _H(5,5)=tt62;
  _H(5,6)=tt26;
  _H(5,7)=tt56;
  _H(5,8)=tt65;
  _H(5,9)=tt38;
  _H(5,10)=tt54;
  _H(5,11)=tt64;
  _H(5,12)=tt16;
  _H(5,13)=tt48;
  _H(5,14)=tt67;
  _H(5,15)=tt33;
  _H(5,16)=tt43;
  _H(5,17)=tt66;
  _H(5,18)=tt26;
  _H(5,19)=tt56;
  _H(5,20)=tt69;
  _H(5,21)=tt38;
  _H(5,22)=tt54;
  _H(5,23)=tt68;
  _H(6,0)=tt24;
  _H(6,1)=tt15;
  _H(6,2)=tt26;
  _H(6,3)=tt28;
  _H(6,4)=tt15;
  _H(6,5)=tt26;
  _H(6,6)=tt14;
  _H(6,7)=tt25;
  _H(6,8)=tt16;
  _H(6,9)=tt20;
  _H(6,10)=tt25;
  _H(6,11)=tt16;
  _H(6,12)=tt36;
  _H(6,13)=tt32;
  _H(6,14)=tt38;
  _H(6,15)=tt39;
  _H(6,16)=tt32;
  _H(6,17)=tt38;
  _H(6,18)=tt31;
  _H(6,19)=tt37;
  _H(6,20)=tt33;
  _H(6,21)=tt35;
  _H(6,22)=tt37;
  _H(6,23)=tt33;
  _H(7,0)=tt25;
  _H(7,1)=tt51;
  _H(7,2)=tt54;
  _H(7,3)=tt15;
  _H(7,4)=tt52;
  _H(7,5)=tt56;
  _H(7,6)=tt25;
  _H(7,7)=tt42;
  _H(7,8)=tt54;
  _H(7,9)=tt15;
  _H(7,10)=tt47;
  _H(7,11)=tt56;
  _H(7,12)=tt37;
  _H(7,13)=tt58;
  _H(7,14)=tt43;
  _H(7,15)=tt32;
  _H(7,16)=tt59;
  _H(7,17)=tt48;
  _H(7,18)=tt37;
  _H(7,19)=tt53;
  _H(7,20)=tt43;
  _H(7,21)=tt32;
  _H(7,22)=tt55;
  _H(7,23)=tt48;
  _H(8,0)=tt26;
  _H(8,1)=tt43;
  _H(8,2)=tt64;
  _H(8,3)=tt38;
  _H(8,4)=tt48;
  _H(8,5)=tt65;
  _H(8,6)=tt16;
  _H(8,7)=tt54;
  _H(8,8)=tt62;
  _H(8,9)=tt33;
  _H(8,10)=tt56;
  _H(8,11)=tt63;
  _H(8,12)=tt26;
  _H(8,13)=tt43;
  _H(8,14)=tt68;
  _H(8,15)=tt38;
  _H(8,16)=tt48;
  _H(8,17)=tt69;
  _H(8,18)=tt16;
  _H(8,19)=tt54;
  _H(8,20)=tt66;
  _H(8,21)=tt33;
  _H(8,22)=tt56;
  _H(8,23)=tt67;
  _H(9,0)=tt28;
  _H(9,1)=tt25;
  _H(9,2)=tt38;
  _H(9,3)=tt24;
  _H(9,4)=tt25;
  _H(9,5)=tt38;
  _H(9,6)=tt20;
  _H(9,7)=tt15;
  _H(9,8)=tt33;
  _H(9,9)=tt14;
  _H(9,10)=tt15;
  _H(9,11)=tt33;
  _H(9,12)=tt39;
  _H(9,13)=tt37;
  _H(9,14)=tt26;
  _H(9,15)=tt36;
  _H(9,16)=tt37;
  _H(9,17)=tt26;
  _H(9,18)=tt35;
  _H(9,19)=tt32;
  _H(9,20)=tt16;
  _H(9,21)=tt31;
  _H(9,22)=tt32;
  _H(9,23)=tt16;
  _H(10,0)=tt25;
  _H(10,1)=tt52;
  _H(10,2)=tt56;
  _H(10,3)=tt15;
  _H(10,4)=tt51;
  _H(10,5)=tt54;
  _H(10,6)=tt25;
  _H(10,7)=tt47;
  _H(10,8)=tt56;
  _H(10,9)=tt15;
  _H(10,10)=tt42;
  _H(10,11)=tt54;
  _H(10,12)=tt37;
  _H(10,13)=tt59;
  _H(10,14)=tt48;
  _H(10,15)=tt32;
  _H(10,16)=tt58;
  _H(10,17)=tt43;
  _H(10,18)=tt37;
  _H(10,19)=tt55;
  _H(10,20)=tt48;
  _H(10,21)=tt32;
  _H(10,22)=tt53;
  _H(10,23)=tt43;
  _H(11,0)=tt26;
  _H(11,1)=tt48;
  _H(11,2)=tt65;
  _H(11,3)=tt38;
  _H(11,4)=tt43;
  _H(11,5)=tt64;
  _H(11,6)=tt16;
  _H(11,7)=tt56;
  _H(11,8)=tt63;
  _H(11,9)=tt33;
  _H(11,10)=tt54;
  _H(11,11)=tt62;
  _H(11,12)=tt26;
  _H(11,13)=tt48;
  _H(11,14)=tt69;
  _H(11,15)=tt38;
  _H(11,16)=tt43;
  _H(11,17)=tt68;
  _H(11,18)=tt16;
  _H(11,19)=tt56;
  _H(11,20)=tt67;
  _H(11,21)=tt33;
  _H(11,22)=tt54;
  _H(11,23)=tt66;
  _H(12,0)=tt31;
  _H(12,1)=tt32;
  _H(12,2)=tt16;
  _H(12,3)=tt35;
  _H(12,4)=tt32;
  _H(12,5)=tt16;
  _H(12,6)=tt36;
  _H(12,7)=tt37;
  _H(12,8)=tt26;
  _H(12,9)=tt39;
  _H(12,10)=tt37;
  _H(12,11)=tt26;
  _H(12,12)=tt14;
  _H(12,13)=tt15;
  _H(12,14)=tt33;
  _H(12,15)=tt20;
  _H(12,16)=tt15;
  _H(12,17)=tt33;
  _H(12,18)=tt24;
  _H(12,19)=tt25;
  _H(12,20)=tt38;
  _H(12,21)=tt28;
  _H(12,22)=tt25;
  _H(12,23)=tt38;
  _H(13,0)=tt32;
  _H(13,1)=tt53;
  _H(13,2)=tt43;
  _H(13,3)=tt37;
  _H(13,4)=tt55;
  _H(13,5)=tt48;
  _H(13,6)=tt32;
  _H(13,7)=tt58;
  _H(13,8)=tt43;
  _H(13,9)=tt37;
  _H(13,10)=tt59;
  _H(13,11)=tt48;
  _H(13,12)=tt15;
  _H(13,13)=tt42;
  _H(13,14)=tt54;
  _H(13,15)=tt25;
  _H(13,16)=tt47;
  _H(13,17)=tt56;
  _H(13,18)=tt15;
  _H(13,19)=tt51;
  _H(13,20)=tt54;
  _H(13,21)=tt25;
  _H(13,22)=tt52;
  _H(13,23)=tt56;
  _H(14,0)=tt33;
  _H(14,1)=tt54;
  _H(14,2)=tt66;
  _H(14,3)=tt16;
  _H(14,4)=tt56;
  _H(14,5)=tt67;
  _H(14,6)=tt38;
  _H(14,7)=tt43;
  _H(14,8)=tt68;
  _H(14,9)=tt26;
  _H(14,10)=tt48;
  _H(14,11)=tt69;
  _H(14,12)=tt33;
  _H(14,13)=tt54;
  _H(14,14)=tt62;
  _H(14,15)=tt16;
  _H(14,16)=tt56;
  _H(14,17)=tt63;
  _H(14,18)=tt38;
  _H(14,19)=tt43;
  _H(14,20)=tt64;
  _H(14,21)=tt26;
  _H(14,22)=tt48;
  _H(14,23)=tt65;
  _H(15,0)=tt35;
  _H(15,1)=tt37;
  _H(15,2)=tt33;
  _H(15,3)=tt31;
  _H(15,4)=tt37;
  _H(15,5)=tt33;
  _H(15,6)=tt39;
  _H(15,7)=tt32;
  _H(15,8)=tt38;
  _H(15,9)=tt36;
  _H(15,10)=tt32;
  _H(15,11)=tt38;
  _H(15,12)=tt20;
  _H(15,13)=tt25;
  _H(15,14)=tt16;
  _H(15,15)=tt14;
  _H(15,16)=tt25;
  _H(15,17)=tt16;
  _H(15,18)=tt28;
  _H(15,19)=tt15;
  _H(15,20)=tt26;
  _H(15,21)=tt24;
  _H(15,22)=tt15;
  _H(15,23)=tt26;
  _H(16,0)=tt32;
  _H(16,1)=tt55;
  _H(16,2)=tt48;
  _H(16,3)=tt37;
  _H(16,4)=tt53;
  _H(16,5)=tt43;
  _H(16,6)=tt32;
  _H(16,7)=tt59;
  _H(16,8)=tt48;
  _H(16,9)=tt37;
  _H(16,10)=tt58;
  _H(16,11)=tt43;
  _H(16,12)=tt15;
  _H(16,13)=tt47;
  _H(16,14)=tt56;
  _H(16,15)=tt25;
  _H(16,16)=tt42;
  _H(16,17)=tt54;
  _H(16,18)=tt15;
  _H(16,19)=tt52;
  _H(16,20)=tt56;
  _H(16,21)=tt25;
  _H(16,22)=tt51;
  _H(16,23)=tt54;
  _H(17,0)=tt33;
  _H(17,1)=tt56;
  _H(17,2)=tt67;
  _H(17,3)=tt16;
  _H(17,4)=tt54;
  _H(17,5)=tt66;
  _H(17,6)=tt38;
  _H(17,7)=tt48;
  _H(17,8)=tt69;
  _H(17,9)=tt26;
  _H(17,10)=tt43;
  _H(17,11)=tt68;
  _H(17,12)=tt33;
  _H(17,13)=tt56;
  _H(17,14)=tt63;
  _H(17,15)=tt16;
  _H(17,16)=tt54;
  _H(17,17)=tt62;
  _H(17,18)=tt38;
  _H(17,19)=tt48;
  _H(17,20)=tt65;
  _H(17,21)=tt26;
  _H(17,22)=tt43;
  _H(17,23)=tt64;
  _H(18,0)=tt36;
  _H(18,1)=tt32;
  _H(18,2)=tt26;
  _H(18,3)=tt39;
  _H(18,4)=tt32;
  _H(18,5)=tt26;
  _H(18,6)=tt31;
  _H(18,7)=tt37;
  _H(18,8)=tt16;
  _H(18,9)=tt35;
  _H(18,10)=tt37;
  _H(18,11)=tt16;
  _H(18,12)=tt24;
  _H(18,13)=tt15;
  _H(18,14)=tt38;
  _H(18,15)=tt28;
  _H(18,16)=tt15;
  _H(18,17)=tt38;
  _H(18,18)=tt14;
  _H(18,19)=tt25;
  _H(18,20)=tt33;
  _H(18,21)=tt20;
  _H(18,22)=tt25;
  _H(18,23)=tt33;
  _H(19,0)=tt37;
  _H(19,1)=tt58;
  _H(19,2)=tt54;
  _H(19,3)=tt32;
  _H(19,4)=tt59;
  _H(19,5)=tt56;
  _H(19,6)=tt37;
  _H(19,7)=tt53;
  _H(19,8)=tt54;
  _H(19,9)=tt32;
  _H(19,10)=tt55;
  _H(19,11)=tt56;
  _H(19,12)=tt25;
  _H(19,13)=tt51;
  _H(19,14)=tt43;
  _H(19,15)=tt15;
  _H(19,16)=tt52;
  _H(19,17)=tt48;
  _H(19,18)=tt25;
  _H(19,19)=tt42;
  _H(19,20)=tt43;
  _H(19,21)=tt15;
  _H(19,22)=tt47;
  _H(19,23)=tt48;
  _H(20,0)=tt38;
  _H(20,1)=tt54;
  _H(20,2)=tt68;
  _H(20,3)=tt26;
  _H(20,4)=tt56;
  _H(20,5)=tt69;
  _H(20,6)=tt33;
  _H(20,7)=tt43;
  _H(20,8)=tt66;
  _H(20,9)=tt16;
  _H(20,10)=tt48;
  _H(20,11)=tt67;
  _H(20,12)=tt38;
  _H(20,13)=tt54;
  _H(20,14)=tt64;
  _H(20,15)=tt26;
  _H(20,16)=tt56;
  _H(20,17)=tt65;
  _H(20,18)=tt33;
  _H(20,19)=tt43;
  _H(20,20)=tt62;
  _H(20,21)=tt16;
  _H(20,22)=tt48;
  _H(20,23)=tt63;
  _H(21,0)=tt39;
  _H(21,1)=tt37;
  _H(21,2)=tt38;
  _H(21,3)=tt36;
  _H(21,4)=tt37;
  _H(21,5)=tt38;
  _H(21,6)=tt35;
  _H(21,7)=tt32;
  _H(21,8)=tt33;
  _H(21,9)=tt31;
  _H(21,10)=tt32;
  _H(21,11)=tt33;
  _H(21,12)=tt28;
  _H(21,13)=tt25;
  _H(21,14)=tt26;
  _H(21,15)=tt24;
  _H(21,16)=tt25;
  _H(21,17)=tt26;
  _H(21,18)=tt20;
  _H(21,19)=tt15;
  _H(21,20)=tt16;
  _H(21,21)=tt14;
  _H(21,22)=tt15;
  _H(21,23)=tt16;
  _H(22,0)=tt37;
  _H(22,1)=tt59;
  _H(22,2)=tt56;
  _H(22,3)=tt32;
  _H(22,4)=tt58;
  _H(22,5)=tt54;
  _H(22,6)=tt37;
  _H(22,7)=tt55;
  _H(22,8)=tt56;
  _H(22,9)=tt32;
  _H(22,10)=tt53;
  _H(22,11)=tt54;
  _H(22,12)=tt25;
  _H(22,13)=tt52;
  _H(22,14)=tt48;
  _H(22,15)=tt15;
  _H(22,16)=tt51;
  _H(22,17)=tt43;
  _H(22,18)=tt25;
  _H(22,19)=tt47;
  _H(22,20)=tt48;
  _H(22,21)=tt15;
  _H(22,22)=tt42;
  _H(22,23)=tt43;
  _H(23,0)=tt38;
  _H(23,1)=tt56;
  _H(23,2)=tt69;
  _H(23,3)=tt26;
  _H(23,4)=tt54;
  _H(23,5)=tt68;
  _H(23,6)=tt33;
  _H(23,7)=tt48;
  _H(23,8)=tt67;
  _H(23,9)=tt16;
  _H(23,10)=tt43;
  _H(23,11)=tt66;
  _H(23,12)=tt38;
  _H(23,13)=tt56;
  _H(23,14)=tt65;
  _H(23,15)=tt26;
  _H(23,16)=tt54;
  _H(23,17)=tt64;
  _H(23,18)=tt33;
  _H(23,19)=tt48;
  _H(23,20)=tt63;
  _H(23,21)=tt16;
  _H(23,22)=tt43;
  _H(23,23)=tt62;
}
template <int DIM>
Mat3d FEMModel<DIM>::calcR(const Vec& x) const
{
  //Stage1
  VecDIMd ctr0=VecDIMd::Zero();
  VecDIMd ctr=VecDIMd::Zero();
  for(sizeType i=0; i<NRP; i++) {
    ctr0+=_X0.template segment<DIM>(i*DIM);
    ctr+=x.template segment<DIM>(_id[i]);
  }
  ctr0/=(scalarD)NRP;
  ctr/=(scalarD)NRP;
  //Stage2
  MatDIMd S=MatDIMd::Zero();
  for(sizeType i=0; i<NRP; i++)
    S+=(_X0.template segment<DIM>(i*DIM)-ctr0)*(x.template segment<DIM>(_id[i])-ctr).transpose();
  //Stage3
  Mat3d ret=Mat3d::Zero();
  Eigen::JacobiSVD<MatDIMd> svd(S,Eigen::ComputeFullU|Eigen::ComputeFullV);
  ret.block<DIM,DIM>(0,0)=svd.matrixU()*svd.matrixV().transpose();
  return ret;
}
//mass
template <int DIM>
Matd getMass()
{
  sizeType NRP=FEMModel<DIM>::NRP;
  Matd ret=Matd::Zero(NRP,NRP);
  ret(0,0)=ret(1,1)=1/pow(3.0f,DIM);
  ret(0,1)=ret(1,0)=0.5f/pow(3.0f,DIM);
  for(sizeType i=1; i<DIM; i++) {
    sizeType nrP=1<<i;
    ret.block(0,nrP,nrP,nrP)=ret.block(0,0,nrP,nrP)*0.5f;
    ret.block(nrP,0,nrP,nrP)=ret.block(0,0,nrP,nrP)*0.5f;
    ret.block(nrP,nrP,nrP,nrP)=ret.block(0,0,nrP,nrP);
  }
  //cout << ret << endl;
  return ret;
}
template <int DIM>
Matd multK(const Matd& M)
{
  Matd ret=Matd::Zero(M.rows()*DIM,M.cols()*DIM);
  for(sizeType r=0; r<M.rows(); r++)
    for(sizeType c=0; c<M.cols(); c++)
      ret.block<DIM,DIM>(r*DIM,c*DIM)=FEMModel<DIM>::MatDIMd::Identity()*M(r,c);
  return ret;
}
template <>
const Matd FEMModel<2>::_M=getMass<2>();
template <>
const Matd FEMModel<2>::_MDIM=multK<2>(FEMModel<2>::_M);
template <>
const Matd FEMModel<3>::_M=getMass<3>();
template <>
const Matd FEMModel<3>::_MDIM=multK<3>(FEMModel<3>::_M);
template class FEMModel<2>;
template class FEMModel<3>;

PRJ_END
