#ifndef BUILD_SCENARIO_H
#define BUILD_SCENARIO_H

#include "DynamicModel.h"
#include <boost/property_tree/ptree.hpp>

PRJ_BEGIN

//build scenario
enum LIMIT_TYPE {
  DIRECTIONAL_QUADRATIC,
  DIRECTIONAL_CUBIC,
  DIRECTIONAL_SOFTMAX,
  DIRECTIONAL_LOG,

  AREA_QUADRATIC,
  AREA_CUBIC,
  AREA_SOFTMAX,
  AREA_LOG,

  LIMIT_NONE,
};
enum CONTACT_TYPE {
  CONTACT_QUADRATIC,
  CONTACT_CUBIC,
  CONTACT_SOFTMAX,
  CONTACT_FIX,
  CONTACT_NONE,
};
template <int DIM>
class BuildScenario
{
public:
  typedef typename DynamicModel<DIM>::VecDIMd VecDIMd;
  typedef typename DynamicModel<DIM>::VecDIMi VecDIMi;
  typedef typename DynamicModel<DIM>::Vec Vec;
  BuildScenario(bool quadratic=false,bool quadraticContact=false);
  void buildDirectionalStrainLimits(DynamicModel<DIM>& model,const VecDIMd& sz,const VecDIMi& res);
  void buildAreaStrainLimits(DynamicModel<DIM>& model,const VecDIMd& sz,const VecDIMi& res);
  void buildGroundContact(DynamicModel<DIM>& model,const VecDIMd& sz,const VecDIMi& res);
  DynamicModel<DIM> buildSpringGrid(const VecDIMd& sz,const VecDIMi& res);
  DynamicModel<DIM> buildFEMGrid(const VecDIMd& sz,const VecDIMi& res);
  boost::property_tree::ptree _pt;
};

PRJ_END

#endif
