#ifndef DYNAMICS_MODEL_H
#define DYNAMICS_MODEL_H

#include "LMInterface.h"

PRJ_BEGIN

template <int DIM>
class ElasticModel;
class LMInterface;
//DynamicModel
template <int DIM>
class DynamicModel : public Objective<scalarD>, public Serializable
{
public:
  typedef typename Eigen::Matrix<sizeType,DIM,1> VecDIMi;
  typedef typename Eigen::Matrix<scalarD,DIM,1> VecDIMd;
  typedef typename Eigen::Matrix<scalarD,DIM,DIM> MatDIMd;
  typedef ParallelMatrix<Vec> SVecs;
  DynamicModel();
  DynamicModel(const string& name);
  virtual void writeVTK(const string& path,const Vec* pData=NULL,const string& pDataName="") const;
  virtual void writeVTK(const Vec* x,const string& path,const Vec* pData=NULL,const string& pDataName="") const;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient) override;
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable) override;
  virtual scalarD operator()(const Vec& x,Vec* fgrad,STrips* fhess) override;
  virtual scalarD operatorThreadSafe(scalarD dt,const Vec& xP,const Vec& x,const Vec& xN,const Vec& f,Vec* fgrad,STrips* fhess) const;
  //dynamic system energy
  scalarD force(const Vec& x,const Vec& f,Vec* fgrad) const;
  scalarD potential(const Vec& x,SVecs* fgrad,STrips* fhess) const;
  scalarD groundContact(const Vec& x,SVecs* fgrad,STrips* fhess) const;
  scalarD strainLimiting(const Vec& x,SVecs* fgrad,STrips* fhess) const;
  scalarD kinetic(scalarD dt,const Vec& xP,const Vec& x,const Vec& xN,Vec* fgrad,STrips* fhess) const;
  virtual int inputs() const override;
  virtual int values() const override;
  virtual void advance(scalarD dt,bool useCB,const Vec* xP=NULL,boost::shared_ptr<LMInterface::LMDenseInterface> interface=NULL);
  virtual void advances(scalarD t,scalarD dt,bool useCB,const string& path,boost::shared_ptr<LMInterface::LMDenseInterface> interface=NULL);
  virtual void debugGradient(scalarD scale);
  virtual void debugErrorConfig();
  virtual void buildMass();
  //setter/getter
  void resetLM(bool useCholmod=false);
  void resetF();
#define DECLARE_GETTER(TYPE,NAME) \
const TYPE& get##NAME() const;  \
TYPE& get##NAME();
  DECLARE_GETTER(vector<boost::shared_ptr<ElasticModel<DIM> > >,Pss)
  DECLARE_GETTER(vector<boost::shared_ptr<ElasticModel<DIM> > >,Stss)
  DECLARE_GETTER(scalarD,TolG)
  DECLARE_GETTER(scalarD,Dt)
  DECLARE_GETTER(Vec,G)
  DECLARE_GETTER(Vec,F)
  DECLARE_GETTER(Vec,X0)
  DECLARE_GETTER(Vec,XN)
  DECLARE_GETTER(Vec,X)
  DECLARE_GETTER(Vec,XP)
  DECLARE_GETTER(SMat,M)
  DECLARE_GETTER(Vec,CoefPss)
  DECLARE_GETTER(Coli,ContactPoints)
#undef DECLARE_GETTER
  //transfer gradient
  virtual bool adjointTransferStage1ThreadSafe(scalarD dt,const Vec& xP,const Vec& x,const Vec& xN,const Vec& f,LMInterface::LMDenseInterface& interface) const;
  virtual void adjointTransferStage2ThreadSafe(scalarD dt,const Vec& xP,const Vec& x,const Vec& xN,const Vec& DEDXP,Vec* DEDX,Vec* DEDXN,Vec* DEDCoefP,Vec* DEDF,LMInterface::LMDenseInterface& interface) const;
  virtual void adjointTransfer(const Vec& DEDXP,Vec* DEDX,Vec* DEDXN,Vec* DEDCoefP,Vec* DEDF,LMInterface::LMDenseInterface& interface) const;
  virtual void adjointTransfer(const Vec& DEDXP,Vec* DEDX,Vec* DEDXN,Vec* DEDCoefP,Vec* DEDF) const;
  virtual void debugAdjointTransfer(scalarD dt,scalarD scale);
protected:
  boost::shared_ptr<LMInterface> _lm;
  vector<boost::shared_ptr<ElasticModel<DIM> > > _pss,_stss;
  bool _dirty;
  scalarD _tolG,_dt;
  Vec _g,_f,_x0,_xN,_x,_xP,_coefPss;
  Coli _contactPoints;
  SVecs _tmpFGrads;
  SMat _M;
};

PRJ_END

#endif
