#include "SpringModel.h"
#include "Utils.h"

USE_PRJ_NAMESPACE

//SpringModel
template <int DIM>
SpringModel<DIM>::SpringModel():ElasticModel<DIM>(typeid(SpringModel<DIM>).name()) {}
template <int DIM>
SpringModel<DIM>::SpringModel(const Vec& x,scalarD coefK,scalarD coefM,sizeType a,sizeType b,sizeType id):ElasticModel<DIM>(typeid(SpringModel<DIM>).name(),id),_coefK(coefK),_coefM(coefM),_a(a*DIM),_b(b*DIM)
{
  _l0=(x.segment<DIM>(_a)-x.segment<DIM>(_b)).norm();
  _coefM*=_l0;
}
template <int DIM>
void SpringModel<DIM>::writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const
{
  Vec2i iss[2];
  iss[0]=Vec2i(_a/DIM,_b/DIM);
  scalarD coefK[2],coefM[2];
  coefK[0]=_coefK*coef;
  coefM[0]=_coefM*coef;
  os.appendCells(iss,iss+1,VTKWriter<scalarD>::LINE);
  os.appendCustomData("coefK",coefK,coefK+1);
  os.appendCustomData("coefM",coefM,coefM+1);
  if(pData) {
    sizeType pid=ElasticModel<DIM>::_pid;
    os.appendCustomData(pDataName,pData->data()+pid,pData->data()+pid+1);
  }
}
template <int DIM>
bool SpringModel<DIM>::read(istream& is,IOData* dat)
{
  ElasticModel<DIM>::read(is,dat);
  readBinaryData(_l0,is);
  readBinaryData(_coefK,is);
  readBinaryData(_coefM,is);
  readBinaryData(_a,is);
  readBinaryData(_b,is);
  return is.good();
}
template <int DIM>
bool SpringModel<DIM>::write(ostream& os,IOData* dat) const
{
  ElasticModel<DIM>::write(os,dat);
  writeBinaryData(_l0,os);
  writeBinaryData(_coefK,os);
  writeBinaryData(_coefM,os);
  writeBinaryData(_a,os);
  writeBinaryData(_b,os);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> SpringModel<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new SpringModel);
}
template <int DIM>
Coli SpringModel<DIM>::ids() const
{
  return Vec2i(_a/DIM,_b/DIM);
}
template <int DIM>
scalarD SpringModel<DIM>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  VecDIMd dl,grad;
  MatDIMd ddl,hess;
  VecDIMd l=x.segment<DIM>(_a)-x.segment<DIM>(_b);
  scalarD lnorm=gradl(l,(fgrad || fhess) ? &dl : NULL,fhess ? &ddl : NULL);
  if(fgrad) {
    grad=dl*(lnorm-_l0)*(_coefK*coef);
    fgrad->getMatrixI().template segment<DIM>(_a)+=grad;
    fgrad->getMatrixI().template segment<DIM>(_b)-=grad;
  }
  if(fhess) {
    hess=(ddl*(lnorm-_l0)+dl*dl.transpose())*(_coefK*coef);
    addBlock(*fhess,_a,_a,hess);
    addBlock(*fhess,_b,_b,hess);
    addBlock(*fhess,_a,_b,-hess);
    addBlock(*fhess,_b,_a,-hess);
  }
  return pow(lnorm-_l0,2)*(coef*_coefK/2);
}
template <int DIM>
scalarD SpringModel<DIM>::operatorMKDot(const Vec& xP,const Vec& b,const Vec& other,sizeType offAug) const
{
  scalarD ret=0;
  VecDIMd dl,grad;
  VecDIMd l=xP.segment<DIM>(_a)-xP.segment<DIM>(_b);
  scalarD lnorm=gradl(l,&dl,NULL);
  grad=dl*(lnorm-_l0)*_coefK;
  ret+=other.template segment<DIM>(_a).dot(grad);
  ret-=other.template segment<DIM>(_b).dot(grad);
  scalarD coef=_coefM/3.0f;
  ret+=other.template segment<DIM>(_a).dot(b.segment<DIM>(_a)  +b.segment<DIM>(_b)/2)*coef;
  ret+=other.template segment<DIM>(_b).dot(b.segment<DIM>(_a)/2+b.segment<DIM>(_b)  )*coef;
  return ret;
}
template <int DIM>
void SpringModel<DIM>::operatorM(STrips& fhess,scalarD coef) const
{
  coef*=_coefM;
  addIK(fhess,_a,_a,coef/3.0f,DIM);
  addIK(fhess,_b,_b,coef/3.0f,DIM);
  addIK(fhess,_a,_b,coef/6.0f,DIM);
  addIK(fhess,_b,_a,coef/6.0f,DIM);
}
template <int DIM>
scalarD SpringModel<DIM>::kineticBruteForce(const Vec& x) const
{
#define SEG 100
  scalarD ret=0,alpha;
  for(sizeType i=0; i<SEG; i++) {
    alpha=((scalarD)i+0.5f)/(scalarD)SEG;
    VecDIMd vel=interp1D<VecDIMd,scalarD>(x.segment<DIM>(0),x.segment<DIM>(DIM),alpha);
    ret+=vel.squaredNorm();
  }
  return ret*(_coefM/(scalarD)SEG)/2;
#undef SEG
}
template <int DIM>
int SpringModel<DIM>::inputs() const
{
  return 2*DIM;
}
template <int DIM>
scalarD SpringModel<DIM>::gradl(const VecDIMd& l,VecDIMd* dl,MatDIMd* ddl)
{
  scalarD lnorm=l.norm();
  if(dl)
    *dl=l/lnorm;
  if(ddl)
    *ddl=(MatDIMd::Identity()-*dl*dl->transpose())/lnorm;
  return lnorm;
}
template class SpringModel<2>;
template class SpringModel<3>;
