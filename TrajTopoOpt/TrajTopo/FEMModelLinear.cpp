#include "FEMModelLinear.h"
#include "Utils.h"

PRJ_BEGIN

template <int DIM>
FEMModelLinear<DIM>::FEMModelLinear():FEMModel<DIM>(typeid(FEMModelLinear<DIM>).name()) {}
template <int DIM>
FEMModelLinear<DIM>::FEMModelLinear(const Vec& x,scalarD poisson,scalarD young,scalarD coefM,sizeType x0,sizeType x1,sizeType x2,sizeType x3,sizeType id):FEMModel<DIM>(typeid(FEMModelLinear<DIM>).name(),id)
{
  FEMModel<DIM>::_id << x0,x1,x2,x3;
  FEMModel<DIM>::_id*=DIM;
  FEMModel<DIM>::_coefM=coefM;
  FEMModel<DIM>::_l[0]=(x.template segment<DIM>(FEMModel<DIM>::_id[1])-x.template segment<DIM>(FEMModel<DIM>::_id[0])).norm();
  FEMModel<DIM>::_l[1]=(x.template segment<DIM>(FEMModel<DIM>::_id[2])-x.template segment<DIM>(FEMModel<DIM>::_id[0])).norm();
  for(sizeType i=0; i<FEMModel<DIM>::NRP; i++)
    FEMModel<DIM>::_X0.template segment<DIM>(i*DIM)=x.template segment<DIM>(FEMModel<DIM>::_id[i]);
  FEMModel<DIM>::calcH(poisson,young);
}
template <int DIM>
FEMModelLinear<DIM>::FEMModelLinear(const Vec& x,scalarD poisson,scalarD young,scalarD coefM,sizeType x0,sizeType x1,sizeType x2,sizeType x3,sizeType x4,sizeType x5,sizeType x6,sizeType x7,sizeType id):FEMModel<DIM>(typeid(FEMModelLinear<DIM>).name(),id)
{
  FEMModel<DIM>::_id << x0,x1,x2,x3,x4,x5,x6,x7;
  FEMModel<DIM>::_id*=DIM;
  FEMModel<DIM>::_coefM=coefM;
  FEMModel<DIM>::_l[0]=(x.template segment<DIM>(FEMModel<DIM>::_id[1])-x.template segment<DIM>(FEMModel<DIM>::_id[0])).norm();
  FEMModel<DIM>::_l[1]=(x.template segment<DIM>(FEMModel<DIM>::_id[2])-x.template segment<DIM>(FEMModel<DIM>::_id[0])).norm();
  FEMModel<DIM>::_l[2]=(x.template segment<DIM>(FEMModel<DIM>::_id[4])-x.template segment<DIM>(FEMModel<DIM>::_id[0])).norm();
  for(sizeType i=0; i<FEMModel<DIM>::NRP; i++)
    FEMModel<DIM>::_X0.template segment<DIM>(i*DIM)=x.template segment<DIM>(FEMModel<DIM>::_id[i]);
  FEMModel<DIM>::calcH(poisson,young);
}
template <int DIM>
boost::shared_ptr<Serializable> FEMModelLinear<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new FEMModelLinear);
}
template <int DIM>
int FEMModelLinear<DIM>::inputsAug() const
{
  return 0;
}
template <int DIM>
scalarD FEMModelLinear<DIM>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  VecDIMAlld DX,HX;
  for(sizeType i=0; i<FEMModel<DIM>::NRP; i++)
    DX.template segment<DIM>(i*DIM)=x.template segment<DIM>(FEMModel<DIM>::_id[i]);
  HX=FEMModel<DIM>::_H*(DX-FEMModel<DIM>::_X0)*coef;
  if(fgrad)
    for(sizeType i=0; i<FEMModel<DIM>::NRP; i++)
      fgrad->getMatrixI().template segment<DIM>(FEMModel<DIM>::_id[i])+=HX.template segment<DIM>(i*DIM);
  if(fhess)
    for(sizeType i=0; i<FEMModel<DIM>::NRP; i++)
      for(sizeType j=0; j<FEMModel<DIM>::NRP; j++)
        addBlock(*fhess,FEMModel<DIM>::_id[i],FEMModel<DIM>::_id[j],FEMModel<DIM>::_H.template block<DIM,DIM>(i*DIM,j*DIM)*coef);
  return (DX-FEMModel<DIM>::_X0).dot(HX)/2;
}
template <int DIM>
scalarD FEMModelLinear<DIM>::operatorMKDot(const Vec& xP,const Vec& b,const Vec& other,sizeType offAug) const
{
  VecDIMAlld X,O,B;
  for(sizeType i=0; i<FEMModel<DIM>::NRP; i++) {
    X.template segment<DIM>(i*DIM)=xP.template segment<DIM>(FEMModel<DIM>::_id[i]);
    B.template segment<DIM>(i*DIM)=b.template segment<DIM>(FEMModel<DIM>::_id[i]);
    O.template segment<DIM>(i*DIM)=other.template segment<DIM>(FEMModel<DIM>::_id[i]);
  }
  return O.dot(FEMModel<DIM>::_H*(X-FEMModel<DIM>::_X0)+FEMModel<DIM>::_MDIM*B*(FEMModel<DIM>::_coefM*FEMModel<DIM>::_l.prod()));
}
template <int DIM>
bool FEMModelLinear<DIM>::isQuadratic() const
{
  return true;
}
template class FEMModelLinear<2>;
template class FEMModelLinear<3>;

PRJ_END
