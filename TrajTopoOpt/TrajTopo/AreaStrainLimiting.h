#ifndef AREA_STRAIN_LIMITING_H
#define AREA_STRAIN_LIMITING_H

#include "ElasticModel.h"

PRJ_BEGIN

//Area Strain Limiting
class AreaStrainLimitingQuadratic : public ElasticModel<2>
{
public:
  using typename Objective<scalarD>::Vec;
  using typename Objective<scalarD>::STrips;
  using typename ElasticModel<2>::VecDIMd;
  using typename ElasticModel<2>::MatDIMd;
  using typename ElasticModel<2>::SVecs;
  AreaStrainLimitingQuadratic();
  AreaStrainLimitingQuadratic(const string& name);
  AreaStrainLimitingQuadratic(const Vec& x,sizeType a,sizeType b,sizeType c,const Vec2d& limit,scalarD coef);
  virtual void writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const override;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual Coli ids() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
  virtual int inputs() const override;
protected:
  scalarD _d0,_d1,_coef;
  Vec6i _id;
};
class AreaStrainLimitingCubic : public AreaStrainLimitingQuadratic
{
public:
  using typename AreaStrainLimitingQuadratic::Vec;
  using typename AreaStrainLimitingQuadratic::STrips;
  using typename AreaStrainLimitingQuadratic::VecDIMd;
  using typename AreaStrainLimitingQuadratic::MatDIMd;
  using typename AreaStrainLimitingQuadratic::SVecs;
  using NAMESPACE::AreaStrainLimitingQuadratic::_d0;
  using NAMESPACE::AreaStrainLimitingQuadratic::_d1;
  using NAMESPACE::AreaStrainLimitingQuadratic::_coef;
  using NAMESPACE::AreaStrainLimitingQuadratic::_id;
  AreaStrainLimitingCubic();
  AreaStrainLimitingCubic(const Vec& x,sizeType a,sizeType b,sizeType c,const Vec2d& limit,scalarD coef);
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
};
class AreaStrainLimitingSoftmax : public AreaStrainLimitingQuadratic
{
public:
  using typename AreaStrainLimitingQuadratic::Vec;
  using typename AreaStrainLimitingQuadratic::STrips;
  using typename AreaStrainLimitingQuadratic::VecDIMd;
  using typename AreaStrainLimitingQuadratic::MatDIMd;
  using typename AreaStrainLimitingQuadratic::SVecs;
  using NAMESPACE::AreaStrainLimitingQuadratic::_d0;
  using NAMESPACE::AreaStrainLimitingQuadratic::_d1;
  using NAMESPACE::AreaStrainLimitingQuadratic::_coef;
  using NAMESPACE::AreaStrainLimitingQuadratic::_id;
  AreaStrainLimitingSoftmax();
  AreaStrainLimitingSoftmax(const Vec& x,sizeType a,sizeType b,sizeType c,const Vec2d& limit,scalarD coef);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
protected:
  scalarD _softmaxReg;
};
class AreaStrainLimitingLog : public AreaStrainLimitingQuadratic
{
public:
  using typename AreaStrainLimitingQuadratic::Vec;
  using typename AreaStrainLimitingQuadratic::STrips;
  using typename AreaStrainLimitingQuadratic::VecDIMd;
  using typename AreaStrainLimitingQuadratic::MatDIMd;
  using typename AreaStrainLimitingQuadratic::SVecs;
  using NAMESPACE::AreaStrainLimitingQuadratic::_d0;
  using NAMESPACE::AreaStrainLimitingQuadratic::_d1;
  using NAMESPACE::AreaStrainLimitingQuadratic::_coef;
  using NAMESPACE::AreaStrainLimitingQuadratic::_id;
  AreaStrainLimitingLog();
  AreaStrainLimitingLog(const Vec& x,sizeType a,sizeType b,sizeType c,const Vec2d& limit,scalarD coef);
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
};

//Volume Strain Limiting
class VolumeStrainLimitingQuadratic : public ElasticModel<3>
{
public:
  using typename Objective<scalarD>::Vec;
  using typename Objective<scalarD>::STrips;
  using typename ElasticModel<3>::VecDIMd;
  using typename ElasticModel<3>::MatDIMd;
  using typename ElasticModel<3>::SVecs;
  VolumeStrainLimitingQuadratic();
  VolumeStrainLimitingQuadratic(const string& name);
  VolumeStrainLimitingQuadratic(const Vec& x,sizeType a,sizeType b,sizeType c,sizeType d,const Vec2d& limit,scalarD coef);
  virtual void writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const override;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual Coli ids() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
  virtual int inputs() const override;
protected:
  static scalarD grada(const Vec3d& a,const Vec3d& b,const Vec3d& c,const Vec3d& d,Vec12d* J,Mat12d* H,scalarD hCoef,scalarD jjtCoef);
  scalarD _d0,_d1,_coef;
  Vec12i _id;
};
class VolumeStrainLimitingCubic : public VolumeStrainLimitingQuadratic
{
public:
  using typename VolumeStrainLimitingQuadratic::Vec;
  using typename VolumeStrainLimitingQuadratic::STrips;
  using typename VolumeStrainLimitingQuadratic::VecDIMd;
  using typename VolumeStrainLimitingQuadratic::MatDIMd;
  using typename VolumeStrainLimitingQuadratic::SVecs;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_d0;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_d1;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_coef;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_id;
  VolumeStrainLimitingCubic();
  VolumeStrainLimitingCubic(const Vec& x,sizeType a,sizeType b,sizeType c,sizeType d,const Vec2d& limit,scalarD coef);
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
};
class VolumeStrainLimitingSoftmax : public VolumeStrainLimitingQuadratic
{
public:
  using typename VolumeStrainLimitingQuadratic::Vec;
  using typename VolumeStrainLimitingQuadratic::STrips;
  using typename VolumeStrainLimitingQuadratic::VecDIMd;
  using typename VolumeStrainLimitingQuadratic::MatDIMd;
  using typename VolumeStrainLimitingQuadratic::SVecs;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_d0;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_d1;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_coef;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_id;
  VolumeStrainLimitingSoftmax();
  VolumeStrainLimitingSoftmax(const Vec& x,sizeType a,sizeType b,sizeType c,sizeType d,const Vec2d& limit,scalarD coef);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
protected:
  scalarD _softmaxReg;
};
class VolumeStrainLimitingLog : public VolumeStrainLimitingQuadratic
{
public:
  using typename VolumeStrainLimitingQuadratic::Vec;
  using typename VolumeStrainLimitingQuadratic::STrips;
  using typename VolumeStrainLimitingQuadratic::VecDIMd;
  using typename VolumeStrainLimitingQuadratic::MatDIMd;
  using typename VolumeStrainLimitingQuadratic::SVecs;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_d0;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_d1;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_coef;
  using NAMESPACE::VolumeStrainLimitingQuadratic::_id;
  VolumeStrainLimitingLog();
  VolumeStrainLimitingLog(const Vec& x,sizeType a,sizeType b,sizeType c,sizeType d,const Vec2d& limit,scalarD coef);
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
};

PRJ_END

#endif
