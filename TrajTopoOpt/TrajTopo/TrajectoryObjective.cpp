#include "TrajectoryObjective.h"
#include "DynamicModel.h"

PRJ_BEGIN

//TrajectoryObjective
template <int DIM>
TrajectoryObjective<DIM>::TrajectoryObjective(const string& name):Serializable(name) {}
template <int DIM>
scalarD TrajectoryObjective<DIM>::operator()(const Vss& xss,Vss* dxss,const Vec& x,Vec* dfdx)
{
  return 0;
}
template <int DIM>
void TrajectoryObjective<DIM>::makeLocalMinima(const Vss& xss,const Vss& dxss,const Vec& x,const Vec& dfdx) {}
template <int DIM>
void TrajectoryObjective<DIM>::debugGradient(sizeType szTraj,sizeType nrX,sizeType nrVar)
{
#define NR_TEST 10
#define DELTA 1E-15f
  for(sizeType i=0; i<NR_TEST; i++) {
    Vec x=Vec::Random(nrVar),tmp;
    Vec dfdx=Vec::Zero(nrVar);
    Vss xss(szTraj,Vec::Zero(nrX));
    Vss dxss=xss,deltass=xss;
    for(sizeType t=0; t<szTraj; t++) {
      xss[t].setRandom();
      deltass[t].setRandom();
    }
    Vec delta=Vec::Random(nrVar);
    scalarD E1=operator()(xss,&dxss,x,&dfdx);
    INFO("-------------------------------------------------------------")
    //var
    tmp=x;
    x+=delta*DELTA;
    scalarD E2=operator()(xss,NULL,x,NULL);
    INFOV("GradientVar: %f Err: %f",dfdx.dot(delta),dfdx.dot(delta)-(E2-E1)/DELTA)
    x=tmp;
    //xss
    scalarD g=0;
    for(sizeType t=0; t<szTraj; t++) {
      xss[t]+=deltass[t]*DELTA;
      g+=deltass[t].dot(dxss[t]);
    }
    E2=operator()(xss,NULL,x,NULL);
    INFOV("GradientX: %f Err: %f",g,g-(E2-E1)/DELTA)
  }
#undef DELTA
#undef NR_TEST
}
template <int DIM>
void TrajectoryObjective<DIM>::reset(const DynamicModel<DIM>& model) {}
//CompositeObjective
template <int DIM>
CompositeObjective<DIM>::CompositeObjective():TrajectoryObjective<DIM>(typeid(CompositeObjective<DIM>).name()) {}
template <int DIM>
bool CompositeObjective<DIM>::read(istream& is,IOData* dat)
{
  readVector(_objs,is,dat);
  return is.good();
}
template <int DIM>
bool CompositeObjective<DIM>::write(ostream& os,IOData* dat) const
{
  writeVector(_objs,os,dat);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> CompositeObjective<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new CompositeObjective<DIM>());
}
template <int DIM>
scalarD CompositeObjective<DIM>::operator()(const Vss& xss,Vss* dxss,const Vec& x,Vec* dfdx)
{
  scalarD E=0;
  for(sizeType i=0; i<(sizeType)_objs.size(); i++)
    E+=_objs[i]->operator()(xss,dxss,x,dfdx);
  return E;
}
template <int DIM>
void CompositeObjective<DIM>::makeLocalMinima(const Vss& xss,const Vss& dxss,const Vec& x,const Vec& dfdx)
{
  for(sizeType i=0; i<(sizeType)_objs.size(); i++)
    _objs[i]->makeLocalMinima(xss,dxss,x,dfdx);
}
template <int DIM>
void CompositeObjective<DIM>::reset(const DynamicModel<DIM>& model)
{
  for(sizeType i=0; i<(sizeType)_objs.size(); i++)
    _objs[i]->reset(model);
}
template class CompositeObjective<2>;
template class CompositeObjective<3>;
//RegularizeObjective
template <int DIM>
RegularizeObjective<DIM>::RegularizeObjective():TrajectoryObjective<DIM>(typeid(RegularizeObjective<DIM>).name()) {}
template <int DIM>
bool RegularizeObjective<DIM>::read(istream& is,IOData* dat)
{
  readBinaryData(_reg,is);
  return is.good();
}
template <int DIM>
bool RegularizeObjective<DIM>::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_reg,os);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> RegularizeObjective<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new RegularizeObjective());
}
template <int DIM>
scalarD RegularizeObjective<DIM>::operator()(const Vss& xss,Vss* dxss,const Vec& x,Vec* dfdx)
{
  if(dfdx) {
    dfdx->setZero(x.size());
    (*dfdx)[x.size()-1]=_reg*x[x.size()-1];
  }
  return _reg*x[x.size()-1]*x[x.size()-1]/2;
}
template <int DIM>
void RegularizeObjective<DIM>::makeLocalMinima(const Vss& xss,const Vss& dxss,const Vec& x,const Vec& dfdx)
{
  sizeType off=dfdx.size()-1;
  _reg-=dfdx[off]/x[off];
}
template class RegularizeObjective<2>;
template class RegularizeObjective<3>;
//HeightObjective
template <int DIM>
HeightObjective<DIM>::HeightObjective():TrajectoryObjective<DIM>(typeid(HeightObjective<DIM>).name()) {}
template <int DIM>
HeightObjective<DIM>::HeightObjective(const DynamicModel<DIM>& model)
  :TrajectoryObjective<DIM>(typeid(HeightObjective<DIM>).name())
{
  reset(model);
}
template <int DIM>
bool HeightObjective<DIM>::read(istream& is,IOData* dat)
{
  readBinaryData(_averageHeight,is);
  readBinaryData(_expCoef,is);
  return is.good();
}
template <int DIM>
bool HeightObjective<DIM>::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_averageHeight,os);
  writeBinaryData(_expCoef,os);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> HeightObjective<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new HeightObjective<DIM>());
}
template <int DIM>
scalarD HeightObjective<DIM>::operator()(const Vss& xss,Vss* dxss,const Vec& x,Vec* dfdx)
{
  scalarD num=0,denom=0;
  Vec H=Vec::Zero(xss.size());
  for(sizeType i=0; i<(sizeType)xss.size(); i++) {
    H[i]=xss[i].template segment(0,_averageHeight.size()).dot(_averageHeight);
    scalarD W=exp(H[i]*_expCoef);
    num+=W*H[i];
    denom+=W;
  }
  if(dxss)
    for(sizeType i=0; i<(sizeType)xss.size(); i++) {
      scalarD W=exp(H[i]*_expCoef);
      dxss->at(i).segment(0,_averageHeight.size())+=_averageHeight*(num*W*_expCoef/(denom*denom));
      dxss->at(i).segment(0,_averageHeight.size())-=_averageHeight*(W*(1+H[i]*_expCoef)/denom);
    }
  return -num/denom;
}
template <int DIM>
void HeightObjective<DIM>::reset(const DynamicModel<DIM>& model)
{
  const Coli& cp=model.getContactPoints();
  const Vec& g=model.getG();
  _averageHeight.setZero(g.size());
  for(sizeType i=0; i<(sizeType)cp.size(); i++)
    _averageHeight.template segment<DIM>(cp[i]*DIM)=-g.template segment<DIM>(cp[i]*DIM).normalized();
  _averageHeight/=(scalarD)cp.size();
}
template class HeightObjective<2>;
template class HeightObjective<3>;
//RandomObjective
template <int DIM>
RandomObjective<DIM>::RandomObjective():TrajectoryObjective<DIM>(typeid(RandomObjective<DIM>).name()) {}
template <int DIM>
RandomObjective<DIM>::RandomObjective(const DynamicModel<DIM>& model)
  :TrajectoryObjective<DIM>(typeid(RandomObjective<DIM>).name())
{
  reset(model);
}
template <int DIM>
bool RandomObjective<DIM>::read(istream& is,IOData* dat)
{
  readBinaryData(_coef,is);
  return is.good();
}
template <int DIM>
bool RandomObjective<DIM>::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_coef,os);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> RandomObjective<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new RandomObjective<DIM>());
}
template <int DIM>
scalarD RandomObjective<DIM>::operator()(const Vss& xss,Vss* dxss,const Vec& x,Vec* dfdx)
{
  scalarD ret=0;
  for(sizeType i=0; i<(sizeType)xss.size(); i++) {
    ret+=xss[i].segment(0,_coef.size()).dot(_coef);
    if(dxss)
      dxss->at(i).segment(0,_coef.size())+=_coef;
  }
  return ret;
}
template <int DIM>
void RandomObjective<DIM>::reset(const DynamicModel<DIM>& model)
{
  _coef.setRandom(model.getX0().size());
}
template class RandomObjective<2>;
template class RandomObjective<3>;

PRJ_END
