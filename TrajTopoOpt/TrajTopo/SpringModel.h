#ifndef SPRING_MODEL_H
#define SPRING_MODEL_H

#include "ElasticModel.h"

PRJ_BEGIN

//SpringModel
template <int DIM>
class SpringModel : public ElasticModel<DIM>
{
public:
  using typename Objective<scalarD>::Vec;
  using typename Objective<scalarD>::STrips;
  using typename ElasticModel<DIM>::VecDIMd;
  using typename ElasticModel<DIM>::MatDIMd;
  using typename ElasticModel<DIM>::SVecs;
  SpringModel();
  SpringModel(const Vec& x,scalarD coefK,scalarD coefM,sizeType a,sizeType b,sizeType id);
  virtual void writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const override;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual Coli ids() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
  virtual scalarD operatorMKDot(const Vec& xP,const Vec& b,const Vec& other,sizeType offAug) const override;
  virtual void operatorM(STrips& fhess,scalarD coef) const override;
  virtual scalarD kineticBruteForce(const Vec& x) const override;
  virtual int inputs() const override;
private:
  static scalarD gradl(const VecDIMd& l,VecDIMd* dl,MatDIMd* ddl);
  scalarD _l0,_coefK,_coefM;
  sizeType _a,_b;
};

PRJ_END

#endif
