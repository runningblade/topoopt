#ifndef UTIL_H
#define UTIL_H

#include <CommonFile/solvers/ParallelVector.h>
#include <CommonFile/IO.h>
#include <Eigen/Sparse>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>

PRJ_BEGIN

//hash
struct Hash {
  size_t operator()(const Vec2i& key) const;
  size_t operator()(const Vec3i& key) const;
  size_t operator()(const Vec4i& key) const;
  size_t operator()(const Coli& key) const;
};
//filesystem
bool exists(const boost::filesystem::path& path);
void removeDir(const boost::filesystem::path& path);
void create(const boost::filesystem::path& path);
void recreate(const boost::filesystem::path& path);
vector<boost::filesystem::path> files(const boost::filesystem::path& path);
vector<boost::filesystem::path> directories(const boost::filesystem::path& path);
void sortFilesByNumber(vector<boost::filesystem::path>& files);
bool isDir(const boost::filesystem::path& path);
size_t fileSize(const boost::filesystem::path& path);
string parseProps(sizeType argc,char** argv,boost::property_tree::ptree& pt);
//property tree
void readPtreeBinary(boost::property_tree::ptree& pt,istream& is);
void writePtreeBinary(const boost::property_tree::ptree& pt,ostream& os);
void readPtreeAscii(boost::property_tree::ptree& pt,const boost::filesystem::path& path);
void writePtreeAscii(const boost::property_tree::ptree& pt,const boost::filesystem::path& path);
//deformation gradient
void calcGComp2D(Mat9X12d& GComp,const Mat3d& d);
void calcGComp3D(Mat9X12d& GComp,const Mat3d& d);
//check validity
template<typename MAT>
bool isFinite(const MAT& x)
{
  return ((x-x).array() == (x-x).array()).all();
}
template<typename MAT>
bool tooLarge(const MAT& x,const typename MAT::Scalar val)
{
  return (x.array().abs() > val).any();
}
//matrix
template <typename T>
static void addIK(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,T coef,int K)
{
  for(int k=0; k<K; k++)
    H.push_back(Eigen::Triplet<scalarD,sizeType>(r+k,c+k,coef));
}
template <typename MT>
static void addI(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,const MT& coef)
{
  sizeType nr=coef.size();
  for(sizeType i=0; i<nr; i++)
    H.push_back(Eigen::Triplet<scalarD,sizeType>(r+i,c+i,coef[i]));
}
template <typename MT>
static void addBlock(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,const MT& coef)
{
  sizeType nrR=coef.rows();
  sizeType nrC=coef.cols();
  for(sizeType i=0; i<nrR; i++)
    for(sizeType j=0; j<nrC; j++)
      H.push_back(Eigen::Triplet<scalarD,sizeType>(r+i,c+j,coef(i,j)));
}
template <typename IT,typename MT>
static void addBlockIndexed(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,const IT& index,const MT& coef)
{
  sizeType nrR=coef.rows();
  sizeType nrC=coef.cols();
  ASSERT(nrR == nrC && nrR == index.size())
  for(sizeType i=0; i<nrR; i++)
    for(sizeType j=0; j<nrR; j++)
      H.push_back(Eigen::Triplet<scalarD,sizeType>(index[i],index[j],coef(i,j)));
}
template <typename SMAT=Eigen::SparseMatrix<scalarD,0,sizeType> >
static void addSparseBlock(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,const SMAT& mat,typename SMAT::Scalar coef)
{
  for(sizeType k=0; k<mat.outerSize(); ++k)
    for(typename SMAT::InnerIterator it(mat,k); it; ++it)
      H.push_back(Eigen::Triplet<scalarD,sizeType>(it.row()+r,it.col()+c,it.value()*coef));
}
template <typename T>
static void extend(T& m,sizeType r,sizeType c)
{
  T tmp=m;
  m.setZero(m.rows()+r,m.cols()+c);
  m.block(0,0,tmp.rows(),tmp.cols())=tmp;
}
//vector read/write for property_tree
template <typename VEC>
VEC parseVec(const string& name,const boost::property_tree::ptree& tree)
{
  vector<typename VEC::Scalar> x;
  for(sizeType i=0;; i++) {
    boost::optional<typename VEC::Scalar> prop=
      tree.get_optional<typename VEC::Scalar>(name+".x"+boost::lexical_cast<string>(i));
    if(!prop)
      break;
    x.push_back(*prop);
  }
  if(VEC::RowsAtCompileTime > 0)
    return Eigen::Map<const VEC>(&x[0],VEC::RowsAtCompileTime);
  else return Eigen::Map<const VEC>(&x[0],x.size());
}
template <typename VEC>
void assignVec(const VEC& x,const string& name,boost::property_tree::ptree& tree)
{
  for(sizeType i=0; i<x.size(); i++)
    tree.put<typename VEC::Scalar>(name+".x"+boost::lexical_cast<string>(i),x[i]);
}
//diagonal shift for sparse matrix
template <typename SMAT=Eigen::SparseMatrix<scalarD,0,sizeType> >
typename SMAT::Scalar maxAbsSparseDiag(const SMAT& mat)
{
  typename SMAT::Scalar ret=0;
  for(sizeType k=0; k<mat.outerSize(); ++k)
    for(typename SMAT::InnerIterator it(mat,k); it; ++it)
      if(it.row() == it.col())
        ret=max(ret,abs(it.value()));
  return ret;
}
template <typename SMAT=Eigen::SparseMatrix<scalarD,0,sizeType> >
void addSparseDiag(SMAT& mat,typename SMAT::Scalar shift)
{
  for(sizeType k=0; k<mat.outerSize(); ++k)
    for(typename SMAT::InnerIterator it(mat,k); it; ++it)
      if(it.row() == it.col())
        it.valueRef()+=shift;
}

PRJ_END

#endif
