#include "DirectionalStrainLimiting.h"
#include "Utils.h"

USE_PRJ_NAMESPACE

//DirectionalStrainLimitingQuadratic
template <int DIM>
DirectionalStrainLimitingQuadratic<DIM>::DirectionalStrainLimitingQuadratic():ElasticModel<DIM>(typeid(DirectionalStrainLimitingQuadratic<DIM>).name(),-1) {}
template <int DIM>
DirectionalStrainLimitingQuadratic<DIM>::DirectionalStrainLimitingQuadratic(const string& name):ElasticModel<DIM>(name,-1) {}
template <int DIM>
DirectionalStrainLimitingQuadratic<DIM>::DirectionalStrainLimitingQuadratic(const Vec& x,sizeType a,sizeType b,const Vec2d& limit,scalarD coef):ElasticModel<DIM>(typeid(DirectionalStrainLimitingQuadratic<DIM>).name(),-1)
{
  _a=a*DIM;
  _b=b*DIM;
  VecDIMd a2b=x.template segment<DIM>(_a)-x.template segment<DIM>(_b);
  _d0=-a2b.norm()*limit[0];
  _d1=-a2b.norm()*limit[1];
  _coef=coef;
}
template <int DIM>
void DirectionalStrainLimitingQuadratic<DIM>::writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const
{
  FUNCTION_NOT_IMPLEMENTED
}
template <int DIM>
bool DirectionalStrainLimitingQuadratic<DIM>::read(istream& is,IOData* dat)
{
  ElasticModel<DIM>::read(is,dat);
  readBinaryData(_d0,is);
  readBinaryData(_d1,is);
  readBinaryData(_coef,is);
  readBinaryData(_a,is);
  readBinaryData(_b,is);
  return is.good();
}
template <int DIM>
bool DirectionalStrainLimitingQuadratic<DIM>::write(ostream& os,IOData* dat) const
{
  ElasticModel<DIM>::write(os,dat);
  writeBinaryData(_d0,os);
  writeBinaryData(_d1,os);
  writeBinaryData(_coef,os);
  writeBinaryData(_a,os);
  writeBinaryData(_b,os);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> DirectionalStrainLimitingQuadratic<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new DirectionalStrainLimitingQuadratic<DIM>());
}
template <int DIM>
Coli DirectionalStrainLimitingQuadratic<DIM>::ids() const
{
  return Vec2i(_a/DIM,_b/DIM);
}
template <int DIM>
scalarD DirectionalStrainLimitingQuadratic<DIM>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  VecDIMd dl,grad;
  MatDIMd ddl,hess;
  VecDIMd a2b=x.template segment<DIM>(_a)-x.template segment<DIM>(_b);
  scalarD lnorm=gradl(a2b,(fgrad || fhess) ? &dl : NULL,fhess ? &ddl : NULL);
  scalarD delta0=min<scalarD>(lnorm+_d0,0),delta1=max<scalarD>(lnorm+_d1,0);
  if(delta0+delta1 == 0)
    return 0;
  if(fgrad) {
    grad=dl*((delta0+delta1)*coef);
    Vec& fgradI=fgrad->getMatrixI();
    fgradI.template segment<DIM>(_a)+=grad;
    fgradI.template segment<DIM>(_b)-=grad;
  }
  if(fhess) {
    hess=ddl*(delta0+delta1);
    if(delta0 != 0 || delta1 != 0)
      hess+=dl*dl.transpose();
    hess*=coef;
    addBlock(*fhess,_a,_a,hess);
    addBlock(*fhess,_b,_b,hess);
    addBlock(*fhess,_a,_b,-hess);
    addBlock(*fhess,_b,_a,-hess);
  }
  return (delta0*delta0+delta1*delta1)*coef/2;
}
template <int DIM>
int DirectionalStrainLimitingQuadratic<DIM>::inputs() const
{
  return 2*DIM;
}
template <int DIM>
scalarD DirectionalStrainLimitingQuadratic<DIM>::gradl(const VecDIMd& l,VecDIMd* dl,MatDIMd* ddl)
{
  scalarD lnorm=l.norm();
  if(dl)
    *dl=l/lnorm;
  if(ddl)
    *ddl=(MatDIMd::Identity()-*dl*dl->transpose())/lnorm;
  return lnorm;
}
template class DirectionalStrainLimitingQuadratic<2>;
template class DirectionalStrainLimitingQuadratic<3>;
//DirectionalStrainLimitingCubic
template <int DIM>
DirectionalStrainLimitingCubic<DIM>::DirectionalStrainLimitingCubic():DirectionalStrainLimitingQuadratic<DIM>(typeid(DirectionalStrainLimitingCubic<DIM>).name()) {}
template <int DIM>
DirectionalStrainLimitingCubic<DIM>::DirectionalStrainLimitingCubic(const Vec& x,sizeType a,sizeType b,const Vec2d& limit,scalarD coef):DirectionalStrainLimitingQuadratic<DIM>(typeid(DirectionalStrainLimitingCubic<DIM>).name())
{
  _a=a*DIM;
  _b=b*DIM;
  VecDIMd a2b=x.template segment<DIM>(_a)-x.template segment<DIM>(_b);
  _d0=-a2b.norm()*limit[0];
  _d1=-a2b.norm()*limit[1];
  _coef=coef;
}
template <int DIM>
boost::shared_ptr<Serializable> DirectionalStrainLimitingCubic<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new DirectionalStrainLimitingCubic<DIM>());
}
template <int DIM>
scalarD DirectionalStrainLimitingCubic<DIM>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  VecDIMd dl,grad;
  MatDIMd ddl,hess;
  VecDIMd a2b=x.template segment<DIM>(_a)-x.template segment<DIM>(_b);
  scalarD lnorm=gradl(a2b,(fgrad || fhess) ? &dl : NULL,fhess ? &ddl : NULL);
  scalarD delta0=min<scalarD>(lnorm+_d0,0),delta1=max<scalarD>(lnorm+_d1,0);
  if(delta0+delta1 == 0)
    return 0;
  if(fgrad) {
    grad=dl*((delta1*delta1-delta0*delta0)*coef);
    Vec& fgradI=fgrad->getMatrixI();
    fgradI.template segment<DIM>(_a)+=grad;
    fgradI.template segment<DIM>(_b)-=grad;
  }
  if(fhess) {
    hess=ddl*((delta1*delta1-delta0*delta0)*coef);
    if(delta0 != 0 || delta1 != 0)
      hess+=dl*dl.transpose()*(2*(delta1-delta0)*coef);
    addBlock(*fhess,_a,_a,hess);
    addBlock(*fhess,_b,_b,hess);
    addBlock(*fhess,_a,_b,-hess);
    addBlock(*fhess,_b,_a,-hess);
  }
  return (delta1*delta1*delta1-delta0*delta0*delta0)*coef/3;
}
template class DirectionalStrainLimitingCubic<2>;
template class DirectionalStrainLimitingCubic<3>;
//DirectionalStrainLimitingSoftmax
template <int DIM>
DirectionalStrainLimitingSoftmax<DIM>::DirectionalStrainLimitingSoftmax():DirectionalStrainLimitingQuadratic<DIM>(typeid(DirectionalStrainLimitingSoftmax<DIM>).name()) {}
template <int DIM>
DirectionalStrainLimitingSoftmax<DIM>::DirectionalStrainLimitingSoftmax(const Vec& x,sizeType a,sizeType b,const Vec2d& limit,scalarD coef):DirectionalStrainLimitingQuadratic<DIM>(typeid(DirectionalStrainLimitingSoftmax<DIM>).name())
{
  _a=a*DIM;
  _b=b*DIM;
  VecDIMd a2b=x.template segment<DIM>(_a)-x.template segment<DIM>(_b);
  _softmaxReg=10/a2b.norm();
  _d0=-a2b.norm()*limit[0]*_softmaxReg;
  _d1=-a2b.norm()*limit[1]*_softmaxReg;
  _coef=coef/_softmaxReg/_softmaxReg;
}
template <int DIM>
bool DirectionalStrainLimitingSoftmax<DIM>::read(istream& is,IOData* dat)
{
  DirectionalStrainLimitingQuadratic<DIM>::read(is,dat);
  readBinaryData(_softmaxReg,is);
  return is.good();
}
template <int DIM>
bool DirectionalStrainLimitingSoftmax<DIM>::write(ostream& os,IOData* dat) const
{
  DirectionalStrainLimitingQuadratic<DIM>::write(os,dat);
  writeBinaryData(_softmaxReg,os);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> DirectionalStrainLimitingSoftmax<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new DirectionalStrainLimitingSoftmax<DIM>());
}
template <int DIM>
scalarD DirectionalStrainLimitingSoftmax<DIM>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  VecDIMd dl,grad;
  MatDIMd ddl,hess;
  VecDIMd a2b=x.template segment<DIM>(_a)-x.template segment<DIM>(_b);
  scalarD lnorm=DirectionalStrainLimitingQuadratic<DIM>::gradl(a2b,(fgrad || fhess) ? &dl : NULL,fhess ? &ddl : NULL)*_softmaxReg;
  scalarD delta0Exp=exp(-lnorm-_d0),delta1Exp=exp(lnorm+_d1);
  scalarD delta0=log(1+delta0Exp),delta1=log(1+delta1Exp),gradCoef=0;
  if(fgrad || fhess)
    gradCoef=(delta1*delta1Exp/(1+delta1Exp)-delta0*delta0Exp/(1+delta0Exp))*coef*_softmaxReg;
  if(fgrad) {
    grad=dl*gradCoef;
    Vec& fgradI=fgrad->getMatrixI();
    fgradI.template segment<DIM>(_a)+=grad;
    fgradI.template segment<DIM>(_b)-=grad;
  }
  if(fhess) {
    hess=ddl*gradCoef;
    hess+=(dl*dl.transpose())*(((delta1+delta1Exp)*delta1Exp/((1+delta1Exp)*(1+delta1Exp))+
                                (delta0+delta0Exp)*delta0Exp/((1+delta0Exp)*(1+delta0Exp)))*coef*_softmaxReg*_softmaxReg);
    addBlock(*fhess,_a,_a,hess);
    addBlock(*fhess,_b,_b,hess);
    addBlock(*fhess,_a,_b,-hess);
    addBlock(*fhess,_b,_a,-hess);
  }
  return (delta1*delta1+delta0*delta0)*coef/2;
}
template class DirectionalStrainLimitingSoftmax<2>;
template class DirectionalStrainLimitingSoftmax<3>;
//DirectionalStrainLimitingLog
template <int DIM>
DirectionalStrainLimitingLog<DIM>::DirectionalStrainLimitingLog():DirectionalStrainLimitingQuadratic<DIM>(typeid(DirectionalStrainLimitingLog<DIM>).name()) {}
template <int DIM>
DirectionalStrainLimitingLog<DIM>::DirectionalStrainLimitingLog(const Vec& x,sizeType a,sizeType b,const Vec2d& limit,scalarD coef):DirectionalStrainLimitingQuadratic<DIM>(typeid(DirectionalStrainLimitingLog<DIM>).name())
{
  _a=a*DIM;
  _b=b*DIM;
  VecDIMd a2b=x.template segment<DIM>(_a)-x.template segment<DIM>(_b);
  _d0=-a2b.norm()*limit[0];
  _d1=-a2b.norm()*limit[1];
  _coef=coef;
}
template <int DIM>
boost::shared_ptr<Serializable> DirectionalStrainLimitingLog<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new DirectionalStrainLimitingLog<DIM>());
}
template <int DIM>
scalarD DirectionalStrainLimitingLog<DIM>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  VecDIMd dl,grad;
  MatDIMd ddl,hess;
  VecDIMd a2b=x.template segment<DIM>(_a)-x.template segment<DIM>(_b);
  scalarD lnorm=DirectionalStrainLimitingQuadratic<DIM>::gradl(a2b,(fgrad || fhess) ? &dl : NULL,fhess ? &ddl : NULL),gradCoef=0;
  if(fgrad || fhess)
    gradCoef=-coef/(lnorm+_d0)-coef/(lnorm+_d1);
  if(fgrad) {
    grad=dl*gradCoef;
    Vec& fgradI=fgrad->getMatrixI();
    fgradI.template segment<DIM>(_a)+=grad;
    fgradI.template segment<DIM>(_b)-=grad;
  }
  if(fhess) {
    hess=ddl*gradCoef;
    hess+=dl*dl.transpose()*(coef/pow(lnorm+_d0,2)+coef/pow(lnorm+_d1,2));
    addBlock(*fhess,_a,_a,hess);
    addBlock(*fhess,_b,_b,hess);
    addBlock(*fhess,_a,_b,-hess);
    addBlock(*fhess,_b,_a,-hess);
  }
  return (-log(lnorm+_d0)-log(-lnorm-_d1))*coef;
}
template class DirectionalStrainLimitingLog<2>;
template class DirectionalStrainLimitingLog<3>;
