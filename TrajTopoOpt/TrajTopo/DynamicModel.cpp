#include "DynamicModel.h"
#include "LMCholmod.h"
#include "LMInterface.h"
#include "SpringModel.h"
#include "FEMModel.h"
#include "FEMModelLinear.h"
#include "GroundContact.h"
#include "AreaStrainLimiting.h"
#include "DirectionalStrainLimiting.h"
#include "Utils.h"
#include <boost/lexical_cast.hpp>

PRJ_BEGIN

//DynamicModel
template <int DIM>
void registerAll(IOData* dat)
{
  registerType<SpringModel<DIM> >(dat);
  registerType<FEMModel<DIM> >(dat);
  registerType<FEMModelLinear<DIM> >(dat);

  registerType<DirectionalStrainLimitingQuadratic<DIM> >(dat);
  registerType<DirectionalStrainLimitingCubic<DIM> >(dat);
  registerType<DirectionalStrainLimitingSoftmax<DIM> >(dat);
  registerType<DirectionalStrainLimitingLog<DIM> >(dat);

  registerType<AreaStrainLimitingQuadratic>(dat);
  registerType<AreaStrainLimitingCubic>(dat);
  registerType<AreaStrainLimitingSoftmax>(dat);
  registerType<AreaStrainLimitingLog>(dat);

  registerType<VolumeStrainLimitingQuadratic>(dat);
  registerType<VolumeStrainLimitingCubic>(dat);
  registerType<VolumeStrainLimitingSoftmax>(dat);
  registerType<VolumeStrainLimitingLog>(dat);

  registerType<GroundContactQuadratic<DIM> >(dat);
  registerType<GroundContactCubic<DIM> >(dat);
  registerType<GroundContactSoftmax<DIM> >(dat);
  registerType<GroundContactFix<DIM> >(dat);
}
template <int DIM>
DynamicModel<DIM>::DynamicModel():Serializable(typeid(DynamicModel).name())
{
#if defined(QUADMATH_SUPPORT) && defined(__GNUC__)
  _tolG=1E-15f;
#else
  _tolG=1E-9f;
#endif
  resetLM();
}
template <int DIM>
DynamicModel<DIM>::DynamicModel(const string& name):Serializable(name)
{
#if defined(QUADMATH_SUPPORT) && defined(__GNUC__)
  _tolG=1E-15f;
#else
  _tolG=1E-9f;
#endif
  resetLM();
}
template <int DIM>
void DynamicModel<DIM>::writeVTK(const string& path,const Vec* pData,const string& pDataName) const
{
  writeVTK(&_x,path,pData,pDataName);
}
template <int DIM>
void DynamicModel<DIM>::writeVTK(const Vec* x,const string& path,const Vec* pData,const string& pDataName) const
{
  VTKWriter<scalarD> os("model",path,true);
  //point
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss(x->size()/DIM,Vec3d::Zero());
  for(sizeType i=0; i<(sizeType)vss.size(); i++)
    vss[i].segment<DIM>(0)=x->segment<DIM>(i*DIM);
  os.appendPoints(vss.begin(),vss.end());
  //cell
  for(sizeType i=0; i<(sizeType)_pss.size(); i++)
    _pss[i]->writeVTK(os,_coefPss[i],pData,pDataName);
  //contact
  vector<scalarD> contact(x->size()/DIM,0);
  for(sizeType i=0; i<(sizeType)_contactPoints.size(); i++)
    contact[_contactPoints[i]]=1;
  os.appendCustomPointData("contact",contact.begin(),contact.end());
}
template <int DIM>
bool DynamicModel<DIM>::read(istream& is,IOData* dat)
{
  registerAll<DIM>(dat);
  readVector(_pss,is,dat);
  readVector(_stss,is,dat);
  readBinaryData(_tolG,is);
  readBinaryData(_dt,is);
  readBinaryData(_g,is,dat);
  readBinaryData(_f,is,dat);
  readBinaryData(_x0,is,dat);
  readBinaryData(_xN,is,dat);
  readBinaryData(_x,is,dat);
  readBinaryData(_xP,is,dat);
  readBinaryData(_coefPss,is,dat);
  readBinaryData(_contactPoints,is);
  _dirty=true;
  _dt=0;
  return is.good();
}
template <int DIM>
bool DynamicModel<DIM>::write(ostream& os,IOData* dat) const
{
  registerAll<DIM>(dat);
  writeVector(_pss,os,dat);
  writeVector(_stss,os,dat);
  writeBinaryData(_tolG,os);
  writeBinaryData(_dt,os);
  writeBinaryData(_g,os,dat);
  writeBinaryData(_f,os,dat);
  writeBinaryData(_x0,os,dat);
  writeBinaryData(_xN,os,dat);
  writeBinaryData(_x,os,dat);
  writeBinaryData(_xP,os,dat);
  writeBinaryData(_coefPss,os,dat);
  writeBinaryData(_contactPoints,os,dat);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> DynamicModel<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new DynamicModel<DIM>());
}
template <int DIM>
int DynamicModel<DIM>::operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient)
{
  FX=operator()(x,&DFDX,(STrips*)NULL);
  return 0;
}
template <int DIM>
int DynamicModel<DIM>::operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable)
{
  fvec.setZero(0);
  if(fjac)
    fjac->clear();
  return 0;
}
template <int DIM>
scalarD DynamicModel<DIM>::operator()(const Vec& x,Vec* fgrad,STrips* fhess)
{
  buildMass();
  return operatorThreadSafe(_dt,x,_x,_xN,_f,fgrad,fhess);
}
template <int DIM>
scalarD DynamicModel<DIM>::operatorThreadSafe(scalarD dt,const Vec& xP,const Vec& x,const Vec& xN,const Vec& f,Vec* fgrad,STrips* fhess) const
{
  ASSERT(!_dirty)
  SVecs tmpFGrads;
  if(fgrad) {
    fgrad->setZero(xP.size());
    if(omp_in_parallel())
      tmpFGrads=SVecs(1,Vec::Zero(xP.size()));
    else tmpFGrads=SVecs(Vec::Zero(xP.size()));
  }
  if(fhess)
    fhess->clear();
  scalarD E=0;
  E+=force(xP,f,fgrad);
  E+=kinetic(dt,xP,x,xN,fgrad,fhess);
  E+=potential(xP,fgrad ? &tmpFGrads : NULL,fhess);
  if(fgrad)
    *fgrad+=tmpFGrads.getMatrix();
  return E;
}
//dynamic system energy
template <int DIM>
scalarD DynamicModel<DIM>::force(const Vec& x,const Vec& f,Vec* fgrad) const
{
  Vec A=_M*_g+f;
  if(fgrad)
    fgrad->segment(0,A.size())-=A;
  return -x.segment(0,A.size()).dot(A);
}
template <int DIM>
scalarD DynamicModel<DIM>::potential(const Vec& x,SVecs* fgrad,STrips* fhess) const
{
  scalarD E=0;
  sizeType I=_x0.size();
  OMP_PARALLEL_FOR_I(OMP_ADD(E))
  for(sizeType i=0; i<(sizeType)_pss.size(); i++)
    E+=_pss[i]->operatorK(x,fgrad,fhess,_coefPss[i],I);
  OMP_PARALLEL_FOR_I(OMP_ADD(E))
  for(sizeType i=0; i<(sizeType)_stss.size(); i++)
    E+=_stss[i]->operatorK(x,fgrad,fhess,1,I);
  return E;
}
template <int DIM>
scalarD DynamicModel<DIM>::groundContact(const Vec& x,SVecs* fgrad,STrips* fhess) const
{
  scalarD E=0;
  sizeType I=_x0.size();
  OMP_PARALLEL_FOR_I(OMP_ADD(E))
  for(sizeType i=0; i<(sizeType)_stss.size(); i++)
    if(boost::dynamic_pointer_cast<GroundContactQuadratic<DIM> >(_stss[i]))
      E+=_stss[i]->operatorK(x,fgrad,fhess,1,I);
  return E;
}
template <int DIM>
scalarD DynamicModel<DIM>::strainLimiting(const Vec& x,SVecs* fgrad,STrips* fhess) const
{
  scalarD E=0;
  sizeType I=_x0.size();
  OMP_PARALLEL_FOR_I(OMP_ADD(E))
  for(sizeType i=0; i<(sizeType)_stss.size(); i++)
    if(boost::dynamic_pointer_cast<DirectionalStrainLimitingQuadratic<DIM> >(_stss[i]))
      E+=_stss[i]->operatorK(x,fgrad,fhess,1,I);
  return E;
}
template <int DIM>
scalarD DynamicModel<DIM>::kinetic(scalarD dt,const Vec& xP,const Vec& x,const Vec& xN,Vec* fgrad,STrips* fhess) const
{
  Vec A=Vec::Zero(_x0.size());
  A+=xP.segment(0,_x0.size());
  A-=x .segment(0,_x0.size())*2;
  A+=xN.segment(0,_x0.size());
  if(fgrad)
    fgrad->segment(0,_x0.size())+=(_M*A)/(dt*dt);
  if(fhess)
    addSparseBlock(*fhess,0,0,_M,1/(dt*dt));
  return A.dot(_M*A)/(2*dt*dt);
}
template <int DIM>
int DynamicModel<DIM>::inputs() const
{
  sizeType inputsAug=0;
  OMP_PARALLEL_FOR_I(OMP_ADD(inputsAug))
  for(sizeType i=0; i<(sizeType)_pss.size(); i++)
    inputsAug+=_pss[i]->inputsAug();
  return _x0.size()+inputsAug;
}
template <int DIM>
int DynamicModel<DIM>::values() const
{
  return 0;
}
template <int DIM>
void DynamicModel<DIM>::advance(scalarD dt,bool useCB,const Vec* xP,boost::shared_ptr<LMInterface::LMDenseInterface> interface)
{
  _dt=dt;
  _xP=xP ? *xP : _x;
  sizeType nrI=inputs();
  if(_xP.size() < nrI) {
    Vec aug=Vec::Zero(nrI-_x0.size());
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<(sizeType)_pss.size(); i++)
      _pss[i]->initAug(_xP,aug);
    _xP=concat(_xP.segment(0,_x0.size()),aug);
  }
  _lm->setTolG(_tolG);
  _lm->useInterface(interface);
  Callback<scalarD,Kernel<scalarD> > cb;
  NoCallback<scalarD,Kernel<scalarD> > ncb;
  sizeType succ=_lm->solveSparse(_xP,*this,useCB ? cb : ncb);
  if(!succ) {
    {
      boost::filesystem::ofstream os("./errorConfig.dat",ios::binary);
      boost::shared_ptr<IOData> dat=getIOData();
      write(os,dat.get());
      //write initial value
      if(xP)
        writeBinaryData(*xP,os);
      else writeBinaryData(_x,os);
      //write timestep
      writeBinaryData(_dt,os);
    }
    INFOV("LM failed (dt=%f,warmStart=%ld), save and exit!",_dt,sizeType(xP?1:0))
    exit(EXIT_FAILURE);
  }
}
template <int DIM>
void DynamicModel<DIM>::advances(scalarD t,scalarD dt,bool useCB,const string& path,boost::shared_ptr<LMInterface::LMDenseInterface> interface)
{
  scalarD time=0;
  recreate(path);
  for(sizeType i=0; time<t; i++) {
    INFOV("At time=%f",time)
    advance(dt,useCB,NULL,interface);
    _xN=_x;
    _x=_xP;
    writeVTK(path+"/frm"+boost::lexical_cast<string>(i)+".vtk");
    time+=dt;
  }
}
template <int DIM>
void DynamicModel<DIM>::debugGradient(scalarD scale)
{
#define DELTA 1E-15f
#define NR_TEST 1
  _dt=0.1f;
  SMat hess;
  Vec grad,grad2,delta;
  Vec x0=getXP().segment(0,_x0.size());
  x0=concat(x0,Vec::Random(inputs()-x0.size()));
  for(sizeType i=0; i<NR_TEST; i++) {
    getXN()=x0+(Vec::Random(x0.size())-Vec::Constant(x0.size(),0.5f))*scale;
    getX ()=x0+(Vec::Random(x0.size())-Vec::Constant(x0.size(),0.5f))*scale;
    getXP()=x0+(Vec::Random(x0.size())-Vec::Constant(x0.size(),0.5f))*scale;
    delta=(Vec::Random(x0.size())-Vec::Constant(x0.size(),0.5f));
    scalarD E1=Objective<scalarD>::operator()(getXP(),&grad,&hess);
    getXP()+=delta*DELTA;
    scalarD E2=Objective<scalarD>::operator()(getXP(),&grad2,(DMat*)NULL);
    INFOV("-------------------------------------------------------------E=%f",E1)
    INFOV("Gradient: %f Err: %f",grad.dot(delta),grad.dot(delta)-(E2-E1)/DELTA)
    INFOV("Hessian: %f Err: %f",(hess*delta).norm(),(hess*delta-(grad2-grad)/DELTA).norm())
  }
#undef NR_TEST
#undef DELTA
}
template <int DIM>
void DynamicModel<DIM>::debugErrorConfig()
{
  Vec init;
  {
    boost::filesystem::ifstream is("./errorConfig.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    read(is,dat.get());
    readBinaryData(init,is);
    readBinaryData(_dt,is);
  }
  writeVTK(&_xN,"./errorConfigXN.vtk");
  writeVTK(&_x,"./errorConfigX.vtk");
  advance(_dt,true,&init);
}
template <int DIM>
void DynamicModel<DIM>::buildMass()
{
  if(!_dirty)
    return;
  _tmp.clear();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)_pss.size(); i++)
    _pss[i]->operatorM(_tmp,_coefPss[i]);
  _M.resize(_x0.size(),_x0.size());
  _M.setFromTriplets(_tmp.begin(),_tmp.end());
  _dirty=false;
  _tmp.clear();
}
//setter/getter
template <int DIM>
void DynamicModel<DIM>::resetLM(bool useCholmod)
{
  _lm.reset(new LMInterface);
  _lm->setTolF(0);
  _lm->setTolX(0);
  _lm->setMaxIter(1E4);
  _lm->setTauMax(1E10f);
  _lm->setTau(1E-3f);
#ifdef CHOLMOD_SUPPORT
  if(useCholmod)
    _lm->useInterface(boost::shared_ptr<LMInterface::LMDenseInterface>(new CholmodWrapper()));
  else _lm->useInterface(NULL);
#endif
}
template <int DIM>
void DynamicModel<DIM>::resetF()
{
  _f.setZero(_g.size());
}
#define DEFINE_GETTER(TYPE,NAME,VAR) \
template <int DIM>  \
const TYPE& DynamicModel<DIM>::get##NAME() const {return VAR;}  \
template <int DIM>  \
TYPE& DynamicModel<DIM>::get##NAME() {return VAR;}
#define DEFINE_GETTERT(TYPE,NAME,VAR) \
template <int DIM>  \
const typename DynamicModel<DIM>::TYPE& DynamicModel<DIM>::get##NAME() const {return VAR;}  \
template <int DIM>  \
typename DynamicModel<DIM>::TYPE& DynamicModel<DIM>::get##NAME() {return VAR;}
DEFINE_GETTER(vector<boost::shared_ptr<ElasticModel<DIM> > >,Pss,_pss)
DEFINE_GETTER(vector<boost::shared_ptr<ElasticModel<DIM> > >,Stss,_stss)
DEFINE_GETTER(scalarD,TolG,_tolG)
DEFINE_GETTER(scalarD,Dt,_dt)
DEFINE_GETTERT(Vec,G,_g)
DEFINE_GETTERT(Vec,F,_f)
DEFINE_GETTERT(Vec,X0,_x0)
DEFINE_GETTERT(Vec,XN,_xN)
DEFINE_GETTERT(Vec,X,_x)
DEFINE_GETTERT(Vec,XP,_xP)
template <int DIM>
const typename DynamicModel<DIM>::SMat& DynamicModel<DIM>::getM() const
{
  const_cast<DynamicModel<DIM>&>(*this).buildMass();
  return _M;
}
template <int DIM>
typename DynamicModel<DIM>::SMat& DynamicModel<DIM>::getM()
{
  buildMass();
  return _M;
}
template <int DIM>
const typename DynamicModel<DIM>::Vec& DynamicModel<DIM>::getCoefPss() const
{
  return _coefPss;
}
template <int DIM>
typename DynamicModel<DIM>::Vec& DynamicModel<DIM>::getCoefPss()
{
  _dirty=true;
  return _coefPss;
}
DEFINE_GETTER(Coli,ContactPoints,_contactPoints)
#undef DEFINE_GETTER
#undef DEFINE_GETTERT
//transfer gradient
template <int DIM>
bool DynamicModel<DIM>::adjointTransferStage1ThreadSafe(scalarD dt,const Vec& xP,const Vec& x,const Vec& xN,const Vec& f,LMInterface::LMDenseInterface& interface) const
{
  STrips trips;
  if(omp_in_parallel())
    trips.clear(1);
  else trips.clear();
  operatorThreadSafe(dt,xP,x,xN,f,NULL,&trips);
  //build
  SMat hess;
  hess.resize(inputs(),inputs());
  hess.setFromTriplets(trips.begin(),trips.end());
  if(!interface.recompute(hess,0,false)) {
    WARNING("Factorization failed!")
    return false;
  } else return true;
}
template <int DIM>
void DynamicModel<DIM>::adjointTransferStage2ThreadSafe(scalarD dt,const Vec& xP,const Vec& x,const Vec& xN,const Vec& DEDXP,Vec* DEDX,Vec* DEDXN,Vec* DEDCoefP,Vec* DEDF,LMInterface::LMDenseInterface& interface) const
{
  Vec invHxPDEDXP=interface.solve(concat(DEDXP.segment(0,_x0.size()),Vec::Zero(inputs()-_x0.size())));
  //DEDX
  if(DEDX)
    *DEDX=(_M*invHxPDEDXP.segment(0,_x0.size()))*(2/(dt*dt));
  //DEDXN
  if(DEDXN)
    *DEDXN=-(_M*invHxPDEDXP.segment(0,_x0.size()))*(1/(dt*dt));
  //DEDCoefP
  if(DEDCoefP) {
    DEDCoefP->setZero(_coefPss.size());
    Vec b=Vec::Zero(_x0.size());
    b+=xP.segment(0,_x0.size());
    b-=x.segment(0,_x0.size())*2;
    b+=xN.segment(0,_x0.size());
    b/=dt*dt;
    b-=_g;
    sizeType I=_x0.size();
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<(sizeType)_pss.size(); i++)
      (*DEDCoefP)[i]=-_pss[i]->operatorMKDot(xP,b,invHxPDEDXP,I);
  }
  //DEDF
  if(DEDF)
    *DEDF=invHxPDEDXP.segment(0,_x0.size());
}
template <int DIM>
void DynamicModel<DIM>::adjointTransfer(const Vec& DEDXP,Vec* DEDX,Vec* DEDXN,Vec* DEDCoefP,Vec* DEDF,LMInterface::LMDenseInterface& interface) const
{
  adjointTransferStage1ThreadSafe(_dt,_xP,_x,_xN,_f,interface);
  adjointTransferStage2ThreadSafe(_dt,_xP,_x,_xN,DEDXP,DEDX,DEDXN,DEDCoefP,DEDF,interface);
}
template <int DIM>
void DynamicModel<DIM>::adjointTransfer(const Vec& DEDXP,Vec* DEDX,Vec* DEDXN,Vec* DEDCoefP,Vec* DEDF) const
{
  boost::shared_ptr<LMInterface::LMDenseInterface> interface=LMInterface::getInterface(false);
  adjointTransfer(DEDXP,DEDX,DEDXN,DEDCoefP,DEDF,*interface);
}
template <int DIM>
void DynamicModel<DIM>::debugAdjointTransfer(scalarD dt,scalarD scale)
{
#define DELTA 1E-7f
#define NR_TEST 1
  Vec xN=_xN;
  Vec x =_x;
  Vec xP=_xP;
  Vec DEDX=Vec::Zero(x.size());
  Vec DEDXN=Vec::Zero(x.size());
  Vec DEDK=Vec::Zero(_coefPss.size());
  Vec DEDF=Vec::Zero(x.size());
  Vec coef=Vec::Random(_x0.size()),delta,deltaK,tmp;
  getCoefPss()=Vec::Random(getCoefPss().size())+Vec::Ones(getCoefPss().size());
  bool useCB=false;
  for(sizeType i=0; i<NR_TEST; i++) {
    _xN=xN+Vec::Random(x.size())*scale;
    _x =x +Vec::Random(x.size())*scale;
    delta=Vec::Random(x.size());
    deltaK=Vec::Random(_coefPss.size());
    advance(dt,useCB,NULL);
    adjointTransfer(coef,&DEDX,&DEDXN,&DEDK,&DEDF);
    scalarD E1=getXP().segment(0,_x0.size()).dot(coef);
    xP=getXP();
    INFO("-------------------------------------------------------------")
    //xN
    tmp=getXN();
    getXN()+=delta*DELTA;
    advance(dt,useCB,&xP);
    scalarD E2=getXP().segment(0,_x0.size()).dot(coef);
    INFOV("DEDXN: %f Err: %f",DEDXN.dot(delta),DEDXN.dot(delta)-(E2-E1)/DELTA)
    getXN()=tmp;
    //x
    tmp=getX();
    getX()+=delta*DELTA;
    advance(dt,useCB,&xP);
    E2=getXP().segment(0,_x0.size()).dot(coef);
    INFOV("DEDX: %f Err: %f",DEDX.dot(delta),DEDX.dot(delta)-(E2-E1)/DELTA)
    getX()=tmp;
    //coefP
    tmp=getCoefPss();
    getCoefPss()+=deltaK*DELTA;
    advance(dt,useCB,&xP);
    E2=getXP().segment(0,_x0.size()).dot(coef);
    INFOV("DEDK: %f Err: %f",DEDK.dot(deltaK),DEDK.dot(deltaK)-(E2-E1)/DELTA)
    getCoefPss()=tmp;
    //f
    tmp=getF();
    getF()+=delta*DELTA;
    advance(dt,useCB,&xP);
    E2=getXP().segment(0,_x0.size()).dot(coef);
    INFOV("DEDF: %f Err: %f",DEDF.dot(delta),DEDF.dot(delta)-(E2-E1)/DELTA)
    getF()=tmp;
  }
#undef NR_TEST
#undef DELTA
}
template class DynamicModel<2>;
template class DynamicModel<3>;

PRJ_END
