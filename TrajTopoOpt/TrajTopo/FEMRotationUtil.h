#ifndef FEM_ROTATION_UTIL_H
#define FEM_ROTATOIN_UTIL_H

#include <CommonFile/MathBasic.h>
//#include <iostream>

PRJ_BEGIN

//basic
template <typename T>
static typename ScalarUtil<T>::ScalarMat3 cross(const typename ScalarUtil<T>::ScalarVec3& v)
{
  typename ScalarUtil<T>::ScalarMat3 ret;
  ret.setZero();
  ret(0,1)=-v[2];
  ret(0,2)=v[1];
  ret(1,2)=-v[0];
  ret(1,0)=v[2];
  ret(2,0)=-v[1];
  ret(2,1)=v[0];
  return ret;
}
template <typename T>
static typename Eigen::Matrix<T,9,9> rotationCoef(const typename ScalarUtil<T>::ScalarMat3& r)
{
  typename Eigen::Matrix<T,9,9> rCoef;
  rCoef.setZero();
#define GI(I,J) J*3+I
  for(sizeType i=0; i<3; i++)
    for(sizeType j=0; j<3; j++)
      for(sizeType k=0; k<3; k++)
        rCoef(GI(i,j),GI(k,j))=r(i,k);
  return rCoef;
#undef GI
}
template <typename T>
static typename ScalarUtil<T>::ScalarVec3 invCross(const typename ScalarUtil<T>::ScalarMat3& wCross)
{
  return typename ScalarUtil<T>::ScalarVec3(wCross(2,1),wCross(0,2),wCross(1,0));
}
template <typename T> //trace([w]*m) = w.dot(invCrossMatTrace(m))
static typename ScalarUtil<T>::ScalarVec3 invCrossMatTrace(const typename ScalarUtil<T>::ScalarMat3& m)
{
  typename ScalarUtil<T>::ScalarVec3 ret;
  ret[0]=m(1,2)-m(2,1);
  ret[1]=m(2,0)-m(0,2);
  ret[2]=m(0,1)-m(1,0);
  return ret;
}
template <typename T> //trace([wA]*m*[wB]) = wA.dot(invDoubleCrossMatTrace(m)*wB)
static typename ScalarUtil<T>::ScalarMat3 invDoubleCrossMatTrace(const typename ScalarUtil<T>::ScalarMat3& m)
{
  typename ScalarUtil<T>::ScalarMat3 ret=m.transpose();
  ret.diagonal().array()-=m.diagonal().sum();
  return ret;
}
//rotation along X,Y
Mat3d expWYZ(scalarD y,scalarD z,Vec3d* DRDY,Vec3d* DRDZ,Vec3d* DRDYDZ);
//rotation using rodriguez formulation
Mat3d expWGradInte(const Vec3d& w);
Vec3d invExpW(const Mat3d& R);
template <typename TDiff,typename TDDiff,typename TDiffRotT>
Mat3d expWGradV(const Vec3d& w,TDiff diffW[3],TDDiff ddiffW[3][3],const Vec3d* wDotT,TDiffRotT diffWDotT[3],Vec3d diffVOut[3]=NULL,Vec3d vOut[3][3]=NULL);
void debugExpWGrad(bool debugBasic=true,bool debugDiff=true,bool debugDiffDotT=true,bool debugInte=true,int maxIt=1000);
void debugInvExpW(int maxIt=1000);
//se/SE(3) operations
Mat4d exponentialSE3(const Vec6d& se3);
Vec6d logarithmSE3(const Mat4d& SE3);
void debugExpLogSE3(int maxIt=1000);
//rotation-strain transformation
template <typename TDiff,typename TDDiff,typename TDiffRotT>
Mat3d calcFGrad(const Mat3d& RS,TDiff diffRS[9],TDDiff ddiffRS[9][9],const Mat3d* RSDotT,TDiffRotT diffRSDotT[9]);
Mat3d calcFGradDU(const Mat3d& U);
Mat3d calcFGradDDU(const Mat3d& U);
Mat3d calcFGradDUDV(const Mat3d& U,const Mat3d& V);
template <typename FTYPE>
Mat3d calcFGrad(const FTYPE& F,Mat9d* diffRS=NULL);
void debugCalcFGrad(bool debugDiff=true,bool debugDDiff=true,bool debugDiffDotT=true,bool debugDUDV=true,int maxIt=1000);
//svd gradient
void adjustF3D(Mat3& F,Mat3& U,Mat3& V,Vec3& S);
void adjustF2D(Mat3& F,Mat3& U,Mat3& V,Vec3& S);
void derivSVD(Mat3& derivU,Vec3& derivD,Mat3& derivV,const Mat3& LHS,const Mat3& U,const Vec3& D,const Mat3& V);
void derivSVDDir(Mat3& derivU,Vec3& derivD,Mat3& derivV,const Mat3& LHS,const Mat3& U,const Vec3& D,const Mat3& V);
void derivRDF(Mat9d& DRDF,const Mat3& F,const Mat3& U,const Vec3& D,const Mat3& V);
void debugDerivSVD(bool debugSVD=true,bool debugRDF=true,int maxIt=1000);

PRJ_END

#endif
