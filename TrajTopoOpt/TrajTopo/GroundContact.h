#ifndef GROUND_CONTACT_H
#define GROUND_CONTACT_H

#include "ElasticModel.h"

PRJ_BEGIN

//Directional Strain Limiting
template <int DIM>
class GroundContactQuadratic : public ElasticModel<DIM>
{
public:
  using typename Objective<scalarD>::Vec;
  using typename Objective<scalarD>::STrips;
  using typename ElasticModel<DIM>::VecDIMd;
  using typename ElasticModel<DIM>::SVecs;
  GroundContactQuadratic();
  GroundContactQuadratic(const string& name);
  GroundContactQuadratic(const Vec& x,sizeType a,const VecDIMd& g,scalarD coef);
  virtual void writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const override;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual Coli ids() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
  virtual int inputs() const override;
protected:
  VecDIMd _g;
  scalarD _g0,_coef;
  sizeType _a;
};
template <int DIM>
class GroundContactCubic : public GroundContactQuadratic<DIM>
{
public:
  using typename GroundContactQuadratic<DIM>::Vec;
  using typename GroundContactQuadratic<DIM>::STrips;
  using typename GroundContactQuadratic<DIM>::VecDIMd;
  using typename GroundContactQuadratic<DIM>::SVecs;
  using NAMESPACE::GroundContactQuadratic<DIM>::_g;
  using NAMESPACE::GroundContactQuadratic<DIM>::_g0;
  using NAMESPACE::GroundContactQuadratic<DIM>::_coef;
  using NAMESPACE::GroundContactQuadratic<DIM>::_a;
  GroundContactCubic();
  GroundContactCubic(const Vec& x,sizeType a,const VecDIMd& g,scalarD coef);
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
};
template <int DIM>
class GroundContactSoftmax : public GroundContactQuadratic<DIM>
{
public:
  using typename GroundContactQuadratic<DIM>::Vec;
  using typename GroundContactQuadratic<DIM>::STrips;
  using typename GroundContactQuadratic<DIM>::VecDIMd;
  using typename GroundContactQuadratic<DIM>::SVecs;
  using NAMESPACE::GroundContactQuadratic<DIM>::_g;
  using NAMESPACE::GroundContactQuadratic<DIM>::_g0;
  using NAMESPACE::GroundContactQuadratic<DIM>::_coef;
  using NAMESPACE::GroundContactQuadratic<DIM>::_a;
  GroundContactSoftmax();
  GroundContactSoftmax(const Vec& x,sizeType a,const VecDIMd& g,scalarD coef,scalarD typicalValue=0.5f);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
protected:
  scalarD _softmaxReg;
};
template <int DIM>
class GroundContactFix : public GroundContactQuadratic<DIM>
{
public:
  using typename GroundContactQuadratic<DIM>::Vec;
  using typename GroundContactQuadratic<DIM>::STrips;
  using typename GroundContactQuadratic<DIM>::VecDIMd;
  using typename GroundContactQuadratic<DIM>::SVecs;
  using NAMESPACE::GroundContactQuadratic<DIM>::_g;
  using NAMESPACE::GroundContactQuadratic<DIM>::_g0;
  using NAMESPACE::GroundContactQuadratic<DIM>::_coef;
  using NAMESPACE::GroundContactQuadratic<DIM>::_a;
  GroundContactFix();
  GroundContactFix(const Vec& x,sizeType a,const VecDIMd& g,scalarD coef);
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
};

PRJ_END

#endif
