#ifndef FEM_MODEL_LINEAR_H
#define FEM_MODEL_LINEAR_H

#include "FEMModel.h"

PRJ_BEGIN

//SpringModel
template <int DIM>
class FEMModelLinear : public FEMModel<DIM>
{
public:
  using typename FEMModel<DIM>::Vec;
  using typename FEMModel<DIM>::STrips;
  using typename FEMModel<DIM>::VecDIMd;
  using typename FEMModel<DIM>::MatDIMd;
  using typename FEMModel<DIM>::SVecs;
  using typename FEMModel<DIM>::ID;
  using typename FEMModel<DIM>::VecDIMAlld;
  using typename FEMModel<DIM>::MatDIMAlld;
  FEMModelLinear();
  FEMModelLinear(const Vec& x,scalarD poisson,scalarD young,scalarD coefM,
           sizeType x0,sizeType x1,sizeType x2,sizeType x3,sizeType id);
  FEMModelLinear(const Vec& x,scalarD poisson,scalarD young,scalarD coefM,
           sizeType x0,sizeType x1,sizeType x2,sizeType x3,
           sizeType x4,sizeType x5,sizeType x6,sizeType x7,sizeType id);
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual int inputsAug() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
  virtual scalarD operatorMKDot(const Vec& xP,const Vec& b,const Vec& other,sizeType offAug) const override;
  virtual bool isQuadratic() const override;
};

PRJ_END

#endif
