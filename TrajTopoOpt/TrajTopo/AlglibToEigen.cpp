#include "AlglibToEigen.h"
#include "OPTInterface.h"
#include <CommonFile/IO.h>

PRJ_BEGIN

//helper
Cold fromAlglib(const alglib::real_1d_array& xAlglib)
{
  return Eigen::Map<const Eigen::VectorXd>(xAlglib.getcontent(),xAlglib.length()).cast<scalarD>();
}
void toAlglib(alglib::real_1d_array& ret,const Cold& x)
{
  if(ret.length() != x.size())
    ret.setlength(x.size());
  for(sizeType i=0; i<ret.length(); i++)
    ret[i]=x[i];
}
alglib::real_1d_array toAlglib(const Cold& x)
{
  alglib::real_1d_array ret;
  toAlglib(ret,x);
  return ret;
}
void toAlglib(alglib::real_2d_array& ret,const Matd& x)
{
  if(ret.rows() != x.rows() || ret.cols() != x.cols())
    ret.setlength(x.rows(),x.cols());
  //Matd xt=x.transpose();
  for(sizeType i=0; i<ret.rows(); i++)
    for(sizeType j=0; j<ret.cols(); j++)
      ret(i,j)=x(i,j);
}
alglib::real_2d_array toAlglib(const Matd& x)
{
  alglib::real_2d_array ret;
  toAlglib(ret,x);
  return ret;
}
void writeVTK(const Cold& x,const string& path)
{
  VTKWriter<scalarD> os("QPSolution",path,true);
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
  for(sizeType i=0; i<x.size(); i++)
    vss.push_back(Vec3d((scalarD)i/(scalarD)x.size(),x[i],0));
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,0,1),
                 VTKWriter<scalarD>::IteratorIndex<Vec3i>(x.size()-1,0,1),
                 VTKWriter<scalarD>::LINE);
}
void alglibGrad(const alglib::real_1d_array& x,double& func,alglib::real_1d_array& grad,void* ptr)
{
  BLEICInterface* bleic=(BLEICInterface*)ptr;
  Eigen::Map<const Eigen::VectorXd> xMap(x.getcontent(),x.length());
  Eigen::Map<Eigen::VectorXd> gradMap(grad.getcontent(),x.length());

  scalarD funcTmp;
  Cold gradTmp=Cold::Zero(x.length());
  bleic->gradient(xMap.cast<scalarD>(),funcTmp,gradTmp);
  gradMap=gradTmp.cast<double>();
  func=funcTmp;
}
void alglibRep(const alglib::real_1d_array& x,double func,void* ptr)
{
  scalarD funcTmp=func;
  BLEICInterface* bleic=(BLEICInterface*)ptr;
  Eigen::Map<const Eigen::VectorXd> xMap(x.getcontent(),x.length());
  bleic->report(xMap.cast<scalarD>(),funcTmp);
}

PRJ_END
