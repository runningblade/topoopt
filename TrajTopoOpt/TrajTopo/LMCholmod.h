#ifndef LM_CHOLMOD_H
#define LM_CHOLMOD_H
#ifdef CHOLMOD_SUPPORT

#include "LMInterface.h"
#include "cholmod.h"

PRJ_BEGIN

class CholmodWrapper : public LMInterface::LMDenseInterface
{
public:
  typedef LMInterface::SMat SMat;
  CholmodWrapper();
  virtual ~CholmodWrapper();
  virtual boost::shared_ptr<LMInterface::LMDenseInterface> copy() const override;
  bool recompute(SMat& smat,scalarD shift,bool sameA) override;
  Matd solve(const Matd& b) override;
  void setSupernodal(bool super);
  void setSameStruct(bool same);
  void setUseEigen(bool useEigen);
  void debug();
private:
  void copyData(const SMat& smat);
  bool tryFactorize(SMat& smat,scalarD shift);
  void clear();
  Eigen::SimplicialCholesky<SMat> _eigenSol;
  cholmod_factor* _L;
  cholmod_sparse* _A;
  cholmod_common _c;
  Matd _ret;
  bool _built,_same,_useEigen,_supernodal;
};

PRJ_END

#endif
#endif
