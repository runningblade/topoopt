#include "GroundContact.h"
#include "Utils.h"

USE_PRJ_NAMESPACE

//GroundContactQuadratic
template <int DIM>
GroundContactQuadratic<DIM>::GroundContactQuadratic():ElasticModel<DIM>(typeid(GroundContactQuadratic<DIM>).name(),-1) {}
template <int DIM>
GroundContactQuadratic<DIM>::GroundContactQuadratic(const string& name):ElasticModel<DIM>(name,-1) {}
template <int DIM>
GroundContactQuadratic<DIM>::GroundContactQuadratic(const Vec& x,sizeType a,const VecDIMd& g,scalarD coef):ElasticModel<DIM>(typeid(GroundContactQuadratic<DIM>).name(),-1)
{
  _g=g.normalized();
  _a=a*DIM;
  _coef=coef;
  _g0=-x.template segment<DIM>(_a).dot(_g);
}
template <int DIM>
void GroundContactQuadratic<DIM>::writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const
{
  FUNCTION_NOT_IMPLEMENTED
}
template <int DIM>
bool GroundContactQuadratic<DIM>::read(istream& is,IOData* dat)
{
  ElasticModel<DIM>::read(is,dat);
  readBinaryData(_g,is);
  readBinaryData(_coef,is);
  readBinaryData(_g0,is);
  readBinaryData(_a,is);
  return is.good();
}
template <int DIM>
bool GroundContactQuadratic<DIM>::write(ostream& os,IOData* dat) const
{
  ElasticModel<DIM>::write(os,dat);
  writeBinaryData(_g,os);
  writeBinaryData(_coef,os);
  writeBinaryData(_g0,os);
  writeBinaryData(_a,os);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> GroundContactQuadratic<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new GroundContactQuadratic<DIM>());
}
template <int DIM>
Coli GroundContactQuadratic<DIM>::ids() const
{
  return Coli::Constant(1,_a/DIM);
}
template <int DIM>
scalarD GroundContactQuadratic<DIM>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  scalarD prj=max<scalarD>(x.template segment<DIM>(_a).dot(_g)+_g0,0);
  if(fgrad)
    fgrad->getMatrixI().template segment<DIM>(_a)+=_g*(prj*coef);
  if(fhess && prj != 0)
    addBlock(*fhess,_a,_a,_g*_g.transpose()*coef);
  return prj*prj*coef/2;
}
template <int DIM>
int GroundContactQuadratic<DIM>::inputs() const
{
  return 2*DIM;
}
template class GroundContactQuadratic<2>;
template class GroundContactQuadratic<3>;
//GroundContactCubic
template <int DIM>
GroundContactCubic<DIM>::GroundContactCubic():GroundContactQuadratic<DIM>(typeid(GroundContactCubic<DIM>).name()) {}
template <int DIM>
GroundContactCubic<DIM>::GroundContactCubic(const Vec& x,sizeType a,const VecDIMd& g,scalarD coef):GroundContactQuadratic<DIM>(typeid(GroundContactCubic<DIM>).name())
{
  _g=g.normalized();
  _a=a*DIM;
  _coef=coef;
  _g0=-x.template segment<DIM>(_a).dot(_g);
}
template <int DIM>
boost::shared_ptr<Serializable> GroundContactCubic<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new GroundContactCubic<DIM>());
}
template <int DIM>
scalarD GroundContactCubic<DIM>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  scalarD prj=max<scalarD>(x.template segment<DIM>(_a).dot(_g)+_g0,0);
  if(fgrad)
    fgrad->getMatrixI().template segment<DIM>(_a)+=_g*(prj*prj*coef);
  if(fhess)
    addBlock(*fhess,_a,_a,_g*_g.transpose()*(2*prj*coef));
  return prj*prj*prj*coef/3;
}
template class GroundContactCubic<2>;
template class GroundContactCubic<3>;
//GroundContactSoftmax
template <int DIM>
GroundContactSoftmax<DIM>::GroundContactSoftmax():GroundContactQuadratic<DIM>(typeid(GroundContactSoftmax<DIM>).name()) {}
template <int DIM>
GroundContactSoftmax<DIM>::GroundContactSoftmax(const Vec& x,sizeType a,const VecDIMd& g,scalarD coef,scalarD typicalValue):GroundContactQuadratic<DIM>(typeid(GroundContactSoftmax<DIM>).name())
{
  _g=g.normalized();
  _a=a*DIM;
  _g0=-x.template segment<DIM>(_a).dot(_g);
  _softmaxReg=10/typicalValue;
  _coef=coef/_softmaxReg/_softmaxReg;
}
template <int DIM>
bool GroundContactSoftmax<DIM>::read(istream& is,IOData* dat)
{
  GroundContactQuadratic<DIM>::read(is,dat);
  readBinaryData(_softmaxReg,is);
  return is.good();
}
template <int DIM>
bool GroundContactSoftmax<DIM>::write(ostream& os,IOData* dat) const
{
  GroundContactQuadratic<DIM>::write(os,dat);
  writeBinaryData(_softmaxReg,os);
  return os.good();
}
template <int DIM>
boost::shared_ptr<Serializable> GroundContactSoftmax<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new GroundContactSoftmax<DIM>());
}
template <int DIM>
scalarD GroundContactSoftmax<DIM>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  scalarD depth=x.template segment<DIM>(_a).dot(_g)+_g0;
  scalarD prjExp=exp(depth*_softmaxReg),prj=log(1+prjExp);
  if(fgrad)
    fgrad->getMatrixI().template segment<DIM>(_a)+=_g*(prjExp*prj*coef*_softmaxReg/(1+prjExp));
  if(fhess)
    addBlock(*fhess,_a,_a,_g*_g.transpose()*((prjExp+prj)*prjExp*coef*_softmaxReg*_softmaxReg/((1+prjExp)*(1+prjExp))));
  return prj*prj*coef/2;
}
template class GroundContactSoftmax<2>;
template class GroundContactSoftmax<3>;
//GroundContactFix
template <int DIM>
GroundContactFix<DIM>::GroundContactFix():GroundContactQuadratic<DIM>(typeid(GroundContactFix<DIM>).name()) {}
template <int DIM>
GroundContactFix<DIM>::GroundContactFix(const Vec& x,sizeType a,const VecDIMd& g,scalarD coef):GroundContactQuadratic<DIM>(typeid(GroundContactFix<DIM>).name())
{
  _a=a*DIM;
  _coef=coef;
  _g=x.template segment<DIM>(_a);
}
template <int DIM>
boost::shared_ptr<Serializable> GroundContactFix<DIM>::copy() const
{
  return boost::shared_ptr<Serializable>(new GroundContactFix<DIM>());
}
template <int DIM>
scalarD GroundContactFix<DIM>::operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const
{
  coef*=_coef;
  VecDIMd prj=x.template segment<DIM>(_a)-_g;
  if(fgrad)
    fgrad->getMatrixI().template segment<DIM>(_a)+=prj*coef;
  if(fhess)
    addIK(*fhess,_a,_a,coef,DIM);
  return prj.squaredNorm()*coef/2;
}
template class GroundContactFix<2>;
template class GroundContactFix<3>;
