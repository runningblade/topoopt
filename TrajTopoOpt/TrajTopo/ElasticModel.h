#ifndef ELASTIC_MODEL_H
#define ELASTIC_MODEL_H

#include <CommonFile/IO.h>
#include <CommonFile/solvers/Objective.h>

PRJ_BEGIN

//ElasticModel
template <int DIM>
class ElasticModel : public Objective<scalarD>, public Serializable
{
public:
  typedef typename Eigen::Matrix<sizeType,DIM,1> VecDIMi;
  typedef typename Eigen::Matrix<scalarD,DIM,1> VecDIMd;
  typedef typename Eigen::Matrix<scalarD,DIM,DIM> MatDIMd;
  typedef ParallelMatrix<Vec> SVecs;
  ElasticModel(const string& name);
  ElasticModel(const string& name,sizeType id);
  virtual ~ElasticModel();
  virtual void writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const=0;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual int inputsAug() const;
  virtual void initAug(const Vec& x,Vec& aug) const;
  virtual Coli ids() const=0;
  //operatorK
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,DMat* fhess,scalarD coef,sizeType offAug);
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,SMat* fhess,scalarD coef,sizeType offAug);
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const=0;
  virtual scalarD operatorMKDot(const Vec& xP,const Vec& b,const Vec& other,sizeType offAug) const;
  //operatorM
  virtual void operatorM(DMat& fhess,scalarD coef);
  virtual void operatorM(SMat& fhess,scalarD coef);
  virtual void operatorM(STrips& fhess,scalarD coef) const;
  virtual scalarD kineticBruteForce(const Vec& x) const;
  virtual bool isQuadratic() const;
  void debugGradient(scalar eps=1E-6f);
protected:
  sizeType _pid;
};

PRJ_END

#endif
