#ifndef ALGLIB_TO_EIGEN_H
#define ALGLIB_TO_EIGEN_H

#include <CommonFile/MathBasic.h>
#include <CommonFile/solvers/alglib/ap.h>

PRJ_BEGIN

//helper
Cold fromAlglib(const alglib::real_1d_array& xAlglib);
void toAlglib(alglib::real_1d_array& ret,const Cold& x);
alglib::real_1d_array toAlglib(const Cold& x);
void toAlglib(alglib::real_2d_array& ret,const Matd& x);
alglib::real_2d_array toAlglib(const Matd& x);
void writeVTK(const Cold& x,const string& path);
void alglibGrad(const alglib::real_1d_array& x,double& func,alglib::real_1d_array& grad,void* ptr);
void alglibRep(const alglib::real_1d_array& x,double func,void* ptr);

PRJ_END

#endif
