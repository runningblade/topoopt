#ifndef FEM_MODEL_H
#define FEM_MODEL_H

#include "ElasticModel.h"

PRJ_BEGIN

//SpringModel
template <int DIM>
class FEMModel : public ElasticModel<DIM>
{
public:
  static const int NRP=1<<DIM;
  using typename Objective<scalarD>::Vec;
  using typename Objective<scalarD>::STrips;
  using typename ElasticModel<DIM>::VecDIMd;
  using typename ElasticModel<DIM>::MatDIMd;
  using typename ElasticModel<DIM>::SVecs;
  typedef typename Eigen::Matrix<sizeType,NRP,1> ID;
  typedef typename Eigen::Matrix<scalarD,NRP*DIM,1> VecDIMAlld;
  typedef typename Eigen::Matrix<scalarD,NRP*DIM,NRP*DIM> MatDIMAlld;
  FEMModel();
  FEMModel(const string& name);
  FEMModel(const string& name,sizeType id);
  FEMModel(const Vec& x,scalarD poisson,scalarD young,scalarD coefM,
           sizeType x0,sizeType x1,sizeType x2,sizeType x3,sizeType id);
  FEMModel(const Vec& x,scalarD poisson,scalarD young,scalarD coefM,
           sizeType x0,sizeType x1,sizeType x2,sizeType x3,
           sizeType x4,sizeType x5,sizeType x6,sizeType x7,sizeType id);
  virtual void writeVTK(VTKWriter<scalarD>& os,scalarD coef,const Vec* pData,const string& pDataName) const override;
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  virtual int inputsAug() const override;
  virtual void initAug(const Vec& x,Vec& aug) const override;
  virtual Coli ids() const override;
  virtual scalarD operatorK(const Vec& x,SVecs* fgrad,STrips* fhess,scalarD coef,sizeType offAug) const override;
  virtual scalarD operatorMKDot(const Vec& xP,const Vec& b,const Vec& other,sizeType offAug) const override;
  virtual void operatorM(STrips& fhess,scalarD coef) const override;
  virtual scalarD kineticBruteForce(const Vec& x) const override;
  virtual int inputs() const override;
protected:
  virtual void calcH(scalarD poisson,scalarD young);
  virtual Mat3d calcR(const Vec& x) const;
  static const Matd _M,_MDIM;
  MatDIMAlld _H;
  VecDIMAlld _X0;
  scalarD _coefM;
  VecDIMd _l;
  ID _id;
};

PRJ_END

#endif
