#include "OPTInterface.h"
#include "AlglibToEigen.h"
#include "Utils.h"
#include <CommonFile/IO.h>
#include <CommonFile/solvers/Minimizer.h>
#include <CommonFile/solvers/alglib/optimization.h>

USE_PRJ_NAMESPACE

//common interface
CommonInterface::CommonInterface()
{
  _tolg=1E-9f;
  _tolf=1E-9f;
  _tolx=1E-9f;
  _maxIter=1E8;
  _sz=-1;
  _hasCI=false;
}
void CommonInterface::setCI(const Matd& CI,const Cold& CI0,sizeType type)
{
  Coli types=Coli::Constant(CI0.size(),type);
  setCI(CI,CI0,types);
}
void CommonInterface::setCLB(const Cold& L)
{
  Cold U=Cold::Constant(L.size(), ScalarUtil<scalarD>::scalar_inf);
  setCB(L,U);
}
void CommonInterface::setCUB(const Cold& U)
{
  Cold L=Cold::Constant(U.size(),-ScalarUtil<scalarD>::scalar_inf);
  setCB(L,U);
}
void CommonInterface::setTolG(scalarD tolg)
{
  _tolg=tolg;
  reset(_sz,_hasCI);
}
void CommonInterface::setTolF(scalarD tolf)
{
  _tolf=tolf;
  reset(_sz,_hasCI);
}
void CommonInterface::setTolX(scalarD tolx)
{
  _tolx=tolx;
  reset(_sz,_hasCI);
}
void CommonInterface::setMaxIter(sizeType maxIter)
{
  _maxIter=maxIter;
  reset(_sz,_hasCI);
}
//minbleic interface
BLEICInterface::BLEICInterface() {}
void BLEICInterface::reset(sizeType sz,bool hasCI)
{
  if(sz <= 0) {
    _sz=sz;
    _state=NULL;
    return;
  }
  _sz=sz;
  _state.reset(new alglib::minbleicstate);
  alglib::real_1d_array x=toAlglib((Cold)Cold::Zero(sz));
  alglib::minbleiccreate(x,*_state);
  alglib::minbleicsetcond(*_state,_tolg,_tolf,_tolx,_maxIter);
}
void BLEICInterface::setCI(const Matd& CI,const Cold& CI0,const Coli& type)
{
  ASSERT(CI.rows() == CI0.size() && CI.cols() == _sz);
  //ASSERT_MSG(_hasCI,"QuickQP does not support linear constraints!")
  Matd CII0=Matd::Zero(CI.rows(),CI.cols()+1);
  CII0.block(0,0,CI.rows(),CI.cols())=CI;
  CII0.col(CI.cols())=-CI0;

  alglib::integer_1d_array typeAlglib;
  typeAlglib.setcontent(type.size(),type.cast<alglib::ae_int_t>().eval().data());
  alglib::minbleicsetlc(*_state,toAlglib(CII0),typeAlglib);
}
void BLEICInterface::setCB(const Cold& L,const Cold& U)
{
  ASSERT(L.size() == _sz && U.size() == _sz)
  alglib::minbleicsetbc(*_state,toAlglib(L),toAlglib(U));
}
void BLEICInterface::report(const Cold& x,scalarD& FX)
{
  INFOV("BLEIC funcVal: %f",FX)
}
void BLEICInterface::gradient(const Cold& x,scalarD& FX,Cold& DFDX)
{
  Eigen::Map<const Cold> xMap(x.data(),x.size());
  Eigen::Map<Cold> DFDXMap(DFDX.data(),DFDX.size());
  operator()(xMap,FX,DFDXMap);
}
bool BLEICInterface::solveCommonFile(Cold& x,Callback<scalarD,Kernel<scalarD> >& cb)
{
  scalarD fx;
  LBFGSMinimizer<scalarD> sol;
  sol.delta()=_tolf;
  sol.epsilon()=_tolg;
  sol.maxIterations()=_maxIter;
  _rep.reset(new alglib::minbleicreport);
  _rep->terminationtype=(alglib::ae_int_t)sol.minimize(x,fx,*this,cb);
  _rep->iterationscount=sol.maxIterations();
  return _rep->terminationtype >= 0;
}
bool BLEICInterface::solve(Cold& x)
{
  ASSERT_MSG(x.size() == _sz && _state,"Setup solver first!")
  alglib::real_1d_array xAlglib;
  alglib::minbleicrestartfrom(*_state,toAlglib(x));
  alglib::minbleicoptimize(*_state,alglibGrad,alglibRep,this);
  _rep.reset(new alglib::minbleicreport);
  alglib::minbleicresults(*_state,xAlglib,*_rep);
  x=fromAlglib(xAlglib);
  //INFOV("Termination type: %ld",_rep->terminationtype)
  return _rep->terminationtype <= 7 && _rep->terminationtype >= 1;
}
sizeType BLEICInterface::getTermination() const
{
  return _rep->terminationtype;
}
sizeType BLEICInterface::getIterationsCount() const
{
  return _rep->iterationscount;
}
scalarD BLEICInterface::getFunctionValue() const
{
  return _state->f;
}
void BLEICInterface::debugGradient(Cold x,const Cold& mask)
{
#define NR_TEST 10
#define DELTA 1E-9f
  scalarD fx,fx2;
  Cold dfdx=Cold::Zero(x.size()),dfdx2=dfdx;
  x.array()*=mask.array();
  gradient(x,fx,dfdx);
  for(sizeType i=0; i<NR_TEST; i++) {
    Cold delta=Cold::Random(x.size());
    delta.array()*=mask.array();
    gradient(x+delta*DELTA,fx2,dfdx2);
    INFOV("Gradient: %f, Err: %f",dfdx.dot(delta),(dfdx.dot(delta)-(fx2-fx)/DELTA))
  }
#undef DELTA
#undef NR_TEST
}
int BLEICInterface::operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient)
{
  Eigen::Map<const Cold> xMap(x.data(),x.size());
  Eigen::Map<Cold> DFDXMap(DFDX.data(),DFDX.size());
  operator()(xMap,FX,DFDXMap);
  return 0;
}
int BLEICInterface::inputs() const
{
  return _sz;
}
