#ifndef LM_INTERFACE_H
#define LM_INTERFACE_H

#include "OPTInterface.h"

PRJ_BEGIN

class LMInterface : public CommonInterface
{
public:
  typedef Objective<scalarD>::SMat SMat;
  typedef Eigen::DiagonalMatrix<scalarD,-1,-1> DMat;
  class LMDenseInterface
  {
  public:
    virtual ~LMDenseInterface() {}
    virtual boost::shared_ptr<LMDenseInterface> copy() const=0;
    virtual bool recompute(SMat& smat,scalarD shift,bool sameA)=0;
    virtual Matd solve(const Matd& b)=0;
  };
  LMInterface();
  virtual void reset(sizeType sz,bool hasCI);
  virtual void setCI(const Matd& CI,const Cold& CI0,const Coli& type);
  virtual void setCB(const Cold& L,const Cold& U);
  virtual void useCache(bool useCache);
  virtual void useWoodbury(bool useWoodbury);
  virtual void useInterface(boost::shared_ptr<LMDenseInterface> sol);
  static boost::shared_ptr<LMDenseInterface> getInterface(bool sym);
  virtual void setKrylov(scalar krylov);
  virtual bool solve(Cold& x);
  bool solveSparse(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb);
  bool solveDense(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb);
  void writeMuHistory(const string& path) const;
  void setTridiag(sizeType tridiag);
  //debug quadratic LM example
  void debugLMExampleQuad();
  //debug general nonlinear LM example
  void debugLMExampleNon();
  //tridiagonal solver functionality
  static void debugTridiag();
  //debug constraints
  static void debugConstraint();
  template <typename MAT>
  static Matd invTridiag(const MAT& LHS,scalarD mu,sizeType tridiag);
  template <typename MAT,typename MATO,typename MATR>
  static MATR solveTridiag(const MAT& LHS,const MATO& RHS,scalarD mu,sizeType tridiag);
  static void checkTridiag(const Matd& LHS,sizeType tridiag);
  Cold solveSys(const SMat& LHS,const Cold& RHS,scalarD mu,const SMat* J,bool sameA,bool& succ) const;
  Cold solveSys(const Matd& LHS,const Cold& RHS,scalarD mu,const Matd* J,bool sameA,bool& succ) const;
  template <typename MAT>
  bool solveInternal(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb);
  Cold enforceConsZero(const Cold& p) const;
  Cold enforceCons(const Cold& p) const;
  void setTauMax(scalarD tauMax);
  void setTau(scalarD tau);
  scalarD tau() const;
  scalarD tauMax() const;
private:
  //data
  vector<scalarD> _muHistory;
  sizeType _iter,_tridiag;
  scalarD _tau,_tauMax,_krylov;
  bool _useCache;
  bool _useWoodbury;
private:
  boost::shared_ptr<Matd> _CI;
  boost::shared_ptr<Cold> _CI0;
  Eigen::FullPivLU<Matd> _invCICIT;
  boost::shared_ptr<LMDenseInterface> _dense;
};

PRJ_END

#endif
