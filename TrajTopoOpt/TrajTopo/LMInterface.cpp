#include "LMInterface.h"
#include "Utils.h"
#include <CommonFile/solvers/PMinresQLP.h>
#include <CommonFile/Timing.h>
#include <CommonFile/IO.h>

PRJ_BEGIN

//quadratic
class Quadratic : public Objective<scalarD>
{
public:
  Quadratic(scalarD NX,scalarD NF) {
    _f.setRandom(NF);
    _J.setRandom(NF,NX);
    _g.setRandom(NX);
    _H.setRandom(NX,NX);
    _H=(_H.transpose()*_H).eval();
  }
  virtual int operator()(const Vec& x,Vec& fvec,Matd* fjac,bool modifiable) {
    fvec=_f+_J*x;
    if(fjac)
      *fjac=_J;
    return 0;
  }
  virtual scalarD operator()(const Vec& x,Vec* fgrad,Matd* fhess) {
    if(fgrad)
      *fgrad=_H*x+_g;
    if(fhess)
      *fhess=_H;
    return x.dot(_H*x/2+_g);
  }
  virtual int values() const {
    return _J.rows();
  }
  virtual int inputs() const {
    return _J.cols();
  }
private:
  Matd _J,_H;
  Vec _f,_g;
};
//mass spring
class MassSpring : public Objective<scalarD>
{
public:
  MassSpring(sizeType np,scalarD l):_np(np),_l(l) {}
  void writeVTK(const Vec& x,const string& path) const {
    vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
    vector<Vec3i,Eigen::aligned_allocator<Vec3i> > iss;
    for(sizeType c=0; c<_np; c++)
      for(sizeType r=0; r<_np; r++) {
        vss.push_back(Vec3d(x[GI(r,c)*2+0],x[GI(r,c)*2+1],0));
        if(r<_np-1)
          iss.push_back(Vec3i(GI(r,c),GI(r+1,c),0));
        if(c<_np-1)
          iss.push_back(Vec3i(GI(r,c),GI(r,c+1),0));
      }
    VTKWriter<scalarD> os("grid",path,true);
    os.appendPoints(vss.begin(),vss.end());
    os.appendCells(iss.begin(),iss.end(),VTKWriter<scalarD>::LINE);
  }
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable) {
    fvec.setZero(values());
    //spring
    sizeType off=0;
    for(sizeType r=0; r<_np-1; r++)
      for(sizeType c=0; c<_np; c++) {
        Vec2d dir=x.segment(GI(r,c)*2,2)-x.segment(GI(r+1,c)*2,2);
        if(fjac)addBlock(*fjac,off,GI(r,c)*2,dir.transpose()/max<scalarD>(dir.norm(),1E-6f));
        if(fjac)addBlock(*fjac,off,GI(r+1,c)*2,-dir.transpose()/max<scalarD>(dir.norm(),1E-6f));
        fvec[off]=dir.norm()-_l;
        off++;
      }
    for(sizeType r=0; r<_np; r++)
      for(sizeType c=0; c<_np-1; c++) {
        Vec2d dir=x.segment(GI(r,c)*2,2)-x.segment(GI(r,c+1)*2,2);
        if(fjac)addBlock(*fjac,off,GI(r,c)*2,dir.transpose()/max<scalarD>(dir.norm(),1E-6f));
        if(fjac)addBlock(*fjac,off,GI(r,c+1)*2,-dir.transpose()/max<scalarD>(dir.norm(),1E-6f));
        fvec[off]=dir.norm()-_l;
        off++;
      }
    //fixing
#define ADD_FIX(R,C)  \
fvec.segment(off,2)=(x.segment(GI(R,C)*2,2)-Vec2i(R,C).cast<scalarD>()); \
if(fjac)addBlock(*fjac,off,GI(R,C)*2,Mat2d::Identity());  \
off+=2;
    ADD_FIX(0    ,0    )
    ADD_FIX(_np-1,0    )
    ADD_FIX(0    ,_np-1)
    ADD_FIX(_np-1,_np-1)
    return 0;
  }
  sizeType GI(sizeType r,sizeType c) const {
    return r+c*_np;
  }
  virtual int values() const {
    return (_np-1)*_np*2+4*2;
  }
  virtual int inputs() const {
    return _np*_np*2;
  }
private:
  sizeType _np;
  scalarD _l;
};
//krylov matrix
class LMKrylov : public KrylovMatrix<scalarD>
{
public:
  typedef LMInterface::SMat SMat;
  LMKrylov(const SMat& A,const SMat* J,const Matd* CI,scalarD mu)
    :_A(A),_J(J),_CI(CI),_mu(mu) {}
  virtual void multiply(const Vec& b,Vec& out) const {
    sizeType nP=_A.rows(),nD=b.size()-nP;
    Vec bP=b.segment(0,nP),bD=b.segment(nP,nD);

    out.setZero(n());
    out.segment(0,nP)=_A*bP+_mu*bP;
    if(_J)
      out.segment(0,nP)+=_J->transpose()*(*_J*bP);
    if(_CI) {
      out.segment(0,nP)+=_CI->transpose()*bD;
      out.segment(nP,nD)=*_CI*bP;
    }
  }
  virtual sizeType n() const {
    return _A.rows()+(_CI?_CI->rows():0);
  }
  const SMat& _A;
  const SMat* _J;
  const Matd* _CI;
  scalarD _mu;
  Vec _tmp;
};
//helper
template <typename MATO>
Eigen::Block<MATO> extractRHSRef(MATO& V,sizeType i,sizeType tridiag)
{
  sizeType off=i*tridiag,sz=min(tridiag,V.rows()-off);
  return V.block(off,0,sz,V.cols());
}
template <typename MATO,typename MATR>
MATR extractRHS(const MATO& V,sizeType i,sizeType tridiag)
{
  sizeType off=i*tridiag,sz=min(tridiag,V.rows()-off);
  return V.block(off,0,sz,V.cols());
}
template <>
Matd extractRHS<LMInterface::DMat,Matd>(const LMInterface::DMat& V,sizeType i,sizeType tridiag)
{
  Matd ret;
  sizeType off=i*tridiag,sz=min(tridiag,V.rows()-off);
  ret.setZero(sz,off+sz);
  ret.block(0,off,sz,sz).diagonal().setOnes();
  return ret;
}
template <typename MAT>
Matd extractDiag(const MAT& M,sizeType i,scalarD mu,sizeType tridiag)
{
  sizeType off=i*tridiag;
  sizeType szR=min(tridiag,M.rows()-off);
  sizeType szC=min(tridiag,M.cols()-off);
  Matd ret=M.block(off,off,szR,szC);
  ret.diagonal().array()+=mu;
  return ret;
}
template <typename MAT>
Matd extractOffDiag(const MAT& M,sizeType i,sizeType tridiag)
{
  sizeType offR=(i+1)*tridiag,offC=offR-tridiag;
  sizeType szR=min(tridiag,M.rows()-offR);
  sizeType szC=min(tridiag,M.cols()-offC);
  return M.block(offR,offC,szR,szC);
}
template <typename MAT>
typename MAT::Scalar maxDiagElem(const MAT& A,const MAT& J)
{
  typename MAT::Scalar ret=0;
  for(int a=0; a<A.rows(); a++)
    ret=max(ret,A.coeff(a,a)+(J.rows() == 0 ? 0 : J.col(a).squaredNorm()));
  return ret;
}
template <typename MAT>
MAT woodbury(LMInterface::LMDenseInterface& invA,const Matd& J,const Matd& invAJT,const MAT& b)
{
  Matd I_JInvAJT=Matd::Identity(J.rows(),J.rows())+J*invAJT;
  return invA.solve(b)-invAJT*I_JInvAJT.ldlt().solve(invAJT.transpose()*b);
}

//lm interface
class LMDenseInterfaceEigenSym : public LMInterface::LMDenseInterface
{
public:
  typedef LMInterface::SMat SMat;
  virtual boost::shared_ptr<LMDenseInterface> copy() const override {
    return boost::shared_ptr<LMDenseInterface>(new LMDenseInterfaceEigenSym());
  }
  virtual bool recompute(SMat& smat,scalarD shift,bool sameA) override {
    if(shift != 0)
      _sol.setShift(shift);
    _sol.compute(smat);
    return _sol.info() == Eigen::Success;
  }
  virtual Matd solve(const Matd& b) override {
    return _sol.solve(b);
  }
  Eigen::SimplicialCholesky<SMat> _sol;
};
class LMDenseInterfaceEigenUnsym : public LMInterface::LMDenseInterface
{
public:
  typedef LMInterface::SMat SMat;
  virtual boost::shared_ptr<LMDenseInterface> copy() const override {
    return boost::shared_ptr<LMDenseInterface>(new LMDenseInterfaceEigenUnsym());
  }
  virtual bool recompute(SMat& smat,scalarD shift,bool sameA) override {
    if(shift != 0) {
      SMat tmp=smat;
      for(sizeType i=0; i<tmp.size(); i++)
        tmp.coeffRef(i,i)+=shift;
      _sol.compute(tmp);
    } else {
      _sol.compute(smat);
    }
    return _sol.info() == Eigen::Success;
  }
  virtual Matd solve(const Matd& b) override {
    return _sol.solve(b);
  }
  Eigen::SparseLU<SMat> _sol;
};
LMInterface::LMInterface():_tridiag(0),_tau(1),_tauMax(1000),_krylov(0),_useCache(false),_useWoodbury(false)
{
  useInterface(NULL);
}
void LMInterface::reset(sizeType sz,bool hasCI)
{
  _CI=NULL;
  _CI0=NULL;
}
void LMInterface::setCI(const Matd& CI,const Cold& CI0,const Coli& type)
{
  _CI.reset(new Matd(CI));
  _CI0.reset(new Cold(CI0));
  _invCICIT.compute(*_CI*_CI->transpose());
}
void LMInterface::setCB(const Cold& L,const Cold& U) {}
void LMInterface::useCache(bool useCache)
{
  _useCache=useCache;
}
void LMInterface::useWoodbury(bool useWoodbury)
{
  _useWoodbury=useWoodbury;
}
void LMInterface::useInterface(boost::shared_ptr<LMDenseInterface> sol)
{
  _dense=sol;
  if(!_dense)
    _dense.reset(new LMDenseInterfaceEigenSym);
}
boost::shared_ptr<LMInterface::LMDenseInterface> LMInterface::getInterface(bool sym)
{
  if(sym)
    return boost::shared_ptr<LMDenseInterface>(new LMDenseInterfaceEigenSym());
  else return boost::shared_ptr<LMDenseInterface>(new LMDenseInterfaceEigenUnsym());
}
void LMInterface::setKrylov(scalar krylov)
{
  _krylov=krylov;
}
bool LMInterface::solve(Cold& x)
{
  return false;
}
bool LMInterface::solveSparse(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb)
{
  return solveInternal<SMat>(p,obj,cb);
}
bool LMInterface::solveDense(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb)
{
  return solveInternal<Matd>(p,obj,cb);
}
void LMInterface::writeMuHistory(const string& path) const
{
  boost::filesystem::ofstream os(path);
  os << "Iter mu" << endl;
  for(sizeType i=0; i<(sizeType)_muHistory.size(); i++)
    os << (i+1) << " " << _muHistory[i] << endl;
}
void LMInterface::setTridiag(sizeType tridiag)
{
  _tridiag=tridiag;
}
//debug quadratic LM example
void LMInterface::debugLMExampleQuad()
{
#define N 20
  Quadratic quad(N,N*2);
  Cold x=Cold::Random(N);
  Callback<scalarD,Kernel<scalarD> > cb;
  solveDense(x,quad,cb);
#undef N
}
//debug general nonlinear LM example
void LMInterface::debugLMExampleNon()
{
#define DELTA 1E-7f
  Matd fjac;
  MassSpring obj(10,0.5f);
  Cold x=Cold::Random(obj.inputs());
  Cold delta=Cold::Random(obj.inputs());
  Cold fvec,fvec2;
  obj.Objective<scalarD>::operator()(x,fvec,&fjac,false);
  obj.Objective<scalarD>::operator()(x+delta*DELTA,fvec2,(Matd*)NULL,false);
  INFOV("FJac: %f Err: %f",(fjac*delta).norm(),(fjac*delta-(fvec2-fvec)/DELTA).norm())

  Callback<scalarD,Kernel<scalarD> > cb;
  //solveDense(x=Cold::Zero(obj.inputs()),obj,cb);
  //obj.writeVTK(x,"massSpringDense.vtk");
  useWoodbury(false);
  solveSparse(x=Cold::Zero(obj.inputs()),obj,cb);
  obj.writeVTK(x,"massSpringSparse.vtk");
  useWoodbury(true);
  solveSparse(x=Cold::Zero(obj.inputs()),obj,cb);
  obj.writeVTK(x,"massSpringSparseWoodbury.vtk");
#undef DELTA
}
//tridiagonal solver functionality
void LMInterface::debugTridiag()
{
#define N 105
#define T 10
#define NS 5
  SMat LHSS;
  Matd LHS=Matd::Zero(N,N);
  Matd RHS=Matd::Random(N,NS);
  sizeType tridiag=T;
  for(sizeType f=0; f<LHS.rows(); f+=tridiag) {
    sizeType t=min<sizeType>(f+tridiag,LHS.rows());
    //diagonal
    Eigen::Block<Matd> LHSB=LHS.block(f,f,t-f,t-f);
    LHSB.setRandom();
    LHSB=(LHSB.transpose()*LHSB).eval();
    //off-diagonal
    if(f >= tridiag) {
      Eigen::Block<Matd> LHSOB1=LHS.block(f-tridiag,f,tridiag,t-f);
      Eigen::Block<Matd> LHSOB2=LHS.block(f,f-tridiag,t-f,tridiag);
      LHSOB1.setRandom();
      LHSOB2=LHSOB1.transpose();
    }
  }
  //to sparse
  Objective<scalarD>::STrips trips;
  LHSS.resize(LHS.rows(),LHS.cols());
  for(sizeType r=0; r<LHS.rows(); r++)
    for(sizeType c=0; c<LHS.cols(); c++)
      if(LHS(r,c) != 0)
        trips.push_back(Objective<scalarD>::STrip(r,c,LHS(r,c)));
  LHSS.setFromTriplets(trips.begin(),trips.end());
  //add regularization
  scalarD mu=1;
  Matd LHSmu=LHS;
  LHSmu.diagonal().array()+=mu;
  //debug
  Matd x=solveTridiag<Matd,Matd,Matd>(LHS,RHS,mu,tridiag);
  Matd xS=solveTridiag<SMat,Matd,Matd>(LHSS,RHS,mu,tridiag);
  INFOV("Sparse: %f Dense: %f Err: %f",x.norm(),xS.norm(),(x-xS).norm())
  Matd xRef=LHSmu.ldlt().solve(RHS);
  INFOV("Tridiag: %f Err: %f",x.norm(),(x-xRef).norm())
  //debug inv
  Matd invLHS=invTridiag(LHS,mu,tridiag),invLHSRef=LHSmu.inverse();
  INFOV("invLHS: %f, invLHSRef: %f err: %f",invLHS.norm(),invLHSRef.norm(),(invLHSRef-invLHS).norm())
#undef T
#undef N
  exit(-1);
}
//debug constraints
void LMInterface::debugConstraint()
{
#define M 10
#define N 100
  bool succ;
  Matd CI=Matd::Random(M,N);
  Cold CI0=Cold::Random(M);
  Cold p=Cold::Random(N),pStar=p;
  //debug enforce cons
  LMInterface lm;
  lm.setCI(CI,CI0,Coli());
  pStar=lm.enforceCons(pStar);
  INFOV("CI: %f Err: %f",(CI*p+CI0).norm(),(CI*pStar+CI0).norm())
  //solve system: dense
  Cold RHS=Cold::Random(N);
  Matd LHS=Matd::Random(N,N);
  LHS=(LHS.transpose()*LHS).eval();
  Cold DENSE=lm.solveSys(LHS,RHS,1E-3f,NULL,false,succ);
  ASSERT_MSG(succ,"Solver failed!")
  INFOV("Dense Norm: %f Cons: %f",DENSE.norm(),(CI*DENSE).norm())
  //solve system: sparse
  SMat LHSS;
  LHSS.resize(N,N);
  for(sizeType r=0; r<N; r++)
    for(sizeType c=0; c<N; c++)
      LHSS.coeffRef(r,c)=LHS(r,c);
  Cold SPARSE=lm.solveSys(LHSS,RHS,1E-3f,NULL,false,succ);
  ASSERT_MSG(succ,"Solver failed!")
  INFOV("Sparse Norm: %f Cons: %f",SPARSE.norm(),(CI*SPARSE).norm())
  exit(-1);
#undef N
#undef M
}
template <typename MAT>
Matd LMInterface::invTridiag(const MAT& LHS,scalarD mu,sizeType tridiag)
{
  DMat identity;
  identity.resize(LHS.rows());
  identity.diagonal().setOnes();
  return solveTridiag<MAT,DMat,Matd>(LHS,identity,mu,tridiag);
}
template <typename MAT,typename MATO,typename MATR>
MATR LMInterface::solveTridiag(const MAT& LHS,const MATO& RHS,scalarD mu,sizeType tridiag)
{
  sizeType n=(LHS.rows()+tridiag-1)/tridiag;
  //Matd tmp;
  Eigen::FullPivLU<Matd> tmp;
  vector<Matd,Eigen::aligned_allocator<Matd> > H(n-1),D(n),OD(n-1);
  vector<Matd,Eigen::aligned_allocator<MATO> > g(n);
  for(sizeType i=0; i<n; i++) {
    D[i]=extractDiag(LHS,i,mu,tridiag);
    if(i<n-1)
      OD[i]=extractOffDiag(LHS,i,tridiag);
  }
  //forward pass
  tmp=D[0].fullPivLu();
  H[0]=-tmp.solve(OD[0].transpose());
  g[0]=tmp.solve(extractRHS<MATO,MATR>(RHS,0,tridiag));
  for(sizeType i=1; i<n; i++) {
    tmp=(D[i]+OD[i-1]*H[i-1]).fullPivLu();
    if(i<n-1)
      H[i]=-tmp.solve(OD[i].transpose());

    //block version
    g[i]=extractRHS<MATO,MATR>(RHS,i,tridiag);
    g[i].block(0,0,OD[i-1].rows(),g[i-1].cols())-=OD[i-1]*g[i-1];
    g[i]=tmp.solve(g[i]).eval();
    //full version
    //g[i]=tmp.solve(extractRHS(RHS,i,tridiag)-OD[i-1]*g[i-1]);
  }
  //backward pass
  Matd lastSol=g[n-1];
  MATR sol=MATR::Zero(RHS.rows(),RHS.cols());
  extractRHSRef<MATR>(sol,n-1,tridiag)=lastSol;
  for(sizeType i=n-2; i>=0; i--) {
    //block version
    lastSol=H[i]*lastSol;
    lastSol.block(0,0,g[i].rows(),g[i].cols())+=g[i];
    //full version
    //lastSol=g[i]+H[i]*lastSol;
    extractRHSRef<MATR>(sol,i,tridiag)=lastSol;
  }
  return sol;
}
void LMInterface::checkTridiag(const Matd& LHS,sizeType tridiag)
{
  for(sizeType f=0; f<LHS.rows(); f+=tridiag) {
    sizeType t=min<sizeType>(f+tridiag,LHS.rows());
    if(f > tridiag) {
      ASSERT_MSG(LHS.block(f,0,t-f,f-tridiag).isZero(),"Tridiagonal check failed!")
    }
    sizeType ff=f+tridiag*2;
    if(LHS.cols() > ff) {
      ASSERT_MSG(LHS.block(f,ff,t-f,LHS.cols()-ff).isZero(),"Tridiagonal check failed!")
    }
  }
}
Cold LMInterface::solveSys(const SMat& A,const Cold& RHS,scalarD mu,const SMat* J,bool sameA,bool& succ) const
{
  if(_krylov > 0) {
    Cold x,RHSAug=RHS;
    PMINRESSolverQLP<scalarD> sol;
    sol.setSolverParameters(_krylov,1E4);
    sol.setKrylovMatrix(boost::shared_ptr<KrylovMatrix<scalarD> >(new LMKrylov(A,J,_CI.get(),mu)));
    if(_CI)
      RHSAug=concat(RHS,Cold::Zero(_CI->rows()));
    x.setZero(RHSAug.size());
    //sol.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> > ));
    succ=sol.solve(RHSAug,x) == PMINRESSolverQLP<scalarD>::SUCCESSFUL;
    return x.segment(0,RHS.size());
  }

  succ=true;
  Cold invARHS;
  Matd invAB,invAJT,JD;
  SMat LHS=A;
  if(J && !_useWoodbury)
    LHS+=J->transpose()**J;
  if(_tridiag > 0) {
    ASSERT_MSG(!_useWoodbury,"Tridiagonal solver does not support Woodbury transform!")
    ASSERT_MSG(!_CI && !_CI0,"Tridiagonal solver does not support constraints!")
    checkTridiag(LHS,_tridiag);
    return solveTridiag<SMat,Cold,Cold>(LHS,RHS,mu,_tridiag);
  } else {
    succ=_dense->recompute(LHS,mu,sameA);
    if(!succ)
      return Cold();
    if(!_CI && !_CI0) {
      if(_useWoodbury) {
        JD=J->toDense();
        invAJT=_dense->solve(Matd(JD.transpose()));
        return woodbury<Cold>(*_dense,JD,invAJT,RHS);
      } else return _dense->solve(RHS);
    } else {
      if(_useWoodbury) {
        JD=J->toDense();
        invAJT=_dense->solve(Matd(JD.transpose()));
        invAB=woodbury<Matd>(*_dense,JD,invAJT,_CI->transpose());
        invARHS=woodbury<Cold>(*_dense,JD,invAJT,RHS);
      } else {
        invAB=_dense->solve(Matd(_CI->transpose()));
        invARHS=_dense->solve(RHS);
      }
      return invARHS-invAB*(*_CI*invAB).eval().ldlt().solve(Matd(invAB.transpose()*RHS));
    }
  }
}
Cold LMInterface::solveSys(const Matd& A,const Cold& RHS,scalarD mu,const Matd* J,bool sameA,bool& succ) const
{
  succ=true;
  Matd LHS=A;
  if(J)
    LHS+=J->transpose()**J;
  if(_tridiag > 0) {
    ASSERT_MSG(!_CI && !_CI0,"Tridiagonal solver does not support constraints!")
    checkTridiag(LHS,_tridiag);
    return solveTridiag<Matd,Cold,Cold>(LHS,RHS,mu,_tridiag);
  } else if(!_CI && !_CI0) {
    return (LHS+Matd::Identity(LHS.rows(),LHS.cols())*mu).llt().solve(RHS);
  } else {
    sizeType nr=LHS.rows(),nrA=_CI->rows();
    Matd LHSC=Matd::Zero(nr+nrA,nr+nrA);
    LHSC.block(0,0,nr,nr)=LHS+Matd::Identity(LHS.rows(),LHS.cols())*mu;
    LHSC.block(nr,0,nrA,nr)=*_CI;
    LHSC.block(0,nr,nr,nrA)=_CI->transpose();
    return LHSC.fullPivLu().solve(concat(RHS,Cold::Zero(nrA))).segment(0,nr);
  }
}
//helper
template <typename MAT>
bool LMInterface::solveInternal(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb)
{
  if(_CI && _CI0)
    p=enforceCons(p);

  MAT J,A,JNew,ANew;
  Cold f,g,gc,fNew,gNew,pNew,delta;
  bool report=dynamic_cast<NoCallback<scalarD,Kernel<scalarD> >*>(&cb) == NULL;
  //we allow problem to be modified here
  obj(p,f,&J,true); //compute new gradient and jacobian
  scalarD ENew,E=f.squaredNorm()/2+obj(p,&g,&A);  //add a non-square part if there is some
  scalarD mu=tau()*maxDiagElem(A,J),muMax=max<scalarD>(mu,1.0f)*tauMax();
  g+=J.transpose()*f;

  cb(p,g,E,p.norm(),g.norm(),mu,0,1);
  if(g.cwiseAbs().maxCoeff() < _tolg) {
    if(report) {
      INFO("Exit on tolG")
    }
    return true;
  }

#define DEC_MU  \
mu=max<scalarD>(mu*max<scalarD>(1/3.0f,1-pow(2*rho-1,3)),minMu);  \
nu=2; \
sameA=false;
#define INC_MU  \
mu=max<scalarD>(mu*nu,minMu); \
nu*=2;  \
sameA=true;
  _iter=0;
  _muHistory.clear();
  bool sameA=false,succ;
  scalarD nu=2,rho=0,deltaF=0,minMu=1E-3f;
  while(true) {
    delta=solveSys(A,-g,mu,&J,sameA,succ);
    if(succ && delta.norm() < _tolx*p.norm()) {
      if(report) {
        INFO("Exit on tolX")
      }
      break;
    } else if(!succ) {
      INC_MU
      if(mu > muMax) {
        if(report) {
          INFO("Exit on maxMu")
        }
        break;
      }
      _iter++;
    } else {
      _muHistory.push_back(mu);
      pNew=p+delta;
      if(_CI && _CI0)
        pNew=enforceCons(pNew);
      if(!_useCache) {
        obj(pNew,fNew,(MAT*)NULL,false);
        ENew=fNew.squaredNorm()/2+obj(pNew,(Cold*)NULL,(MAT*)NULL);
      } else {
        obj(pNew,fNew,&JNew,false); //compute new gradient and jacobian
        ENew=fNew.squaredNorm()/2+obj(pNew,&gNew,&ANew); //add a non-square part if there is some
      }
      deltaF=E-ENew;
      rho=deltaF/(delta.dot(mu*delta-g)/2);
      if(rho > 0 && deltaF > 0) {
        p=pNew;
        //we allow problem to be modified here
        if(!_useCache) {
          obj(pNew,f,&J,true); //compute new gradient and jacobian
          E=f.squaredNorm()/2+obj(pNew,&g,&A); //add a non-square part if there is some
        } else {
          swap(E,ENew);
          J.swap(JNew);
          A.swap(ANew);
          f.swap(fNew);
          g.swap(gNew);
        }
        g+=J.transpose()*f;
        gc=(_CI && _CI0) ? enforceConsZero(g) : g;
        if(gc.cwiseAbs().maxCoeff() < _tolg) {
          if(report) {
            INFO("Exit on tolG")
          }
          break;
        }
        if(deltaF < _tolf) {
          if(report) {
            INFO("Exit on tolF")
          }
          break;
        }
        cb(p,gc,E,p.norm(),gc.norm(),mu,_iter,1);
        DEC_MU
        _iter++;
      } else {
        INC_MU
        if(mu > muMax) {
          if(report) {
            INFO("Exit on maxMu")
          }
          break;
        }
        _iter++;
      }
    }
    if(_iter == _maxIter) {
      if(report) {
        INFO("Exit on maxIter")
      }
      break;
    }
  }
#undef DEC_MU
#undef INC_MU
  return _iter < _maxIter;// && mu < muMax;
}
Cold LMInterface::enforceConsZero(const Cold& p) const
{
  return p-_CI->transpose()*_invCICIT.solve(*_CI*p);
}
Cold LMInterface::enforceCons(const Cold& p) const
{
  return p-_CI->transpose()*_invCICIT.solve(*_CI*p+*_CI0);
}
void LMInterface::setTauMax(scalarD tauMax)
{
  _tauMax=max<scalarD>(tauMax,1E-10f);
}
void LMInterface::setTau(scalarD tau)
{
  _tau=max<scalarD>(tau,1E-10f);
}
scalarD LMInterface::tau() const
{
  return _tau;
}
scalarD LMInterface::tauMax() const
{
  return _tauMax;
}
//LMInterface
template Matd LMInterface::invTridiag<Objective<scalarD>::SMat>(const Objective<scalarD>::SMat& LHS,scalarD mu,sizeType tridiag);
template Cold LMInterface::solveTridiag<Objective<scalarD>::SMat,Cold>(const Objective<scalarD>::SMat& LHS,const Cold& RHS,scalarD mu,sizeType tridiag);
template Matd LMInterface::solveTridiag<Objective<scalarD>::SMat,Matd>(const Objective<scalarD>::SMat& LHS,const Matd& RHS,scalarD mu,sizeType tridiag);

PRJ_END
