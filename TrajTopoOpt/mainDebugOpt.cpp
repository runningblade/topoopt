#include "TrajTopo/TrajectoryOptimization.h"
#include "TrajTopo/TrajectoryObjective.h"
#include "TrajTopo/BuildScenario.h"
#include "TrajTopo/DynamicModel.h"
#include "TrajTopo/LMCholmod.h"
#include "TrajTopo/LMUmfpack.h"

USE_PRJ_NAMESPACE

#define TYPE 1
template <typename T>
void ioFilter(T& val)
{
  {
    boost::filesystem::ofstream os("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.write(os,dat.get());
  }
  {
    val=T();
    boost::filesystem::ifstream is("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.read(is,dat.get());
  }
}
void debugSpringSym2D()
{
  DynamicModel<2> d=BuildScenario<2>().buildSpringGrid(Vec2d(1,0.5f),Vec2i(11,11));
  ioFilter(d);
  TrajectoryOptimization<2> opt(d);
  ioFilter(opt);
  opt.resetVar(2);
  opt.addVarCoefP();
  opt.writeCoefPVTK("./SpringNonSym2D.vtk");
  opt.addVarCoefPSym();
  opt.writeCoefPVTK("./SpringSym2D.vtk");
}
void debugSpringSym3D()
{
  DynamicModel<3> d=BuildScenario<3>().buildSpringGrid(Vec3d(1,0.5f,1),Vec3i(5,5,5));
  ioFilter(d);
  TrajectoryOptimization<3> opt(d);
  ioFilter(opt);
  opt.resetVar(2);
  opt.addVarCoefP();
  opt.writeCoefPVTK("./SpringNonSym3D.vtk");
  opt.addVarCoefPSym();
  opt.writeCoefPVTK("./SpringSym3D.vtk");
}
void debugFEMSym2D()
{
  DynamicModel<2> d=BuildScenario<2>().buildFEMGrid(Vec2d(1,0.5f  ),Vec2i(11,11));
  ioFilter(d);
  TrajectoryOptimization<2> opt(d);
  ioFilter(opt);
  opt.resetVar(2);
  opt.addVarCoefP();
  opt.writeCoefPVTK("./FEMNonSym2D.vtk");
  opt.addVarCoefPSym();
  opt.writeCoefPVTK("./FEMSym2D.vtk");
}
void debugFEMSym3D()
{
  DynamicModel<3> d=BuildScenario<3>().buildFEMGrid(Vec3d(1,0.5f,1),Vec3i(5,5,5));
  ioFilter(d);
  TrajectoryOptimization<3> opt(d);
  ioFilter(opt);
  opt.resetVar(2);
  opt.addVarCoefP();
  opt.writeCoefPVTK("./FEMNonSym3D.vtk");
  opt.addVarCoefPSym();
  opt.writeCoefPVTK("./FEMSym3D.vtk");
}
void debugSpringRandomObjective2D(scalarD scale)
{
  BuildScenario<2> builder(false);
  builder._pt.put<scalarD>("coefGroundK",1E4f);
  builder._pt.put<sizeType>("contactType",CONTACT_FIX);
  DynamicModel<2> d=builder.buildSpringGrid(Vec2d(1,0.5f),Vec2i(11,11));
  boost::shared_ptr<TrajectoryObjective<2> > obj(new RandomObjective<2>());
  obj->reset(d);

  TrajectoryOptimization<2> opt(d);
  //opt.useSuiteSparse(true);
  opt.resetObj(obj);
  opt.resetVar(0.5f);
#if TYPE == 1
  opt.debugTopologyGradient(scale,5E2f,false);
#elif TYPE == 2
  opt.debugTrajectoryGradient(scale,scale);
#else
  opt.debugMinimizeTrajectoryGradient(scale,1E3f,1E-3f);
#endif
}
void debugFEMRandomObjective2D(scalarD scale)
{
  BuildScenario<2> builder(false);
  builder._pt.put<scalarD>("coefGroundK",1E4f);
  builder._pt.put<sizeType>("contactType",CONTACT_FIX);
  DynamicModel<2> d=builder.buildFEMGrid(Vec2d(1,0.5f),Vec2i(11,11));
  boost::shared_ptr<TrajectoryObjective<2> > obj(new RandomObjective<2>());
  obj->reset(d);

  TrajectoryOptimization<2> opt(d);
  //opt.useSuiteSparse(true);
  opt.resetObj(obj);
  opt.resetVar(0.5f);
#if TYPE == 1
  opt.debugTopologyGradient(scale,5E2f,false);
#elif TYPE == 2
  opt.debugTrajectoryGradient(scale,scale);
#else
  opt.debugMinimizeTrajectoryGradient(scale,1E3f,1E-3f);
#endif
}
void debugHeightObj2D()
{
  HeightObjective<2> obj;
  obj._averageHeight.setRandom(100);
  obj._expCoef=0.01f;
  ioFilter(obj);
  obj.debugGradient(10,100,15);
}
void debugHeightObj3D()
{
  HeightObjective<3> obj;
  obj._averageHeight.setRandom(100);
  obj._expCoef=0.01f;
  ioFilter(obj);
  obj.debugGradient(10,100,15);
}
void debugCholmod()
{
#ifdef CHOLMOD_SUPPORT
  CholmodWrapper().debug();
#endif
}
void debugUmfpack()
{
#ifdef UMFPACK_SUPPORT
  UmfpackWrapper().debug();
#endif
}
int main()
{
  //debugCholmod();
  //debugUmfpack();
  //DynamicModel<2> model;
  //model.debugErrorConfig();
  //debugSpringSym2D();
  //debugSpringSym3D();
  //debugFEMSym2D();
  //debugFEMSym3D();
  debugSpringRandomObjective2D(0.05f);
  debugFEMRandomObjective2D(0.05f);
  //debugHeightObj2D();
  //debugHeightObj3D();
  return 0;
}
