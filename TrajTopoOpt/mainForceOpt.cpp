#include "TrajTopo/TrajectoryOptimization.h"
#include "TrajTopo/TrajectoryObjective.h"
#include "TrajTopo/BuildScenario.h"
#include "TrajTopo/DynamicModel.h"
#include "TrajTopo/Utils.h"

USE_PRJ_NAMESPACE

#define RES 11
template <typename T>
void ioFilter(T& val)
{
  {
    boost::filesystem::ofstream os("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.write(os,dat.get());
  }
  {
    val=T();
    boost::filesystem::ifstream is("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.read(is,dat.get());
  }
}
void springForceOpt2D()
{
  BuildScenario<2> builder;
  assignVec(Vec2d(Vec2d(0.75f,1.25f)),"limit",builder._pt);
  builder._pt.put<scalarD>("coefK",5E4f);
  DynamicModel<2> d=builder.buildSpringGrid(Vec2d(1,1),Vec2i(RES,RES));
  boost::shared_ptr<HeightObjective<2> > obj(new HeightObjective<2>());
  obj->_expCoef=log(100)*RES;
  obj->reset(d);

  TrajectoryOptimization<2> opt(d);
  ioFilter(opt);
  opt.resetObj(obj);
  opt.resetVar(1);
  opt.addVarFTopG();
  opt.getPt().put<bool>("trajGrad.useCB",true);
  opt.getPt().put<scalarD>("force.target",-1/scalarD(RES));
  ASSERT(opt.optimizeForceBruteForce())
  opt.makeLocalMinima();
  opt.TrajectoryOptimization<2>::write("./springForceOpt2D.dat");
  opt.writeTrajResultVTK("./springForceAnim");
}
void FEMForceOpt2D()
{
  BuildScenario<2> builder;
  assignVec(Vec2d(Vec2d(0.75f,1.25f)),"limit",builder._pt);
  builder._pt.put<scalarD>("coefK",1E4f);
  DynamicModel<2> d=builder.buildFEMGrid(Vec2d(1,1),Vec2i(RES,RES));
  boost::shared_ptr<HeightObjective<2> > obj(new HeightObjective<2>());
  obj->_expCoef=log(100)*RES;
  obj->reset(d);

  TrajectoryOptimization<2> opt(d);
  ioFilter(opt);
  opt.resetObj(obj);
  opt.resetVar(1);
  opt.addVarFTopG();
  opt.getPt().put<bool>("trajGrad.useCB",true);
  opt.getPt().put<scalarD>("force.target",-1/scalarD(RES));
  ASSERT(opt.optimizeForceBruteForce())
  opt.makeLocalMinima();
  opt.TrajectoryOptimization<2>::write("./FEMForceOpt2D.dat");
  opt.writeTrajResultVTK("./FEMForceAnim");
}
int main()
{
  springForceOpt2D();
  FEMForceOpt2D();
  return 0;
}
