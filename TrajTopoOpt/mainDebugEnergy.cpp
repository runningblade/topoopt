#include "TrajTopo/DirectionalStrainLimiting.h"
#include "TrajTopo/AreaStrainLimiting.h"
#include "TrajTopo/GroundContact.h"
#include "TrajTopo/TrajectoryObjective.h"

USE_PRJ_NAMESPACE

template <typename T>
void ioFilter(T& val)
{
  {
    boost::filesystem::ofstream os("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.write(os,dat.get());
  }
  {
    val=T();
    boost::filesystem::ifstream is("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.read(is,dat.get());
  }
}
template <typename MODEL>
void debugDirectionalStrain2D()
{
  typedef Cold Vec;
  MODEL s2(Vec::Random(4),0,1,Vec2d(0.75f,1.25f),RandEngine::randR01());
  ioFilter(s2);
  s2.debugGradient();
}
template <typename MODEL>
void debugAreaStrain2D()
{
  typedef Cold Vec;
  MODEL s2(Vec::Random(6),0,1,2,Vec2d(0.75f,1.25f),RandEngine::randR01());
  ioFilter(s2);
  s2.debugGradient();
}
template <typename MODEL>
void debugDirectionalStrain3D()
{
  typedef Cold Vec;
  MODEL s3(Vec::Random(6),0,1,Vec2d(0.75f,1.25f),RandEngine::randR01());
  ioFilter(s3);
  s3.debugGradient();
}
template <typename MODEL>
void debugAreaStrain3D()
{
  typedef Cold Vec;
  MODEL s3(Vec::Random(12),0,1,2,3,Vec2d(0.75f,1.25f),RandEngine::randR01());
  ioFilter(s3);
  s3.debugGradient();
}
template <typename MODEL>
void debugContact2D()
{
  typedef Cold Vec;
  MODEL s2(Vec::Random(4),1,Vec2d::Random(),RandEngine::randR01());
  ioFilter(s2);
  s2.debugGradient();
}
template <typename MODEL>
void debugContact3D()
{
  typedef Cold Vec;
  MODEL s3(Vec::Random(6),1,Vec3d::Random(),RandEngine::randR01());
  ioFilter(s3);
  s3.debugGradient();
}
void debugHeightObj2D()
{
  HeightObjective<2> obj;
  obj._averageHeight.setRandom(100);
  obj._expCoef=0.01f;
  ioFilter(obj);
  obj.debugGradient(10,100,15);
}
void debugHeightObj3D()
{
  HeightObjective<3> obj;
  obj._averageHeight.setRandom(100);
  obj._expCoef=0.01f;
  ioFilter(obj);
  obj.debugGradient(10,100,15);
}
int main()
{
  //directional strain
  debugDirectionalStrain2D<DirectionalStrainLimitingQuadratic<2> >();
  debugDirectionalStrain2D<DirectionalStrainLimitingCubic<2> >();
  debugDirectionalStrain2D<DirectionalStrainLimitingSoftmax<2> >();
  debugDirectionalStrain2D<DirectionalStrainLimitingLog<2> >();

  debugDirectionalStrain3D<DirectionalStrainLimitingQuadratic<3> >();
  debugDirectionalStrain3D<DirectionalStrainLimitingCubic<3> >();
  debugDirectionalStrain3D<DirectionalStrainLimitingSoftmax<3> >();
  debugDirectionalStrain3D<DirectionalStrainLimitingLog<3> >();
  //area strain
  debugAreaStrain2D<AreaStrainLimitingQuadratic>();
  debugAreaStrain2D<AreaStrainLimitingCubic>();
  debugAreaStrain2D<AreaStrainLimitingSoftmax>();
  debugAreaStrain2D<AreaStrainLimitingLog>();

  debugAreaStrain3D<VolumeStrainLimitingQuadratic>();
  debugAreaStrain3D<VolumeStrainLimitingCubic>();
  debugAreaStrain3D<VolumeStrainLimitingSoftmax>();
  debugAreaStrain3D<VolumeStrainLimitingLog>();
  //contact
  debugContact2D<GroundContactQuadratic<2> >();
  debugContact3D<GroundContactQuadratic<3> >();
  debugContact2D<GroundContactCubic<2> >();
  debugContact3D<GroundContactCubic<3> >();
  debugContact2D<GroundContactSoftmax<2> >();
  debugContact3D<GroundContactSoftmax<3> >();
  debugContact2D<GroundContactFix<2> >();
  debugContact3D<GroundContactFix<3> >();
  //height
  debugHeightObj2D();
  debugHeightObj3D();
  return 0;
}
