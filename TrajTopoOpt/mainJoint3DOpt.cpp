#include "TrajTopo/TrajectoryOptimization.h"
#include "TrajTopo/TrajectoryObjective.h"
#include "TrajTopo/BuildScenario.h"
#include "TrajTopo/DynamicModel.h"
#include "TrajTopo/Utils.h"

USE_PRJ_NAMESPACE

#define RES 5
template <typename T>
void ioFilter(T& val)
{
  {
    boost::filesystem::ofstream os("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.write(os,dat.get());
  }
  {
    val=T();
    boost::filesystem::ifstream is("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.read(is,dat.get());
  }
}
void springJoint3D()
{
  BuildScenario<3> builder;
  assignVec(Vec2d(Vec2d(0.75f,1.25f)),"limit",builder._pt);
  builder._pt.put<scalarD>("coefK",5E4f);
  DynamicModel<3> d=builder.buildSpringGrid(Vec3d(1,1,1),Vec3i(RES,RES,RES));
  boost::shared_ptr<HeightObjective<3> > obj(new HeightObjective<3>());
  obj->_expCoef=log(100)*RES;
  obj->reset(d);

  TrajectoryOptimization<3> opt(d);
  ioFilter(opt);
  opt.resetObj(obj);
  opt.resetVar(1);
  opt.addVarCoefPSym();
  opt.addVarFTopG();
  opt.getPt().put<scalarD>("force.deltaF",1E4f);
  opt.getPt().put<bool>("trajGrad.useCB",false);
  opt.getPt().put<string>("joint.prefix","./spring3D");
  opt.getPt().put<scalarD>("force.target",-1/scalarD(RES));
  opt.optimizeJoint();
}
void FEMJoint3D()
{
  BuildScenario<3> builder;
  assignVec(Vec2d(Vec2d(0.75f,1.25f)),"limit",builder._pt);
  builder._pt.put<scalarD>("coefK",1E4f);
  DynamicModel<3> d=builder.buildFEMGrid(Vec3d(1,1,1),Vec3i(RES,RES,RES));
  boost::shared_ptr<HeightObjective<3> > obj(new HeightObjective<3>());
  obj->_expCoef=log(100)*RES;
  obj->reset(d);

  TrajectoryOptimization<3> opt(d);
  ioFilter(opt);
  opt.resetObj(obj);
  opt.resetVar(1);
  opt.addVarCoefPSym();
  opt.addVarFTopG();
  opt.getPt().put<bool>("trajGrad.useCB",false);
  opt.getPt().put<string>("joint.prefix","./FEM3D");
  opt.getPt().put<scalarD>("force.target",-1/scalarD(RES));
  opt.optimizeJoint();
}
int main()
{
  OmpSettings::getOmpSettings().useAllThreads();
  springJoint3D();
  FEMJoint3D();
  return 0;
}
