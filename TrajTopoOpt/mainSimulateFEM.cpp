#include "TrajTopo/TrajectoryObjective.h"
#include "TrajTopo/BuildScenario.h"
#include "TrajTopo/DynamicModel.h"
#include "TrajTopo/FEMModel.h"
#include "TrajTopo/FEMModelLinear.h"
#include "TrajTopo/Utils.h"

USE_PRJ_NAMESPACE

//#define DBG
#define MODEL FEMModel
template <typename T>
void ioFilter(T& val)
{
  {
    boost::filesystem::ofstream os("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.write(os,dat.get());
  }
  {
    val=T();
    boost::filesystem::ifstream is("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.read(is,dat.get());
  }
}
void debugFEM2D()
{
  typedef Cold Vec;
  MODEL<2> s2(Vec::Random(8),0.45f,100,10, 0,1,2,3, 0);
  ioFilter(s2);
  s2.debugGradient();
}
void debugFEM3D()
{
  typedef Cold Vec;
  MODEL<3> s3(Vec::Random(24),0.45f,100,10, 0,1,2,3,4,5,6,7, 0);
  ioFilter(s3);
  s3.debugGradient();
}
void debugShape2D(scalarD scale)
{
  DynamicModel<2> d=BuildScenario<2>().buildFEMGrid(Vec2d(1,0.5f),Vec2i(10,10));
  ioFilter(d);
  d.writeVTK("./shape.vtk");
  d.debugGradient(scale);
}
void debugShape3D(scalarD scale)
{
  DynamicModel<3> d=BuildScenario<3>().buildFEMGrid(Vec3d(1,0.5f,1),Vec3i(5,5,5));
  ioFilter(d);
  d.writeVTK("./shape.vtk");
  d.debugGradient(scale);
}
void debugAdjoint2D(scalarD scale)
{
  DynamicModel<2> d=BuildScenario<2>().buildFEMGrid(Vec2d(1,0.5f  ),Vec2i(10,10));
  ioFilter(d);
  d.debugAdjointTransfer(0.05f,scale);
}
void debugAdjoint3D(scalarD scale)
{
  DynamicModel<3> d=BuildScenario<3>().buildFEMGrid(Vec3d(1,0.5f,1),Vec3i(5,5,5));
  ioFilter(d);
  d.debugAdjointTransfer(0.05f,scale);
}
void debugSimulate2D()
{
  BuildScenario<2> builder;
  builder._pt.put<scalarD>("coefK",1E4f);
  DynamicModel<2> d=builder.buildFEMGrid(Vec2d(1,0.5f),Vec2i(10,10));
  ioFilter(d);
  d.writeVTK("./init.vtk");
  d.advances(1.0f,0.01f,true,"./anim2D");
}
void debugSimulate3D()
{
  BuildScenario<3> builder;
  builder._pt.put<scalarD>("coefK",1E4f);
  DynamicModel<3> d=builder.buildFEMGrid(Vec3d(1,0.5f,1),Vec3i(5,5,5));
  ioFilter(d);
  d.writeVTK("./init.vtk");
  d.advances(1.0f,0.01f,true,"./anim3D");
}
void debugSimulate2DBend()
{
  BuildScenario<2> builder;
  builder._pt.put<scalarD>("coefK",1E4f);
  assignVec(Vec2d(-9.81f,0),"g",builder._pt);
  builder._pt.put<scalarD>("coefGroundK",1E6f);
  builder._pt.put<sizeType>("contactType",CONTACT_FIX);
  //builder._pt.put<scalarD>("coefStrainLimiting",10);
  //builder._pt.put<sizeType>("limitType",AREA_LOG);
  builder._pt.put<scalarD>("coefStrainLimiting",1E10);
  builder._pt.put<sizeType>("limitType",AREA_CUBIC);
  DynamicModel<2> d=builder.buildFEMGrid(Vec2d(1,5),Vec2i(11,51));
  ioFilter(d);
  //d.resetLM(true);
  d.writeVTK("./init.vtk");
  d.advances(5.0f,0.01f,true,"./anim2DBend");
}
void debugSimulate3DBend()
{
  BuildScenario<3> builder;
  builder._pt.put<scalarD>("coefK",1E4f);
  assignVec(Vec2d(-9.81f,0),"g",builder._pt);
  builder._pt.put<scalarD>("coefGroundK",1E6f);
  builder._pt.put<sizeType>("contactType",CONTACT_FIX);
  DynamicModel<3> d=builder.buildFEMGrid(Vec3d(1,5,1),Vec3i(6,31,6));
  ioFilter(d);
  d.resetLM(true);
  d.writeVTK("./init.vtk");
  d.advances(5.0f,0.01f,true,"./anim3DBend");
}
int main()
{
#ifdef DBG
  debugFEM2D();
  debugFEM3D();
  debugShape2D(0.05f);
  debugShape3D(0.05f);
  debugAdjoint2D(0.05f);
  debugAdjoint3D(0.05f);
#else
  //debugSimulate2D();
  //debugSimulate3D();
  debugSimulate2DBend();
  //debugSimulate3DBend();
#endif
  return 0;
}
