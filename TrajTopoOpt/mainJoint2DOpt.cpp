#include "TrajTopo/TrajectoryOptimization.h"
#include "TrajTopo/TrajectoryObjective.h"
#include "TrajTopo/BuildScenario.h"
#include "TrajTopo/DynamicModel.h"
#include "TrajTopo/Utils.h"

USE_PRJ_NAMESPACE

#define RES 21
template <typename T>
void ioFilter(T& val)
{
  {
    boost::filesystem::ofstream os("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.write(os,dat.get());
  }
  {
    val=T();
    boost::filesystem::ifstream is("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.read(is,dat.get());
  }
}
void springJoint2D()
{
  BuildScenario<2> builder(true);
  builder._pt.put<scalarD>("coefK",5E4f);
  assignVec(Vec2d(0.5f,100.0f),"limit",builder._pt);
  //builder._pt.put<sizeType>("limitType",AREA_SOFTMAX);
  //builder._pt.put<scalarD>("coefStrainLimiting",10);
  //builder._pt.put<sizeType>("limitType",AREA_LOG);
  builder._pt.put<scalarD>("coefStrainLimiting",1E11);
  builder._pt.put<sizeType>("limitType",AREA_CUBIC);
  DynamicModel<2> d=builder.buildSpringGrid(Vec2d(1,1),Vec2i(RES,RES));
  boost::shared_ptr<HeightObjective<2> > obj(new HeightObjective<2>());
  obj->_expCoef=log(100)*RES;
  obj->reset(d);

  TrajectoryOptimization<2> opt(d);
  ioFilter(opt);
  opt.resetObj(obj);
  opt.resetVar(1);
  opt.addVarCoefPSym();
  opt.addVarFTopG();
  opt.getPt().put<bool>("trajGrad.useCB",false);
  opt.getPt().put<string>("joint.prefix","./spring2D");
  opt.getPt().put<scalarD>("force.target",-1/scalarD(RES));
  opt.optimizeJoint();
}
void FEMJoint2D()
{
  BuildScenario<2> builder(true);
  builder._pt.put<scalarD>("coefK",1E4f);
  assignVec(Vec2d(0.5f,100.0f),"limit",builder._pt);
  //builder._pt.put<sizeType>("limitType",AREA_SOFTMAX);
  //builder._pt.put<scalarD>("coefStrainLimiting",10);
  //builder._pt.put<sizeType>("limitType",AREA_LOG);
  builder._pt.put<scalarD>("coefStrainLimiting",1E11);
  builder._pt.put<sizeType>("limitType",AREA_CUBIC);
  DynamicModel<2> d=builder.buildFEMGrid(Vec2d(1,1),Vec2i(RES,RES));
  boost::shared_ptr<HeightObjective<2> > obj(new HeightObjective<2>());
  obj->_expCoef=log(100)*RES;
  obj->reset(d);

  TrajectoryOptimization<2> opt(d);
  ioFilter(opt);
  opt.resetObj(obj);
  opt.resetVar(1);
  opt.addVarCoefPSym();
  opt.addVarFTopG();
  opt.getPt().put<bool>("trajGrad.useCB",false);
  opt.getPt().put<string>("joint.prefix","./FEM2D");
  opt.getPt().put<scalarD>("force.target",-1/scalarD(RES));
  opt.optimizeJoint();
}
int main()
{
  //TrajectoryOptimization<2> opt;
  //opt.debugBackwardError();
  OmpSettings::getOmpSettings().useAllThreads();
  springJoint2D();
  //FEMJoint2D();
  return 0;
}
