#include "TrajTopo/TrajectoryOptimization.h"
#include "TrajTopo/TrajectoryObjective.h"
#include "TrajTopo/BuildScenario.h"
#include "TrajTopo/DynamicModel.h"

USE_PRJ_NAMESPACE

#define LINEAR false
template <typename T>
void ioFilter(T& val)
{
  {
    boost::filesystem::ofstream os("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.write(os,dat.get());
  }
  {
    val=T();
    boost::filesystem::ifstream is("tmp.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerType<T>(dat.get());
    val.read(is,dat.get());
  }
}
void debugSpringTopoOpt()
{
  TrajectoryOptimization<2> opt;
  opt.read("./springResult.dat");
  opt.debugTopologyGradient(-1,-1,false);
}
void debugMinimizeTrajectoryGradient()
{
  TrajectoryOptimization<2> opt;
  opt.debugMinimizeTrajectoryGradientError();
}
void springTopoOpt()
{
  TrajectoryOptimization<2> opt;
  opt.TrajectoryOptimization::read("./springForceOpt2D.dat");
  opt.addVarCoefPSym();
  opt.resetTimeVarContactPhase(1);
  opt.writeTrajResultVTK("./springInit");
  opt.getPt().put<bool>("trajGrad.useCB",false);
  opt.getPt().put<sizeType>("topo.maxIter",1000);
  opt.optimizeTopology();
  opt.write("./springResult.dat");
  opt.writeTrajResultVTK("./springResult");
}
void FEMTopoOpt()
{
  TrajectoryOptimization<2> opt;
  opt.TrajectoryOptimization::read("./FEMForceOpt2D.dat");
  opt.addVarCoefPSym();
  opt.resetTimeVarContactPhase(1);
  opt.writeTrajResultVTK("./FEMInit");
  opt.getPt().put<bool>("trajGrad.useCB",false);
  opt.getPt().put<sizeType>("topo.maxIter",1000);
  opt.optimizeTopology();
  opt.write("./FEMResult.dat");
  opt.writeTrajResultVTK("./FEMResult");
}
int main()
{
  //debugMinimizeTrajectoryGradient();
  //debugSpringTopoOpt();
  springTopoOpt();
  FEMTopoOpt();
  return 0;
}
